<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- firstName [input]
- lastName [input]
- jobRole [select]
- companyType [select]
- companySize [select]
- companyName [input]
- email [email]
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => 'BAD',
   'name' => 'BookADemo',
   'description' => '',
   'creationDate' => 0,
   'modificationDate' => 1622727390,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => NULL,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => NULL,
         'border' => false,
         'name' => 'Inquiry',
         'type' => NULL,
         'region' => NULL,
         'title' => 'Inquiry',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => 500,
             'defaultValue' => NULL,
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'firstName',
             'title' => 'FirstName',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => 500,
             'defaultValue' => NULL,
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'lastName',
             'title' => 'Last Name',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
             'fieldtype' => 'select',
             'options' => 
            array (
              0 => 
              array (
                'key' => 'Owner/CEO',
                'value' => 'Owner/CEO',
              ),
              1 => 
              array (
                'key' => 'Management',
                'value' => 'Management',
              ),
              2 => 
              array (
                'key' => 'Buyer',
                'value' => 'Buyer',
              ),
              3 => 
              array (
                'key' => 'Designer',
                'value' => 'Designer',
              ),
              4 => 
              array (
                'key' => 'Developer',
                'value' => 'Developer',
              ),
              5 => 
              array (
                'key' => 'Marketing',
                'value' => 'Marketing',
              ),
              6 => 
              array (
                'key' => 'Sales',
                'value' => 'Sales',
              ),
            ),
             'width' => 500,
             'defaultValue' => '',
             'optionsProviderClass' => '',
             'optionsProviderData' => '',
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'dynamicOptions' => false,
             'name' => 'jobRole',
             'title' => 'jobRole',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          3 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
             'fieldtype' => 'select',
             'options' => 
            array (
              0 => 
              array (
                'key' => 'DIY/Garden Centre',
                'value' => 'DIY/Garden Centre',
              ),
              1 => 
              array (
                'key' => 'MAss Merchant/Warehouse',
                'value' => 'MAss Merchant/Warehouse',
              ),
              2 => 
              array (
                'key' => 'Supermarket',
                'value' => 'Supermarket',
              ),
              3 => 
              array (
                'key' => 'Department Store',
                'value' => 'Department Store',
              ),
              4 => 
              array (
                'key' => 'Commercial Interior',
                'value' => 'Commercial Interior',
              ),
              5 => 
              array (
                'key' => 'Homeware/Interior Designer',
                'value' => 'Homeware/Interior Designer',
              ),
              6 => 
              array (
                'key' => 'HoReCa',
                'value' => 'HoReCa',
              ),
              7 => 
              array (
                'key' => 'Individual',
                'value' => 'Individual',
              ),
              8 => 
              array (
                'key' => 'Other',
                'value' => 'Other',
              ),
            ),
             'width' => 500,
             'defaultValue' => '',
             'optionsProviderClass' => '',
             'optionsProviderData' => '',
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'dynamicOptions' => false,
             'name' => 'companyType',
             'title' => 'companyType',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          4 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
             'fieldtype' => 'select',
             'options' => 
            array (
              0 => 
              array (
                'key' => '1-10',
                'value' => '1-10',
              ),
              1 => 
              array (
                'key' => '11-100',
                'value' => '11-100',
              ),
              2 => 
              array (
                'key' => '101-250',
                'value' => '101-250',
              ),
              3 => 
              array (
                'key' => '251-500',
                'value' => '251-500',
              ),
              4 => 
              array (
                'key' => '501-1000',
                'value' => '501-1000',
              ),
              5 => 
              array (
                'key' => '1001+',
                'value' => '1001+',
              ),
            ),
             'width' => 500,
             'defaultValue' => '',
             'optionsProviderClass' => '',
             'optionsProviderData' => '',
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'dynamicOptions' => false,
             'name' => 'companySize',
             'title' => 'companySize',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          5 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
             'fieldtype' => 'input',
             'width' => 500,
             'defaultValue' => NULL,
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => false,
             'showCharCount' => false,
             'name' => 'companyName',
             'title' => 'Company Name',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
          6 => 
          Pimcore\Model\DataObject\ClassDefinition\Data\Email::__set_state(array(
             'fieldtype' => 'email',
             'width' => 500,
             'defaultValue' => NULL,
             'queryColumnType' => 'varchar',
             'columnType' => 'varchar',
             'columnLength' => 190,
             'phpdocType' => 'string',
             'regex' => '',
             'unique' => NULL,
             'showCharCount' => NULL,
             'name' => 'email',
             'title' => 'Email',
             'tooltip' => '',
             'mandatory' => true,
             'noteditable' => true,
             'index' => false,
             'locked' => false,
             'style' => '',
             'permissions' => NULL,
             'datatype' => 'data',
             'relationType' => false,
             'invisible' => false,
             'visibleGridView' => false,
             'visibleSearch' => false,
             'defaultValueGenerator' => '',
          )),
        ),
         'locked' => false,
         'icon' => '',
      )),
    ),
     'locked' => false,
     'icon' => NULL,
  )),
   'icon' => '',
   'previewUrl' => '',
   'group' => 'Master',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'generateTypeDeclarations' => false,
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'dao' => NULL,
));
