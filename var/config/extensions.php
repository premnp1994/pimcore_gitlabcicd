<?php

return [
    "bundle" => [
        "CustomerManagementFrameworkBundle\\PimcoreCustomerManagementFrameworkBundle" => TRUE,
        "Pimcore\\Bundle\\EcommerceFrameworkBundle\\PimcoreEcommerceFrameworkBundle" => TRUE
    ],
    "areabrick" => [
        "crousel" => TRUE
    ]
];
