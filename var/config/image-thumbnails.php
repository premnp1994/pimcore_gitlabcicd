<?php 

return [
    "BannerImage" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 500,
                    "height" => 500,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "BannerImage",
        "description" => "",
        "group" => "",
        "format" => "ORIGINAL",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1614256209,
        "creationDate" => 1614255351,
        "forcePictureTag" => FALSE,
        "id" => "BannerImage"
    ],
    "ThumbnailImage" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 300,
                    "height" => 300,
                    "forceResize" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "ThumbnailImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623211247,
        "creationDate" => 1614255374,
        "forcePictureTag" => FALSE,
        "id" => "ThumbnailImage"
    ],
    "productListImage" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 410,
                    "height" => 410,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productListImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623734139,
        "creationDate" => 1623212008,
        "forcePictureTag" => FALSE,
        "id" => "productListImage"
    ],
    "productDetailMainImage" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 737,
                    "height" => 737,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productDetailMainImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623218347,
        "creationDate" => 1623212065,
        "forcePictureTag" => FALSE,
        "id" => "productDetailMainImage"
    ],
    "productDetailThumbnailImage" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 85,
                    "height" => 85,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productDetailThumbnailImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623218302,
        "creationDate" => 1623212081,
        "forcePictureTag" => FALSE,
        "id" => "productDetailThumbnailImage"
    ],
    "recentlyViewedProductImage" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 351,
                    "height" => 351,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "recentlyViewedProductImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623302837,
        "creationDate" => 1623302817,
        "forcePictureTag" => FALSE,
        "id" => "recentlyViewedProductImage"
    ],
    "galleryCarousel" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 1852,
                    "height" => 748
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "galleryCarousel",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1624509206,
        "creationDate" => 1623405942,
        "forcePictureTag" => FALSE,
        "id" => "galleryCarousel"
    ],
    "galleryCarouselPreview" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 100,
                    "height" => 54,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "galleryCarouselPreview",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1623406239,
        "creationDate" => 1623406214,
        "forcePictureTag" => FALSE,
        "id" => "galleryCarouselPreview"
    ],
    "CaseStudySlider" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 1852,
                    "height" => 748,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "name" => "CaseStudySlider",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1624883723,
        "creationDate" => 1624542058,
        "forcePictureTag" => FALSE,
        "id" => "CaseStudySlider"
    ],
    "CaseStudyBlocks" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 484,
                    "height" => 268,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "name" => "CaseStudyBlocks",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1624542548,
        "creationDate" => 1624542274,
        "forcePictureTag" => FALSE,
        "id" => "CaseStudyBlocks"
    ],
    "CaseStudyPopup" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 1346,
                    "height" => 543,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "name" => "CaseStudyPopup",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1624542643,
        "creationDate" => 1624542621,
        "forcePictureTag" => FALSE,
        "id" => "CaseStudyPopup"
    ],
    "socialIcon" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 32,
                    "height" => 32,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "socialIcon",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1624884748,
        "creationDate" => 1624884731,
        "forcePictureTag" => FALSE,
        "id" => "socialIcon"
    ],
    "leadership" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 444,
                    "height" => 440,
                    "positioning" => NULL,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "leadership",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1625126149,
        "creationDate" => 1625126132,
        "forcePictureTag" => FALSE,
        "id" => "leadership"
    ],
    "serviceImage" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 138,
                    "height" => 127,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "serviceImage",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1625146314,
        "creationDate" => 1625146273,
        "forcePictureTag" => FALSE,
        "id" => "serviceImage"
    ]
];
