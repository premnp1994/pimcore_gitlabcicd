$(document).ready(function() {
	var sync1 = $("#syncs1");
	var sync2 = $("#syncs2");
	var slidesPerPage = 4; //globaly define number of elements per page
	var syncedSecondary = true;

	sync1.owlCarousel({
		items: 1,
		slideSpeed: 2000,
		nav: true,
		autoplay: false, 
		dots: true,
		loop: true,
		responsiveRefreshRate: 200,
		navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	}).on('changed.owl.carousel', syncPosition);

	sync2
		.on('initialized.owl.carousel', function() {
			sync2.find(".owl-item").eq(0).addClass("current");
		})
		.owlCarousel({
			items: slidesPerPage,
			dots: true,
			nav: true,
			smartSpeed: 200,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			slideSpeed: 500,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        992:{
            items:3.99
        }
    },
			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
			responsiveRefreshRate: 100
		}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		//var current = el.item.index;

		//if you disable loop you have to comment this block
		var count = el.item.count - 1;
		var current = Math.round(el.item.index - (el.item.count / 2) - .5);

		if (current < 0) {
			current = count;
		}
		if (current > count) {
			current = 0;
		}

		//end block

		sync2
			.find(".owl-item")
			.removeClass("current")
			.eq(current)
			.addClass("current");
		var onscreen = sync2.find('.owl-item.active').length - 1;
		var start = sync2.find('.owl-item.active').first().index();
		var end = sync2.find('.owl-item.active').last().index();

		if (current > end) {
			sync2.data('owl.carousel').to(current, 100, true);
		}
		if (current < start) {
			sync2.data('owl.carousel').to(current - onscreen, 100, true);
		}
	}

	function syncPosition2(el) {
		if (syncedSecondary) {
			var number = el.item.index;
			sync1.data('owl.carousel').to(number, 100, true);
		}
	}

	sync2.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).index();
		sync1.data('owl.carousel').to(number, 300, true);
	});
	
	
	
	var sync12 = $("#syncs1-2");
	var sync22 = $("#syncs2-2");
	var slidesPerPage = 4; //globaly define number of elements per page
	var syncedSecondary = true;

	sync12.owlCarousel({
		items: 1,
		slideSpeed: 2000,
		nav: true,
		autoplay: false, 
		dots: true,
		loop: true,
		responsiveRefreshRate: 200,
		navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	}).on('changed.owl.carousel', syncPosition);

	sync22
		.on('initialized.owl.carousel', function() {
			sync22.find(".owl-item").eq(0).addClass("current");
		})
		.owlCarousel({
			items: slidesPerPage,
			dots: true,
			nav: true,
			smartSpeed: 200,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			slideSpeed: 500,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        992:{
            items:3.99
        }
    },
			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
			responsiveRefreshRate: 100
		}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		//var current = el.item.index;

		//if you disable loop you have to comment this block
		var count = el.item.count - 1;
		var current = Math.round(el.item.index - (el.item.count / 2) - .5);

		if (current < 0) {
			current = count;
		}
		if (current > count) {
			current = 0;
		}

		//end block

		sync22
			.find(".owl-item")
			.removeClass("current")
			.eq(current)
			.addClass("current");
		var onscreen = sync22.find('.owl-item.active').length - 1;
		var start = sync22.find('.owl-item.active').first().index();
		var end = sync22.find('.owl-item.active').last().index();

		if (current > end) {
			sync22.data('owl.carousel').to(current, 100, true);
		}
		if (current < start) {
			sync22.data('owl.carousel').to(current - onscreen, 100, true);
		}
	}

	function syncPosition2(el) {
		if (syncedSecondary) {
			var number = el.item.index;
			sync12.data('owl.carousel').to(number, 100, true);
		}
	}

	sync22.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).index();
		sync12.data('owl.carousel').to(number, 300, true);
	});
	
	
	var sync13 = $("#syncs1-3");
	var sync23 = $("#syncs2-3");
	var slidesPerPage = 4; //globaly define number of elements per page
	var syncedSecondary = true;

	sync13.owlCarousel({
		items: 1,
		slideSpeed: 2000,
		nav: true,
		autoplay: false, 
		dots: true,
		loop: true,
		responsiveRefreshRate: 200,
		navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	}).on('changed.owl.carousel', syncPosition);

	sync23
		.on('initialized.owl.carousel', function() {
			sync23.find(".owl-item").eq(0).addClass("current");
		})
		.owlCarousel({
			items: slidesPerPage,
			dots: true,
			nav: true,
			smartSpeed: 200,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			slideSpeed: 500,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        992:{
            items:3.99
        }
    },
			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
			responsiveRefreshRate: 100
		}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		//var current = el.item.index;

		//if you disable loop you have to comment this block
		var count = el.item.count - 1;
		var current = Math.round(el.item.index - (el.item.count / 2) - .5);

		if (current < 0) {
			current = count;
		}
		if (current > count) {
			current = 0;
		}

		//end block

		sync23
			.find(".owl-item")
			.removeClass("current")
			.eq(current)
			.addClass("current");
		var onscreen = sync23.find('.owl-item.active').length - 1;
		var start = sync23.find('.owl-item.active').first().index();
		var end = sync23.find('.owl-item.active').last().index();

		if (current > end) {
			sync23.data('owl.carousel').to(current, 100, true);
		}
		if (current < start) {
			sync23.data('owl.carousel').to(current - onscreen, 100, true);
		}
	}

	function syncPosition2(el) {
		if (syncedSecondary) {
			var number = el.item.index;
			sync13.data('owl.carousel').to(number, 100, true);
		}
	}

	sync23.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).index();
		sync13.data('owl.carousel').to(number, 300, true);
	});
	
	
	var sync14 = $("#syncs1-4");
	var sync24 = $("#syncs2-4");
	var slidesPerPage = 4; //globaly define number of elements per page
	var syncedSecondary = true;

	sync14.owlCarousel({
		items: 1,
		slideSpeed: 2000,
		nav: true,
		autoplay: false, 
		dots: true,
		loop: true,
		responsiveRefreshRate: 200,
		navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	}).on('changed.owl.carousel', syncPosition);

	sync24
		.on('initialized.owl.carousel', function() {
			sync24.find(".owl-item").eq(0).addClass("current");
		})
		.owlCarousel({
			items: slidesPerPage,
			dots: true,
			nav: true,
			smartSpeed: 200,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			slideSpeed: 500,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        992:{
            items:3.99
        }
    },
			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
			responsiveRefreshRate: 100
		}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		//var current = el.item.index;

		//if you disable loop you have to comment this block
		var count = el.item.count - 1;
		var current = Math.round(el.item.index - (el.item.count / 2) - .5);

		if (current < 0) {
			current = count;
		}
		if (current > count) {
			current = 0;
		}

		//end block

		sync24
			.find(".owl-item")
			.removeClass("current")
			.eq(current)
			.addClass("current");
		var onscreen = sync24.find('.owl-item.active').length - 1;
		var start = sync24.find('.owl-item.active').first().index();
		var end = sync24.find('.owl-item.active').last().index();

		if (current > end) {
			sync24.data('owl.carousel').to(current, 100, true);
		}
		if (current < start) {
			sync24.data('owl.carousel').to(current - onscreen, 100, true);
		}
	}

	function syncPosition2(el) {
		if (syncedSecondary) {
			var number = el.item.index;
			sync14.data('owl.carousel').to(number, 100, true);
		}
	}

	sync24.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).index();
		sync14.data('owl.carousel').to(number, 300, true);
	});
	
});