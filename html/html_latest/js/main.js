$(document).ready(function(){

    // Stuff to do as soon as the DOM is ready;
    var currentYear = (new Date).getFullYear();
    $(".c-year").text( currentYear );

    // logo slider
    $('.banner-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout: 5000,
        // margin:10,
        nav:false,
        dots:true,
        items:1,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        responsive:{
            
        }
    });

      // OUR FAVORITE FINDS
      $('.favorite-finds-slider').owlCarousel({
        loop:true,
        autoplay:false,
        smartSpeed: 1200,
        autoplayTimeout: 7000,
        margin:75,
        nav:true,
        dots:false,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            992:{
                items:4
            }
        }
    });

       // RECOMMENDED FOR YOU
       $('.recom-for-you-slider').owlCarousel({
        loop:true,
        autoplay:false,
        smartSpeed: 1200,
        autoplayTimeout: 7000,
        margin:75,
        nav:true,
        dots:false,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            992:{
                items:3.99
            }
        }
    });

          // RECENTLY VIEWED
          $('.recently-viewed-slider').owlCarousel({
            loop:true,
            autoplay:false,
            smartSpeed: 1200,
            autoplayTimeout: 7000,
            margin:75,
            nav:true,
            dots:false,
            navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:3
                },
                992:{
                    items:3.99
                }
            }
        });

    // portfolio filter function

    // logo slider
    $('.home-brand-slider').owlCarousel({
        loop:true,
        autoplay:false,
        smartSpeed: 1200,
        autoplayTimeout: 7000,
        margin:0,
        nav:true,
        dots:false,
        navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            992:{
                items:4
            }
        }
    });

    // portfolio filter function

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

// End portfolio filter function

//smooth scroll
$('.links a[href^="#"]').on('click',function (e) {
  e.preventDefault();
  var target = this.hash;
  var $target = $(target);
  $('html, body').stop().animate({
      'scrollTop': $target.offset().top
  }, 900, 'swing', function () {
      // window.location.hash = target;
  });
});
//End smooth scroll


// date range function
$(function() {
    $('input[name="daterange"]').daterangepicker({
      opens: 'right',
      showDropdowns: true,
    }, function(start, end, label) {
      console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
  });
  

  //single date range function
  $(function() {
    $('input[name="birthday"]').daterangepicker({
      opens: 'right',
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: parseInt(moment().format('YYYY'),10)
    }, function(start, end, label) {
      var years = moment().diff(start, 'years');
      alert("You are " + years + " years old!");
    });
  });


    // lightbox options
    lightbox.option({
        'resizeDuration': 100
      })

    // loader
    setTimeout(function(){
        $(".bs-loader").css({"display": "none"});
    }, 1500);
    
    $(".select-auto").select2({
      tags: true,
      minimumResultsForSearch: -1,
      placeholder: "Type of enquiry",
    });
	
    $(".select-auto2").select2({
      tags: true,
      minimumResultsForSearch: -1,
      placeholder: "Signature",
    });
	
    $(".select-auto3").select2({
      tags: true,
      minimumResultsForSearch: -1,
      placeholder: "Monthly",
    });
    
    $(".select-job-role").select2({
      tags: true,
      minimumResultsForSearch: -1,
      // placeholder: "Job Role",
    });

    $(".type-of-company").select2({
      tags: true,
      minimumResultsForSearch: -1,
      // placeholder: "Type Of Company",
    });
    
    $(".company-size").select2({
      tags: true,
      minimumResultsForSearch: -1,
      // placeholder: "Company Size",
    });

    AOS.init({
      // disable: 'mobile'
    }); 
  
    $('.forgot-pass').on('click', function (e) {
      e.preventDefault();
      $('#tabs li.active').removeClass('active').addClass('hide');
      $('#password').tab('show')
    })

    $('input[name=customer]').change(function(){
      var value = $(this).val();
      $('#customer-selection').hide();
      $('#manufacturer').show();
    });

   

});
const file = document.querySelector('#portfolio-upload');
file.addEventListener('change', (e) => {
  const [file] = e.target.files;
  const { name: fileName, size } = file;
  // Convert size in bytes to kilo bytes
  const fileSize = (size / 1000).toFixed(2);
  // Set the text content
  const fileNameAndSize = `${fileName} - ${fileSize}KB`;
  document.querySelector('.file-upload-label').textContent = fileNameAndSize;
  $('.remove-file').addClass('attached');
});
document.querySelector('.remove-file').addEventListener("click", (e) => {
  document.querySelector('.file-upload-label').textContent = "Pls link or attach your portfolio";
  $('.remove-file').removeClass('attached');
  const fileInput = document.querySelector('#portfolio-upload');
  fileInput.value="";
  /*var selectedFile = fileInput.files[0];
  console.log(selectedFile);
  fileInput.files = [];
  selectedFile = fileInput.files[0];
  console.log(selectedFile);*/
});






