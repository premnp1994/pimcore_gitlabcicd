<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\AppHelpers;

use AppBundle\ExceptionHandlers\BadRequestException;
use AppBundle\ExceptionHandlers\PreConditionFailedException;
use Doctrine\DBAL\Schema\AbstractAsset;
use Pimcore\Db;
use Pimcore\Model\DataObject\Service;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\User;
use Pimcore\Model\User\Role;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Tool;
use Pimcore\Model\Asset;
use Symfony\Component\Security\Csrf;
use DateTime;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use Pimcore\Navigation\Page\Document as NavDocument;
use GlobalBundle\Website\LinkGenerator\CategoryLinkGenerator;

class GeneralHelper
{
    /**
     * @Functional Description: Print Array in format without exit
     * @Created by : Rajeshsingh Chauhan
     * @param $obj
     * @param int $ex
     */
    public static function pr($obj, $ex = 0)
    {
        echo "<pre>";
        print_r($obj);
        echo "</pre>";
        if ($ex) {
            exit;
        }
    }

    /**
     * @Functional Description: Print Array in format with exit
     * @Created by : Rajeshsingh Chauhan
     * @param $arr
     */
    public static function prex($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        exit;
    }

    /**
     * @Functional Description: Check required params validation for request
     * @param array $requestPara
     * @param array $require
     * @param string $validationType
     * @param string $language
     * @return array
     */
    public static function checkRequiredField($requestPara = array(), $require = array(), $validationType = "Data is missing in request!", $language = "en")
    {
        $errCount = 0;
        $msg = [];
        foreach ($require as $val) {
            if (!isset($requestPara[$val]) || $requestPara[$val] == '') {
                $errCount++;
                $msg[] = $val . ' ' . self::getTranslationFromKey("is_required", $language);
            }
        }
        return array('errors' => $errCount, "validationType" => $validationType, 'msg' => $msg);
    }

    /**
     * @Functional Description: Convert object into array
     * @param $obj
     * @param $arr
     * @return mixed
     */
    public static function objToArray($obj, &$arr)
    {
        $t = array('scheduledTasks', 'lazyLoadedFields');
        if (!is_object($obj) && !is_array($obj)) {
            return $obj;
        }
        foreach ($obj as $key => $value) {
            if ((strpos($key, 'o_') === false || strpos($key, 'o_id') !== false) && strpos($key, '____pimcore') === false) {
                if (!empty($value)) {
                    $arr[$key] = array();
                    self::objToArray($value, $arr[$key]);
                } else {
                    if (!in_array($key, $t)) {
                        $arr[$key] = $value;
                    }
                }
            }
        }
        return $arr;
    }


    /**
     * @Functional Description: get Translation from key
     * @param $key
     * @param string $language
     * @return mixed|string
     */
    public static function getTranslationFromKey($key, $language = 'en')
    {
        $db = Db::get();
        $languages = Tool::getValidLanguages();
        $exists = in_array($language, $languages);
        if (empty($exists)) {
            $language = "en";
        }
        $result = $db->fetchOne("SELECT `text` FROM translations_website where `key` = ? AND `language` = ?", [$key, $language]);
        if ($result) {
            return $result;
        } else {
            return $key;
        }
    }

    /**
     * @Functional Description: get request data
     * @return false|string
     */
    public static function getRequestJson()
    {
        if (!empty($_REQUEST)) {
            return json_encode($_REQUEST);
        } else {
            return json_encode(json_decode(file_get_contents('php://input'), true));
        }
    }

    /**
     * @Functional Description: get value of parent
     * @param $classname
     * @param $value
     * @return mixed
     */
    public static function getParentObject($classname, $value)
    {
        $dataObjectPath = "\\Pimcore\\Model\\DataObject\\";
        $classObj = $dataObjectPath . $classname;
        $classObj = new $classObj;
        return $classObj = $classObj::getByName($value, 1);
    }

    /**
     * @Functional Description: get selected language value
     * @return string
     */
    public static function getRequestedLanguage()
    {
        $lang = 'es';
        if (function_exists('getallheaders')) {
            $headers = getallheaders(); //get header to get request language
            foreach ($headers as $header => $value) {
                if (isset($header) && strtolower($header) === "language-code" && $value != "" && in_array($value, Tool::getValidLanguages())) {
                    $lang = $value;
                }
            }
        }
        return $lang;
    }

    /**
     * @Functional Description: Trims empty spaces in array elements (recursively trim multidimesional arrays)
     * @param $data
     * @return array|string|null
     *
     */
    public static function trimData($data)
    {
        if ($data == null) {
            $result = "";
        } elseif (is_array($data)) {
            $result = array_map('self::trimData', $data);
        } elseif (is_string($data)) {
            $result = trim($data);
        } else {
            $result = $data;
        }

        return $result;
    }

    /**
     * @Functional Description: Get Website setting from key.
     * @param $key
     * @param string $returnValue
     * @return mixed|string
     */
    public static function getWebsiteSetting($key, $returnValue = "N/A")
    {
        $response = WebsiteSetting::getByName($key);
        if ($response && $response->getData()) {
            $returnValue = $response->getData();
        }
        return $returnValue;
    }

    /**
     * @Functional Description: Send messages.
     *
     * @param $number
     * @param $message
     * @param string $lang
     * @return array
     * @throws PreConditionFailedException
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public static function sendMessage($number,$message,$brandObj = array(),$lang = 'en'){
        // Your Account SID and Auth Token from twilio.com/console
        $account_sid = self::getWebsiteSetting('twilio_account_sid');
        $auth_token = self::getWebsiteSetting('twilio_auth_token');

        // A Twilio number you own with SMS capabilities
        $twilio_number = self::getWebsiteSetting('twilio_number');
        if(!empty($brandObj) && $brandObj->getTwilioEnabled()){
            $account_sid = $brandObj->getAccountSID();
            $auth_token = $brandObj->getAuthToken();
            $twilio_number = $brandObj->getTwilioNumber();
        }
        $client = new Client($account_sid,$auth_token);

        try {
            $message = $client->messages->create(
            // Where to send a text message (your cell phone?)
                $number,
                array(
                    'from' => $twilio_number,
                    'body' => $message
                )
            );
            return array('status'=>$message->status,'sid'=>$message->sid);
        } catch (TwilioException $exception) {
            $translation = self::getTranslationFromKey("twilio_error", $lang) . " - " . $exception->getMessage();
            throw new PreConditionFailedException($translation);
        }
    }

    /**
     * Sending email
     *
     * @param $email
     * @param null $template
     * @param null $params
     * @param null $subject
     * @return bool
     * @throws \Exception
     */
    public static function sendMail($email, $template = null, $params = null, $subject = null, $attachment = null)
    {
        try {
            $template = (!empty($template)) ? $template : '';
            $params = (!empty($params)) ? $params : '';
            if (!empty($email)) {
                $mail = new \Pimcore\Mail();
                $mail->addTo($email);
                $mail->setParams($params);
                $mail->setDocument($template);
                $mail->setSubject($subject);
                if($attachment){
                    $mail->createAttachment($attachment->getData(),'text/plain',$attachment->getFilename());
                }
                $mail->send();
                $data = "EmailsResponse==>".$mail;
//                file_put_contents('Email.log', $data.PHP_EOL, FILE_APPEND);
                if ($mail) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $data = "EmailsResponseError==>".json_encode($e->getMessage());
            file_put_contents('Email.log', $data.PHP_EOL, FILE_APPEND);
            \Pimcore\Log\Simple::log('Error in (' . __FILE__ . ' >>> ' . __FUNCTION__ . ')', $e->getMessage());
        }
    }

    /**
     * Sending email
     *
     * @param null $email
     * @param null $template
     * @param null $params
     *
     * @return bool
     *
     * @throws \Exception
     */
    public static function sendAttachedNotifyMails($dmEmail, $emails, $param, $subject, $sheetInfo, $template = null)
    {
        try {
            $mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            $template = (!empty($template)) ? $template : '';
            $param = (!empty($param)) ? $param : '';
            $response["success"] = false;
            $response["msg"] = "No email attached for ";

            if (!empty($dmEmail)) {
                $mail = new \Pimcore\Mail();
                $mail->addTo($dmEmail);
                if (!empty($emails)) {
                    foreach ($emails as $email) {
                        $mail->addCc($email);
                    }
                }
                $mail->setDocument($template);
                $mail->setParams($param);
                $mail->setSubject($subject);
                $mail->createAttachment($sheetInfo['sheet'], $mimeType, $sheetInfo['fileName']);
                $mail->send();
                $response["success"] = false;
                $response["msg"] = "Email sending failed for ";
                if ($mail) {
                    $response["success"] = true;
                    $response["msg"] = "Email sending success for ";
                }
            }

            return $response;
        } catch (Exception $e) {
            \Pimcore\Log\Simple::log('Error in (' . __FILE__ . ' >>> ' . __FUNCTION__ . ')', $e->getMessage());
        }
    }

    /**
     * @Functional Description: Create folder in data objects if not exists
     * @param $path
     * @return string $folder
     */
    public static function validateFolderPath($path)
    {
        try {
            if (empty(Service::pathExists($path))) {
                $folder = Service::createFolderByPath($path);
            } else {
                $folder = AbstractObject::getByPath($path);
            }
            return $folder;
        } catch (\Exception $e) {
        }
    }


    /**
     * @Functional Description: Create folder in asset if not exists
     * @param $path
     * @return string $folder
     */
    public static function validateAssetFolderPath($path)
    {
        try {
            if (empty(Asset\Service::pathExists($path))) {
                $folder = Asset\Service::createFolderByPath($path);
            } else {
                $folder = Asset::getByPath($path);
            }
            return $folder;
        } catch (\Exception $e) {
        }
    }

    /**
     * @Functional Description: This function decodes base64 format image
     * @param string $base64Data base64 image data
     * @return array
     */
    public static function decodeBase64Image($base64Data)
    {
        $pos = strpos($base64Data, ';');
        $mimeType = explode(':', substr($base64Data, 0, $pos))[1];
        $mime_split = explode('/', $mimeType);
        $extension = $mime_split[1];
        list(, $base64Data) = explode(';', $base64Data);
        list(, $base64Data) = explode(',', $base64Data);
        $base64Data = base64_decode($base64Data);
        return array("decodedBase64Data" => $base64Data, "extension" => $extension, "mimeType" => $mimeType);
    }

    /**
     * @Functional Description: check allowed file extension
     * @param $base64DecodedData
     * @param $lang
     * @param string $contentType
     * @return int|string
     * @throws BadRequestException
     */
    public static function validateBase64MimeType($base64DecodedData, $lang, $contentType)
    {
        $isValid = false;
        $extension = "";
        $f = finfo_open();
        $mimeType = finfo_buffer($f, $base64DecodedData, FILEINFO_MIME_TYPE);
        $allMimes = file_get_contents(VALIDATE_FILE_TYPE);
        $allMimes = json_decode($allMimes, true);
        if (isset($allMimes[$contentType])) {
            foreach ($allMimes[$contentType] as $key => $value) {
                if (in_array($mimeType, $value)) {
                    $isValid = true;
                    $extension = $key;
                }
            }
            if ($isValid) {
                return $extension;
            } else {
                $translation = self::getTranslationFromKey("invalid_file", $lang) . " - " . $contentType;
                throw new BadRequestException($translation);
            }
        } else {
            $translation = self::getTranslationFromKey("invalid_file_content_type", $lang) . " - " . $contentType;
            throw new BadRequestException($translation);
        }
    }

    /**
     * Description Get Asset URL
     * @param $asset
     * @return string|null
     */
    public static function getAssetFullUrl($asset) {
        if (is_string($asset)) {  //if passed asset path in place of asset
            $asset = Asset::getByPath($asset);
        }

        if ($asset == null) {
            return null;
        }

        if (CDN_ENABLED) {
            return PIMCORE_CDN_BASEURL . $asset->getPath() . urlencode($asset->getFilename());
        } elseif (S3_ENABLED) {
            return S3_PUBLIC_URL . $asset->getPath() . urlencode($asset->getFilename());
        } else {
            return \Pimcore\Tool::getHostUrl() . $asset->getPath() . urlencode($asset->getFilename());
        }
    }

    /**
     * Desc:- Upload gallery file
     * @param $uploadPath
     * @param $base64Data
     * @param $contentType
     * @param $lang
     * @param $gallery
     * @return Asset
     * @throws BadRequestException
     */
    public static function uploadFile($uploadPath, $base64Data, $contentType, $lang, $gallery = "")
    {
        try {
            $uploadPath = self::validateAssetFolderPath($uploadPath . $gallery . "/" . $contentType);
            $fileData = base64_decode($base64Data);
            $extension = self::validateBase64MimeType($fileData, $lang, $contentType);
            $fileName = uniqid() . "." . $extension;
            $asset = new Asset();
            $asset->setParent($uploadPath);
            $asset->setFilename($fileName);
            $asset->setData($fileData);
            $asset->save();
            return $asset;
        } catch (\Exception $ex) {
            $msg = $ex->getMessage();
            \Pimcore\Log\Simple::log('Error in (' . __FILE__ . ' >>> ' . __FUNCTION__ . ')', $msg);
            throw new BadRequestException($msg);
        }
    }

    /**
     * Desc:- Replace space from string
     * @param $string
     * @return string|string[]
     */
    public static function replaceSpace($string)
    {
        return str_replace(' ', '-', $string);
    }

    /**
     * Desc:- Remove special characters form string
     * @param $str
     * @return string|string[]
     */
    public static function removeSpecialChar($str)
    {
        return str_replace(array('`', '\'', '"', ';', '<', '>'), '', $str);
    }

    /**
     * @Functional Description: generate token
     *
     * @param $userId
     * @param bool $checkFirstLogin
     * @return string
     */
    public static function getToken($userId,$checkFirstLogin = true)
    {
        try {
            $user = User::getById($userId);
            $csrf = new Csrf\TokenGenerator\UriSafeTokenGenerator();
            $token = $csrf->generateToken();
            if($checkFirstLogin){
                $user->setIsFirstLogin(false);
            }
            $user->setUserToken($token);
            $user->setOmitMandatoryCheck(true);
            $user->save();
            return $token;
        } catch (\Exception $e) {
            \Pimcore\Log\Simple::log('Error in (' . __FILE__ . ' >>> ' . __FUNCTION__ . ')', $e->getMessage());
        }
    }

    /**
     * @Functional Description: validate token
     * @param bool $returnObj
     * @return bool
     */
    public static function validateToken($returnObj = false)
    {
        if (function_exists('getallheaders')) {
            $headers = getallheaders(); //header to get authorization token
            foreach ($headers as $header => $value) {
                if (isset($header) && strtolower($header) === "token" && $value != "") {
                    $apiToken = $value;
                    $userObj = User::getByUserToken($apiToken, 1);
                    if ($userObj instanceof User) {
                        if ($returnObj) {
                            return $userObj;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Desc:- To fetch data with Query Builder
     * @param int $classId
     * @param array $columns
     * @param string $condition
     * @param array $parameters
     * @param string $fetch
     *
     * @return mixed
     */
    public static function customQuery($classId, $columns, $condition, $parameters, $fetch = 'fetchAll')
    {
        try {
            $db = \Pimcore\Db::get();
            $table = 'object_query_' . $classId;
            $qb = $db->select()
                ->from(['o' => $table], $columns)
                ->columns($columns);

            if (!empty($condition) && !empty($parameters)) {
                $qb->where($condition)
                    ->setParameters($parameters);
            }

            $response = $qb->execute()->$fetch();
        } catch (\Exception $e) {
            \Pimcore\Log\Simple::log('Error in (' . __FILE__ . ' >>> ' . __FUNCTION__ . ')', $e->getMessage());
        }
        return $response;
    }

    /**
     * @param $array
     * @return mixed
     */
    public static function recursiveProcess($array)
    {
        foreach($array as $key => $value)
        {
            if(is_array($value))
            {
                $array[$key] = self::recursiveProcess($value);
            }
            else
            {
                $array[$key] = array_map('utf8_encode', array($key => $value));
            }
        }
        return $array;
    }

    /**
     * @param bool $returnUser
     * @return array|\Pimcore\Model\User|Role|null
     */
    public static function getBackendUserOrRole($returnUser = true){
        $currentUser = \Pimcore\Tool\Admin::getCurrentUser();
        if($returnUser){
            return $currentUser;
        }else{
            $roleId = $currentUser->getRoles();
            if(!empty($currentUser) && isset($roleId[0]) && !empty($roleId[0])){
                return  Role::getById($roleId[0]);
            }
            return array();
        }
    }

    /*
     * @param string $type
     * @param int $elmentId
     *
     * return string
     */
    public static function getElementTags($type,$elementId){
        $tagArray=array();
        $tags = \Pimcore\Model\Element\Tag::getTagsForElement($type,$elementId);
        $tagArray=array();
        foreach($tags as $tag){
            $tagArray[]= ucfirst($tag->getName());
        }
        return implode(",",$tagArray);
    }

    public static function getRecurciveCategoryOld($category,$categoryPage){

        foreach ($category->getChildren() as $subCategory) {

            $subCategoryPage = new NavDocument([
                'label' => $subCategory->getLabel(),
                'id' => 'category-' . $subCategory->getId(),
                'uri' => $subCategory->getLabel()
            ]);

            $categoryPage->addPage($subCategoryPage);
            if(!empty($subCategory->getChildren())){
                self::getRecurciveCategory($subCategory,$subCategoryPage);
            }
        }

        return $categoryPage;

    }

}

