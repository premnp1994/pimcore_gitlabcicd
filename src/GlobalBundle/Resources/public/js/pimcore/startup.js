pimcore.registerNS("pimcore.plugin.GlobalBundle");

pimcore.plugin.GlobalBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.GlobalBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
        // this.navEl = Ext.get('pimcore_menu_search').insertSibling('<li id="pimcore_menu_mds" data-menu-tooltip="Create Supplier User" class="pimcore_menu_item pimcore_menu_needs_children"><img src="/bundles/global/images/adduser.svg"></li>', 'before');
        // this.menu = new Ext.menu.Menu({
        //     cls: "pimcore_navigation_flyout"
        // });
        // pimcore.layout.toolbar.prototype.mdsMenu = this.menu;
    },
    postOpenObject: function (object, type) {
        if (type === 'object' && (object.data.general.o_className === 'SupplierCompany' || object.data.general.o_className === 'City'  || object.data.general.o_className === 'Company')) {
             
            var country = object.edit.dataFields['country'].component;
            var state = object.edit.dataFields['state'].component;
            if(object.data.general.o_className !== 'City'){
            var city = object.edit.dataFields['city'].component;
            }

            Ext.Ajax.request({
                url: '/getStates',
                method: 'POST',
                params: {
                    id: object.id,
                    country: country.getValue()
                },
                success: function (fp, o) {
                    var jso = JSON.parse(fp.responseText);
                    console.log(jso.success);
                    if (jso.success == true) {
                        var states = Ext.create('Ext.data.Store', {
                            fields: ['key', 'value'],
                            data: jso.list
                        });

                        state.setStore(states);
                    }

                },
                failure: function () { console.log('failure'); }
            });
            Ext.Ajax.request({
                url: '/getCities',
                method: 'POST',
                params: {
                    id: object.id,
                    state: state.getValue()
                },
                success: function (fp, o) {
                    var jso = JSON.parse(fp.responseText);
                    console.log(jso.success);
                    if (jso.success == true) {
                        var cities = Ext.create('Ext.data.Store', {
                            fields: ['key', 'value'],
                            data: jso.list
                        });

                        city.setStore(cities);
                    }

                },
                failure: function () { console.log('failure'); }
            });

            var state = object.edit.dataFields['state'].component;
            country.on("change", function (field, newValue, oldValue) {
                if (newValue != '') {
                    var states = Ext.create('Ext.data.Store', {
                        fields: ['key', 'value'],
                        data: ['','']
                    });
                    state.setStore(states);
                    Ext.Ajax.request({
                        url: '/getStates',
                        method: 'POST',
                        params: {
                            id: object.id,
                            country: newValue
                        },
                        success: function (fp, o) {
                            var jso = JSON.parse(fp.responseText);
                            console.log(jso.success);
                            if (jso.success == true) {
                                var states = Ext.create('Ext.data.Store', {
                                    fields: ['key', 'value'],
                                    data: jso.list
                                });

                                state.setStore(states);
                            }

                        },
                        failure: function () { console.log('failure'); }
                    });
                }
            }, this);
            state.on("change", function (field, newValue, oldValue) {

                if (newValue != '') {
                    Ext.Ajax.request({
                        url: '/getCities',
                        method: 'POST',
                        params: {
                            id: object.id,
                            state: newValue
                        },
                        success: function (fp, o) {
                            var jso = JSON.parse(fp.responseText);
                            console.log(jso.success);
                            if (jso.success == true) {
                                var cities = Ext.create('Ext.data.Store', {
                                    fields: ['key', 'value'],
                                    data: jso.list
                                });

                                city.setStore(cities);
                            }

                        },
                        failure: function () { console.log('failure'); }
                    });
                }
            }, this);
        }
        if (type === 'object' && object.data.general.o_className === 'Category') {
            var preset = object.edit.dataFields['preset'].component;
            preset.on("change", function (field, newValue, oldValue) {

                if (newValue != '') {
                    Ext.Ajax.request({
                        url: '/getDescription',
                        method: 'POST',
                        params: {
                            id: object.id,
                            presetId: newValue
                        },
                        success: function (fp, o) {
                            object.edit.layout.items.items[0].items.items[0].items.items[1].items.items[0].items.items[0].items.items[1].setValue(fp.responseText.replace(/['"]+/g, ''))
                        },
                        failure: function () { console.log('failure'); }
                    });
                } else {
                    object.edit.layout.items.items[0].items.items[0].items.items[1].items.items[0].items.items[0].items.items[1].setValue('');
                }

            }, this);

        }
        if (type === 'object' && object.data.general.o_className === 'Product') {

            var tabPanel = object.edit.layout.items.items[0];
            object.edit.layout.items.items[0].on(
                'tabchange', function (tabPanel, tab) {
                    if (tab.name == 'Extra Details') {
                        tab.items.items[0].items.items[0].on("change", function (fieldval, newval, oldval) {
                            console.log(newval)
                            if (newval != '') {
                                Ext.Ajax.request({
                                    url: '/getDescription',
                                    method: 'POST',
                                    params: {
                                        id: object.id,
                                        presetId: newval
                                    },
                                    success: function (fp, o) {
                                        tab.items.items[1].items.items[0].items.items[0].items.items[0].items.items[0].setValue(fp.responseText.replace(/['"]+/g, ''))
                                    },
                                    failure: function () { console.log('failure'); }
                                });
                            } else {
                                tab.items.items[1].items.items[0].items.items[0].items.items[0].items.items[0].setValue('');
                            }

                        }, this);
                        tab.items.items[0].items.items[1].on("change", function (fieldval, newval, oldval) {

                            console.log(newval)
                            if (newval != '') {
                                Ext.Ajax.request({
                                    url: '/getDescription',
                                    method: 'POST',
                                    params: {
                                        id: object.id,
                                        presetId: newval
                                    },
                                    success: function (fp, o) {
                                        tab.items.items[1].items.items[0].items.items[0].items.items[0].items.items[1].setValue(fp.responseText.replace(/['"]+/g, ''))
                                    },
                                    failure: function () { console.log('failure'); }
                                });
                            } else {
                                tab.items.items[1].items.items[0].items.items[0].items.items[0].items.items[1].setValue('');
                            }
                        }, this);
                    }
                    if (tab.name == 'Basic Details') {
                        tab.items.items[3].items.items[0].on("change", function (field, newValue, oldValue) {
                            if (newValue != '') {
                                Ext.Ajax.request({
                                    url: '/getDescription',
                                    method: 'POST',
                                    params: {
                                        id: object.id,
                                        presetId: newValue
                                    },
                                    success: function (fp, o) {
                                        console.log(object.edit.layout.items.items[0].items.items[0].items.items[4].items.items[0].items.items[0].items.items[1]);
                                        console.log(fp.responseText);
                                        tab.items.items[4].items.items[0].items.items[0].items.items[1].items.items[0].setValue(fp.responseText.replace(/['"]+/g, ''));
                                    },
                                    failure: function () { console.log('failure'); }
                                });
                            } else {
                                tab.items.items[4].items.items[0].items.items[0].items.items[1].items.items[0].setValue('');
                            }
                        }, this);
                        tab.items.items[3].items.items[1].on("change", function (fieldval, newval, oldval) {
                            if (newval != '') {
                                Ext.Ajax.request({
                                    url: '/getDescription',
                                    method: 'POST',
                                    params: {
                                        id: object.id,
                                        presetId: newval
                                    },
                                    success: function (fp, o) {
                                        tab.items.items[4].items.items[0].items.items[0].items.items[1].items.items[1].setValue(fp.responseText.replace(/['"]+/g, ''));
                                    },
                                    failure: function () { console.log('failure'); }
                                });
                            } else {
                                tab.items.items[4].items.items[0].items.items[0].items.items[1].items.items[1].setValue('');
                            }

                        }, this);
                    }
                }
            )


            if (object.edit.layout.items.items[0].items.items[0].name == 'Basic Details') {

                object.edit.layout.items.items[0].items.items[0].items.items[3].items.items[0].on("change", function (field, newValue, oldValue) {
                    if (newValue != '') {
                        Ext.Ajax.request({
                            url: '/getDescription',
                            method: 'POST',
                            params: {
                                id: object.id,
                                presetId: newValue
                            },
                            success: function (fp, o) {

                                object.edit.layout.items.items[0].items.items[0].items.items[4].items.items[0].items.items[0].items.items[1].items.items[0].setValue(fp.responseText.replace(/['"]+/g, ''));
                            },
                            failure: function () { console.log('failure'); }
                        });
                    } else {

                        object.edit.layout.items.items[0].items.items[0].items.items[4].items.items[0].items.items[0].items.items[1].items.items[0].setValue('');
                    }

                    //
                }, this);
                object.edit.layout.items.items[0].items.items[0].items.items[3].items.items[1].on("change", function (fieldval, newval, oldval) {

                    console.log(newval)
                    if (newval != '') {
                        Ext.Ajax.request({
                            url: '/getDescription',
                            method: 'POST',
                            params: {
                                id: object.id,
                                presetId: newval
                            },
                            success: function (fp, o) {

                                object.edit.layout.items.items[0].items.items[0].items.items[4].items.items[0].items.items[0].items.items[1].items.items[1].setValue(fp.responseText.replace(/['"]+/g, ''));
                            },
                            failure: function () { console.log('failure'); }
                        });
                    } else {

                        object.edit.layout.items.items[0].items.items[0].items.items[4].items.items[0].items.items[0].items.items[1].items.items[1].setValue('');
                    }

                }, this);
            }
        }
    },
    pimcoreReady: function (params, broker) {
      //  this.navEl = Ext.get('pimcore_menu_search').insertSibling('<li id="pimcore_menu_newuser" data-menu-tooltip="Supplier user" class="pimcore_menu_item pimcore_menu_needs_children true-initialized"><img src="/bundles/pimcorecustomermanagementframework/icons/outline-group-24px.svg"></li>', 'before');
        // alert("GlobalBundle ready!");
    }
});
var GlobalBundlePlugin = new pimcore.plugin.GlobalBundle();

