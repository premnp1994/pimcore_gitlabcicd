<?php

namespace GlobalBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Pimcore\Model\DataObject;

class DefaultController extends FrontendController
{
    /**
     * @Route("/global")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from global');
    }

    /**
     * @Route("/getDescription")
     */
    public function getDescription(Request $request)
    {
        $myObject = DataObject\Product::getById($request->get('id'));

        if (!empty($request->get('presetId'))) {
            $productData = DataObject\DescriptionPreset::getById($request->get('presetId'));

            $presetDescription = $productData->getDescription('en');
        }

        return $this->json($presetDescription);
    }

}
