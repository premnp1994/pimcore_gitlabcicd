<?php

namespace GlobalBundle\Controller;

use GlobalBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Pimcore\Model\DataObject\BookADemo;
use Pimcore\Model\DataObject\MiSourceContact;
use Pimcore\Model\DataObject\Folder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Pimcore\Model\DataObject;
use GlobalBundle\AppHelpers\GeneralHelper;


class HomeController extends BaseController
{
    /**
     * @Route("/global")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from global');
    }
	/**
	 * Description: Misource landing page
     * @Route("/")
     */
	public function defaultAction(Request $request)
    {
			
    }
	
	/**
     *  Description:- Misource landing page - Book A demo ajax submit action
     * 
     * @Route("/booking",name="bookAdemo");
     * @param Request $request
     * @return success or error msg
    */ 
	public function bookingAction(Request $request){
		 //return new Response('booking page');
		 
		 if($request->getMethod() === "POST") {
            if($request->get("submitAction") == 'bookAdemo') {
				$firstName = $request->get("firstName");
				$lastName = $request->get("lastName");
				$jobRole = $request->get("jobRole");
				$companyType = $request->get("companyType");
				$companySize = $request->get("companySize");
				$companyName = $request->get("companyName");
				$email = $request->get("email");
				
				$unique_key = "BD-".time();
				
				$c = new BookADemo();
				$c->setFirstName($firstName);
				$c->setLastName($lastName);
				$c->setJobRole($jobRole);
				$c->setCompanyType($companyType);
				$c->setCompanySize($companySize);
				$c->setCompanyName($companyName);
				$c->setEmail($email);
				$c->setParent(GeneralHelper::validateFolderPath(BOOKADEMO_PATH));
				$c->setKey($unique_key);
				$c->setPublished(true);
				try {
					$c->save();
					return new Response('success');
				} catch(\Exception $e) {
					return new Response('error');
				}
				
			}
		 }
		
	}
	
	/**
     *  Description:- Misource landing page - footer contact form ajax submit action
     * 
     * @Route("/contact",name="contactUs");
     * @param Request $request
     * @return success or error msg
    */ 
	
	public function contactAction(Request $request){
		 //return new Response('booking page');
		 
		 if($request->getMethod() === "POST") {
			 if($request->get("submitAction") == 'contactUs') {
				$message = $request->get("message");
				$name = $request->get("name");
				$email = $request->get("email");
				$inquiryType = $request->get("inquiryType");
				
				$unique_key = "CU-".time();				
				
				$cntus = new MiSourceContact();
				$cntus->setName($name);
				$cntus->setEmail($email);
				$cntus->setMessage($message);
				$cntus->setInquiryType($inquiryType);				
				$cntus->setParent(GeneralHelper::validateFolderPath(CONTACTUS_PATH));
				$cntus->setKey($unique_key);
				$cntus->setPublished(true);
				try {
					$cntus->save();
					return new Response('success');
				} catch(\Exception $e) {
					return new Response('error');
				}
			}
		 }
		 //return new Response($data);
	}

}
