<?php

namespace GlobalBundle\Providers;

use AppBundle\Logger\AppLogger;
use Pimcore\Db;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Templating\Helper\Cache;

/**
 * Class OptionsProvider
 * @package Cadence\
 */
class MultilingualActiveProvider implements SelectOptionsProviderInterface {
	const LOGGER_MODULE = 'Dynamic Select';
	const SEPARATOR = '||';
	const CACHE_TAGS = ['dynamic_dropdown_output_lifetime'];
	const CACHE_PRIORITY = 1000;

	/**
	 * @param array $context
	 * @param \Pimcore\Model\DataObject\ClassDefinition\Data\Select|string $fieldDefinition
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {
		$list = [];
        $locale = DEFAULT_LANGUAGE_IN_IMPORT;

		$dataProvider = $fieldDefinition->getOptionsProviderData();

		try {
			$masterData = explode(self::SEPARATOR, $dataProvider);

			if (empty(array_search("", $masterData))) {
				/** @var Concrete $masterClass */
				$masterClass = $masterData[0];
                $masterKeyGetter = $masterData[1];
                $masterValueGetter = $masterData[2];

				if (class_exists($masterClass) && !empty($masterClass)) {
					$masterClassName = (new \ReflectionClass($masterClass))->getShortName();
                    $cacheKey = $masterClassName.'_data_'.$locale;
					$masterClassObj = new $masterClass();
					$classId = strtoupper($masterClassObj->getClassId());
                    $masterKeyGetter = $classId.".".$masterKeyGetter;
                    $joinTableAlias = $classId."_".$locale;
                    $joinTable = "object_localized_{$classId}_".$locale." as {$joinTableAlias}";
                    $masterValueGetter = $joinTableAlias.".".$masterValueGetter;
                    $sql = "SELECT {$masterKeyGetter}  as `value`, {$masterValueGetter}  as `key` FROM object_{$classId} as {$classId}" .
                        " LEFT JOIN {$joinTable} on {$joinTableAlias}.oo_id = {$classId}.oo_id WHERE {$classId}.o_published = 1  ORDER BY `key`";
                    file_put_contents("master.log",json_encode($sql));
					$stmt = Db::get()->prepare($sql);
					$stmt->execute();

					$list = $stmt->fetchAll();
                    file_put_contents("option.log",json_encode($list));
					if ($fieldDefinition->getMandatory() && !empty($list)) {
						array_unshift($list, array("key" => "(Empty)", "value" => ""));
					}
//                    Cache::save($list, $cacheKey, self::CACHE_TAGS, null, self::CACHE_PRIORITY, true);
				} else {
					 AppLogger::log(self::LOGGER_MODULE)->error('Not a valid class. ' . $dataProvider);
				}
			} else {
				 AppLogger::log(self::LOGGER_MODULE)->error('Class or Key, Value must not be empty. ' . $dataProvider);
			}
		} catch (\Exception $e) {
			 AppLogger::log(self::LOGGER_MODULE)->error($e->getMessage() . $dataProvider);
		}
       // file_put_contents("list.log",json_encode($list));
		return $list;
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return mixed
	 */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}
}
