<?php
/**
 * @package BayerBundle
 *
 * Dynamic options provider class
 *
 * Service Name: \BayerBundle\Services\OptionsProvider
 * Option Provider Data: {"class": "Country","key": "name","value": "id","extra": "abbreviation"}
 *
 * @author Bhavika Matariya
 */


namespace GlobalBundle\Providers;

use PHPUnit\Exception;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Templating\Helper\Cache;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class SelectOptionProvider implements SelectOptionsProviderInterface
{

    CONST CACHE_TAGS = ['content'];
	CONST CACHE_PRIORITY = 1000;
    public function getOptions($context, $fieldDefinition)
    {
      
    
        $providerData = $fieldDefinition->optionsProviderData;
        if (empty($optionsData) && !empty($context)) {
            if(isset($context['listOptions'])) {
                $providerData = $context['optionsProviderData'];
            }
        }

        try {
            if (!empty($providerData)) {
                
                $providerData    = json_decode($providerData);
                $class           = $providerData->class;
                $orderKey        = $providerData->orderKey;
                $order           = $providerData->order;
                $objClassListing = '\\Pimcore\\Model\\DataObject\\' . $class . '\\Listing';
             //   $cacheKey = 'content_color_list';
                
                if (\Pimcore\Tool::classExists($objClassListing) ) {
                   
                    $optionsData = new $objClassListing();
                    if(!empty($providerData->type)){
                    $optionsData->setCondition("presetType = '".$providerData->type."'");
                    }
                    $optionsData->setOrderKey($orderKey ?: "o_id");
                    $optionsData->setOrder($order ?: "asc");
                    $optionsData->load();

                    if (!empty($optionsData)) {
                        $opKey   = trim($providerData->key) ? 'get' . ucfirst($providerData->key) : '';
                        $opValue = trim($providerData->value) ? 'get' . ucfirst($providerData->value) : '';
                        $opExtra = trim($providerData->extra) ? 'get' . ucfirst($providerData->extra) : '';

                        $keyArray = array();
                        $result   = array();

                        foreach ($optionsData as $options) {
                            if (method_exists($options, $opKey) && method_exists($options, $opValue)) {
                                $key   = $options->$opKey();
                                $value = $options->$opValue();
                                $extra = method_exists($options, $opExtra) && !empty($options->$opExtra()) ? " (" . $options->$opExtra() . ")" : '';

                                if (!key_exists($key, $keyArray) && !empty($options->$opKey()) && !empty($options->$opValue())) {
                                    $keyArray[$key] = $key;

                                    $result[] = array("key" => $key . $extra, "value" => $value);
                                }
                            }
                        }

                        return $result;
                    }
                }
            }
        } catch (Exception $e) {
            //skip
        }
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return true;
    }
}
