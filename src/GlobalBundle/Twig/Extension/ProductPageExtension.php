<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Twig\Extension;

use Aws\signer\signerClient;
use GlobalBundle\AppHelpers\GeneralHelper;
use GlobalBundle\Website\LinkGenerator\ProductLinkGenerator;
use Pimcore\Bundle\EcommerceFrameworkBundle\Model\ProductInterface;
use Pimcore\Model\DataObject\Customer;
use Pimcore\Translation\Translator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;


class ProductPageExtension extends AbstractExtension
{

    /**
     * @var ProductLinkGenerator
     */
    protected $productLinkGenerator;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * ProductPageExtension constructor.
     *
     * @param ProductLinkGenerator $productLinkGenerator
     * @param Translator $translator
     */
    public function __construct(ProductLinkGenerator $productLinkGenerator, Translator $translator)
    {
        $this->productLinkGenerator = $productLinkGenerator;
        $this->translator = $translator;
    }
    const OBJECT_TYPE_ACTUAL_PRODUCT = 'actual-product';

    public function getFunctions()
    {
        return [
            new TwigFunction('app_product_detaillink', [$this, 'generateLink']),
            new TwigFunction('app_product_filter_subscription', [$this, 'filterBySubscription']),
			new TwigFunction('app_product_materials',[$this, 'generateStringFromIdsArray']),
            new TwigFunction('app_product_materials_image',[$this, 'productMaterialsImage']),
            new TwigFunction('app_product_count',[$this, 'productCount']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('colorname', [$this, 'getColorName'])
        ];
    }

    /**
     * @param ProductInterface $product
     * @return string
     */
    public function generateLink(ProductInterface $product): string
    {
        return $this->productLinkGenerator->generateWithMockup($product, []);
    }



    public function filterBySubscription($filter){

        if(isset($_SESSION['_pimcore_admin']['customer']) && !empty($_SESSION['_pimcore_admin']['customer'])){
            $customer = $_SESSION['_pimcore_admin']['customer'];
            if(!empty($customer)){
                $subscriptionType = $customer->getCompany()->getSubscription();

                $curateFilter =  explode(',',GeneralHelper::getWebsiteSetting('CurateFilter'));
                $signatureFilter =  explode(',',GeneralHelper::getWebsiteSetting('SignatureFilter'));
                $essencialRemoveFilter = array_merge($curateFilter,$signatureFilter);

                $filterArray=array();
                foreach($filter->getItems() as $value){

                    if($subscriptionType == "Essential"){
                        if(!in_array($value->getField()->getField(),$essencialRemoveFilter)){
                            $filterArray[] = $value;
                        }
                     }elseif($subscriptionType == "Curate"){
                       if(!in_array($value->getField()->getField(),$signatureFilter)){
                            $filterArray[] = $value;
                       }
                    }else{
                        $filterArray[] = $value;
                    }

                }
                $filter->setItems($filterArray);
            }
        }

        return $filter;

    }

    /**
     * @param array|null $colorNames
     *
     * @return string
     */
    public function getColorName(?array $colorNames = []): string
    {
        $translatedColors = [];
        if ($colorNames) {
            foreach ($colorNames as $color) {
                $translatedColors[] = $this->translator->trans(mb_strtolower('attribute.' . $color));
            }
        }

        return implode('-', $translatedColors);
    }


	/**
     * @param array $IDs
     * @param string $className
     * @return string
     */
	public function generateStringFromIdsArray($IDs, $className){
		$items = '';
		if($IDs && $className){
			foreach($IDs as $value){
				$color[$value] = [];
				$objectStr =  'Pimcore\\Model\\DataObject\\'.ucfirst($className).'\\Listing';
				$object = new $objectStr();
				$object->setCondition(' o_id = ? ', [$value]);
				$object->setLimit(1);
				$loadObjectData = $object->load();

				if($className == 'color'){
					$items = $loadObjectData;
				}else{
					if(!empty($loadObjectData)){
						$items .= $loadObjectData[0]->getName().', ';
					}
				}

			}
		}
		return $items;
	}

    public function productMaterialsImage($IDs, $className){
        $items = '';
        if($IDs && $className){
            foreach($IDs as $value){
                $color[$value] = [];
                $objectStr =  'Pimcore\\Model\\DataObject\\'.ucfirst($className).'\\Listing';
                $object = new $objectStr();
                $object->setCondition(' o_id = ? ', [$value]);
                $object->setLimit(1);
                $loadObjectData = $object->load();

                if($className == 'color'){
                    $items = $loadObjectData;
                }else{
                    if(!empty($loadObjectData)){
                        $items .= $loadObjectData[0]->getImage().', ';
                    }
                }

            }
        }
        return $items;
    }

    public function productCount($productListing){
	    p_r($productListing); die;
        p_r($productListing->getTotalCount()); die;
	    die;
        return 11;
    }
}
