<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Twig\Extension;

use GlobalBundle\AppHelpers\GeneralHelper;
use GlobalBundle\Website\LinkGenerator\AbstractProductLinkGenerator;
use GlobalBundle\Website\LinkGenerator\CategoryLinkGenerator;
use GlobalBundle\Website\LinkGenerator\ProductLinkGenerator;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\SiteMenu;
use Pimcore\Model\Document;
use Pimcore\Navigation\Container;
use Pimcore\Navigation\Page\Document as NavDocument;
use Pimcore\Templating\Helper\Navigation;
use Pimcore\Templating\Helper\Placeholder;
use Pimcore\Translation\Translator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use AppBundle\Website\Tool\Text;


class NavigationExtension extends AbstractExtension
{
    const NAVIGATION_EXTENSION_POINT_PROPERTY = 'navigation_extension_point';

    /**
     * @var CategoryLinkGenerator
     */
    protected $categoryLinkGenerator;

    /**
     * @var ProductLinkGenerator
     */
    protected $productLinkGenerator;

    /**
     * @var Navigation
     */
    protected $navigationHelper;

    /**
     * @var Placeholder
     */
    protected $placeholderHelper;

    /**
     * @param Navigation $navigationHelper
     */
    public function __construct(Navigation $navigationHelper, Placeholder $placeholderHelper, CategoryLinkGenerator $categoryLinkGenerator, ProductLinkGenerator $productLinkGenerator,Translator $translator)
    {
        $this->navigationHelper = $navigationHelper;
        $this->categoryLinkGenerator = $categoryLinkGenerator;
        $this->productLinkGenerator = $productLinkGenerator;
        $this->placeholderHelper = $placeholderHelper;
        $this->translator = $translator;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('app_navigation_data_links', [$this, 'getDataLinks']),
            new TwigFunction('app_navigation_enrich_breadcrumbs', [$this, 'enrichBreadcrumbs']),
            new TwigFunction('app_navigation_menubar', [$this, 'getMenuBar']),
            new TwigFunction('app_navigation_product_details', [$this, 'getProductLinks']),
            new TwigFunction('app_general_filter_translate', [$this, 'translateValues']),
            new TwigFunction('app_general_filter_sort', [$this, 'sort']),
            new TwigFunction('app_general_filter_sort_objects', [$this, 'sortObjects']),
            new TwigFunction('app_general_filter_value_string', [$this, 'fromIdToValueStr']),
            new TwigFunction('pimcore_pagination_url', [$this, 'pimcorePaginationUrl']),
            new TwigFunction('app_navigation_category_url', [$this, 'getCategoryUrl']),
            new TwigFunction('app_navigation_sitemap_id', [$this, 'getSitemapId']),
            new TwigFunction('app_navigation_product_in_wishlist', [$this, 'getProductWishlistStatus']),
            new TwigFunction('app_navigation_check_user_subscription_type', [$this, 'checkUserSubscriptionType']),
            
            
        ];
    }

    /**
     * @param Document $document
     * @param Document $startNode
     *
     * @return \Pimcore\Navigation\Container
     */
    public function getDataLinks(Document $document, Document $startNode)
    {
        $navigation = $this->navigationHelper->build([
            'active' => $document,
            'root' => $startNode,
            'pageCallback' => function($page, $document) {
                $rootCategory = $document->getProperty(AbstractProductLinkGenerator::ROOT_CATEGORY_PROPERTY_NAME);

                foreach ($rootCategory->getChildren() as $category) {

                    $categoryPage = new NavDocument([
                            'label' => $category->getLabel(),
                            'id' => 'category-' . $category->getId(),
                            'uri' => $category->getLabel()
                        ]);

                        $page->addPage($categoryPage);
                    }
            }
        ]);

        return $navigation;
    }

    public function getCategoryUrl($sitemenu){
        $menu['label'] = $sitemenu->getLabel();
        $menu['id'] = $sitemenu->getId();
        if($sitemenu->getLevel() == 1 || $sitemenu->getLevel() == 2){
            $menu['url'] = "/shop/".Text::toUrl($sitemenu->getLabel() ?? 'elements')."~c".$sitemenu->getId();
        }else{
            $menu['url'] = ($sitemenu->getCategory() != NULL) ? $this->categoryLinkGenerator->generate($sitemenu,[],true):"#";
        }
        return $menu;
    }


    public function getSitemapId($siteMenuId,$category){
       
        if($siteMenuId){
            $siteMenu = SiteMenu::getById($siteMenuId);    
        }else{
            $siteMenu = SiteMenu::getByCategory($category,1);
            $siteMenuId = $siteMenu->getId();
        }
        
        if($siteMenu->getLevel() == 2){
            foreach($siteMenu->getChildren() as $value){
                if($value->getCategory()->getId() == $category->getId()){
                  $id = $value->getId();
                }
            }
        }elseif($siteMenu->getLevel() == 1){
            foreach($siteMenu->getChildren() as $value){
                foreach($value->getChildren() as $val){
                    if($val->getCategory()->getId() == $category->getId()){
                      $id = $val->getId();
                    }
                }
            }
        }else{
            $id = $siteMenuId;
        }
        
        return $id;
    }

    public function getMenuBar(){
        $siteMenu = SiteMenu::getByLevel(1);
        $sitemenuData = $siteMenu->load();
        //$rootCategory = $document->getProperty(AbstractProductLinkGenerator::ROOT_CATEGORY_PROPERTY_NAME);
        $menu = [];
        foreach ($sitemenuData as $key => $value) {
            $menu[$key]['label'] = $value->getLabel();
            $menu[$key]['id'] = $value->getId();
            if($value->getLevel() == 1 || $value->getLevel() == 2){
                $menu[$key]['url'] = "/shop/".Text::toUrl($value->getLabel() ?? 'elements')."~c".$value->getId();

            }
            $menu[$key]['order'] = $value->getOrder();
            if(!empty($value->getCategory())){
                $menu[$key]['image'] = !empty($value->getCategory()->getImage())?$value->getCategory()->getImage():'';
            }
            $menu[$key]['children'] = [];
            
            if(!empty($value->getChildren())){
                $menu[$key]['children']  =  $this->getRecurciveCategory($value);
            }
        }
        $keys = array_column($menu, 'order');
        array_multisort($keys, SORT_ASC, $menu);

        return $menu;
    }

    public function getRecurciveCategory($category){

        $menu = [];
        $allCategoryId = [];
        foreach ($category->getChildren() as $key => $value) {
            $showAll = [];

            $menu[$key]['label'] = $value->getLabel();
            $menu[$key]['id'] = $value->getId();
            $menu[$key]['order'] = $value->getOrder();
            if($value->getLevel() == 2){
                $menu[$key]['url'] = "/shop/".Text::toUrl($value->getLabel() ?? 'elements')."~c".$value->getId();
                //$menu[$key]['url'] = $this->categoryLinkGenerator->generateSitemenu($value,[],true);
            }else{
                $menu[$key]['url'] = ($value->getCategory() != NULL) ? $this->categoryLinkGenerator->generate($value,[],true):"#";
            }
            
            if(!empty($value->getCategory())){
                $menu[$key]['image'] = !empty($value->getCategory()->getImage())?$value->getCategory()->getImage():'';
            }
            $menu[$key]['children'] = [];

            if(!empty($value->getChildren())){

                $menu[$key]['children'] = $this->getRecurciveCategory($value);
                if($value->getLevel() == 2 && $value->getShowAll()){
                    $allCategoryId=array();
                    foreach($value->getChildren() as $vv){
                        $allCategoryId[] = $vv->getCategory()->getId();
                    }
                    $allCategoryIdStr = implode(',',array_unique($allCategoryId));
                    $showAll[-1]['label'] = "Show All ".$value->getLabel();
                    $showAll[-1]['id'] = $value->getId();
                    $showAll[-1]['order'] = 0;
                    $showAll[-1]['url'] = "/shop/".Text::toUrl($value->getLabel() ?? 'elements')."~c".$value->getId();
                    //$showAll[-1]['url'] = ($value->getCategory() != NULL) ? $this->categoryLinkGenerator->generate($value->getCategory(),[$allCategoryIdStr],true):"#";
                }
                $menu[$key]['children'] = array_merge($showAll,$menu[$key]['children']);
            }
        }
        $keys = array_column($menu, 'order');
        array_multisort($keys, SORT_ASC, $menu);
        return $menu;
    }

    public function getProductLinks($product){

        $url = $this->productLinkGenerator->generateWithMockup($product, []);
        return $url;
    }

    /**
     * @param Container $navigation
     *
     * @return Container
     *
     * @throws \Exception
     */
    public function enrichBreadcrumbs()
    {
        $breadctumbsArray = [];
        if(!empty($_SESSION['_sf2_attributes']['siteMenuId']) && isset($_SESSION['_sf2_attributes']['siteMenuId'])){
           
            $data = SiteMenu::getById($_SESSION['_sf2_attributes']['siteMenuId']);
            
            if($data->getLevel() == 3){
                $urlLevel1 = "/shop/".Text::toUrl($data->getParent()->getParent()->getLabel() ?? 'elements')."~c".$data->getParent()->getParent()->getId();
                $urlLevel2 = "/shop/".Text::toUrl($data->getParent()->getLabel() ?? 'elements')."~c".$data->getParent()->getId();
                $urlLevel3 = "/shop/".Text::toUrl($data->getLabel() ?? 'elements')."~c".$data->getCategory()->getId();
                $breadctumbsArray = array(
                    "level1"=>array("id" => $data->getParent()->getParent()->getId(),"name" => $data->getParent()->getParent()->getLabel(),"url" => $urlLevel1),
                    "level2"=>array("id" => $data->getParent()->getId(),"name" => $data->getParent()->getLabel(),"url" => $urlLevel2),
                    "level3"=>array("id" => $data->getId(),"name" => $data->getLabel(),"url"=>$urlLevel3)
                );
            }elseif($data->getLevel() == 2){
                $urlLevel1 = "/shop/".Text::toUrl($data->getParent()->getLabel() ?? 'elements')."~c".$data->getParent()->getId();
                $breadctumbsArray = array(
                    "level1"=>array("id" => $data->getParent()->getId(),"name" => $data->getParent()->getLabel(),"url" => $urlLevel1),
                    "level2"=>array("id" => $data->getId(),"name" => $data->getLabel()),
                );
            }else{
                $breadctumbsArray = array(
                    "level1"=>array("id" => $data->getId(),"name" => $data->getLabel()),
                );
            }
        }
      // p_r($breadctumbsArray);die;
        return $breadctumbsArray;
    }

    public function translateValues($values)
    {
        foreach ($values as &$modifyingValue) {
            $modifyingValue['translated'] = $this->translator->trans(mb_strtolower('attribute.' . $modifyingValue['value']));
        }

        return $values;
    }

    /**
     * @param array $values
     * @param string|null $fieldname
     *
     * @return array
     */
    public function sort(array $values, string $fieldname = null): array
    {
        @usort($values, function ($left, $right) use ($fieldname) {
            $leftString = $left;
            $rightString = $right;

            if ($fieldname) {
                $methodName = 'get' . ucfirst($fieldname);
                if (is_array($leftString)) {
                    $leftString = $leftString[$fieldname];
                } elseif (method_exists($leftString, $methodName)) {
                    $leftString = $leftString->$methodName();
                }
                if (is_array($rightString)) {
                    $rightString = $rightString[$fieldname];
                } elseif (method_exists($rightString, $methodName)) {
                    $rightString = $rightString->$methodName();
                }
            }

            return strcmp($leftString, $rightString);
        });

        return $values;
    }

    /**
     * @param array $values
     * @param string $fieldname
     * @param array $objects
     *
     * @return array
     */
    public function sortObjects(array $values, string $fieldname, array $objects): array
    {
        @usort($values, function ($left, $right) use ($fieldname, $objects) {
            $leftString = '';
            $rightString = '';

            $methodName = 'get' . ucfirst($fieldname);

            $object = $objects[$left['value']];
            if ($object && method_exists($object, $methodName)) {
                $leftString = $object->$methodName();
            }

            $object = $objects[$right['value']];
            if ($object && method_exists($object, $methodName)) {
                $rightString = $object->$methodName();
            }

            return strcmp($leftString, $rightString);
        });

        return $values;
    }

    /**
     * @param string $className
     * @param number $value
     * @return string $value
     */
    public function fromIdToValueStr($className, $value){

        if($className != 'isFCL'){
            if($className == 'lifeStyleTheme'){
                $className = 'LifeStyleCollection';
            }

            if($className == 'productionLeadTime'){
                $className = 'LeadTime';
            }

            $objectStr =  'Pimcore\\Model\\DataObject\\'.ucfirst($className).'\\Listing';
            $object = new $objectStr();
            $object->setCondition(' o_id = ? ', [$value]);
            $object->setLimit(1);
            $loadObjectData = $object->load();
            return $loadObjectData[0]->getName();
        }else{
            return $value;
        }

    }

    /**
     * @param  Request Parameter $requestVariables
     * @param  string $currentUrl
     * @return modifiedweburl $x
     */
    public function pimcorePaginationUrl($requestVariables, $currentUrl = ''){
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        if(in_array('sort',$_REQUEST)){
            $actual_linkX =    $actual_link;
            $actual_linkY =    $actual_link;
        }else{
            $actual_linkX = substr($actual_link, 0, strpos($actual_link, "&".$requestVariables."="));
            $actual_linkY = substr($actual_link, 0, strpos($actual_link, "?".$requestVariables."="));
        }

        if($actual_linkX){
             $actual_link = $actual_linkX;
        }
        if($actual_linkY){
            $actual_link = $actual_linkY;
        }

        $urlData = explode('?',$actual_link);

        if(count($urlData) >= 2){
            $lchar = substr($actual_link, -1);
            if($lchar == '?') {
                $x = $actual_link.$requestVariables.'=';
            }else{
                $x = $actual_link.'&'.$requestVariables.'=';
            }
        }else{
            $lchar = substr($actual_link, -1);
            if($lchar == '?'){
                $x = $actual_link.$requestVariables.'=';
            }else{
                $x = $actual_link.'?'.$requestVariables.'=';
            }
        }
        return $x;
    }

    public function getProductWishlistStatus($product){
        if(isset($_SESSION['_pimcore_admin']['customer']) && !empty($_SESSION['_pimcore_admin']['customer'])){
            $customer = $_SESSION['_pimcore_admin']['customer'];
            $wishlistListing = new DataObject\Wishlist\Listing();
            $wishlistListing->setCondition("customer__id = ".$customer->getId());
           // $wishlistListing->setCondition("products like '%,products|".$product->getId().",%'");
            $wishlistData = $wishlistListing->load();
           // p_r($wishlistListing);die;
            if(count($wishlistData) != 0){
                return $wishlistData[0]->getLable();
            }
        }
        return false;
    }

    public function checkUserSubscriptionType(){
        if(isset($_SESSION['_pimcore_admin']['customer']) && !empty($_SESSION['_pimcore_admin']['customer'])){
            $customer = $_SESSION['_pimcore_admin']['customer'];
            return $customer->getCompany()->getSubscription();
        }
    }

    
}
