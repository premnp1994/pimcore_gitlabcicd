<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Services;

use AppBundle\Model\CustomerManagementFramework\Activity\LoginActivity;
use Pimcore\Bundle\EcommerceFrameworkBundle\IndexService\ProductList\ElasticSearch\AbstractElasticSearch;
use Pimcore\Bundle\EcommerceFrameworkBundle\IndexService\ProductList\ProductListInterface;


class ProductServices
{

    public function getProductListing($customer, $paramCategory, $indexService, $sort){

            $customerIncludeProductCategory = $customer->getCompany()->getProductCategory();
            $customerIncludeCategoryIdArray = array();
            $customerIncludeProductIdArray = array();
            if(!empty($customerIncludeProductCategory)){
                foreach($customerIncludeProductCategory as $custIncProCat){
                    if($custIncProCat->geto_className() == "Product"){
                        $customerIncludeProductIdArray[] = $custIncProCat->getId();
                    }else{
                        $customerIncludeCategoryIdArray[] = $custIncProCat->getId();

                    }
                }
                if(!empty($customerIncludeCategoryIdArray)){
                    $includeCategoryServices = new ExcludeCategoryServices();
                    $includeCategoryResponse = $includeCategoryServices->getCategory($customerIncludeCategoryIdArray);
                    $customerIncludeCategoryIdArray = $includeCategoryResponse;
                }
            }

            $customerExcludedProductCategory = $customer->getCompany()->getExcludedProductCategory();
            $customerExcludedCategoryIdArray = array();
            $customerExcludedProductIdArray = array();
            if(!empty($customerExcludedProductCategory)){
                foreach($customerExcludedProductCategory as $custExcProCat){
                    if($custExcProCat->geto_className() == "Product"){
                        $customerExcludedProductIdArray[] = $custExcProCat->getId();
                    }else{
                        $customerExcludedCategoryIdArray[] = $custExcProCat->getId();
                    }
                }
                if(!empty($customerExcludedCategoryIdArray)){
                    $excludeCategoryServices = new ExcludeCategoryServices();
                    $excludeCategoryResponse = $excludeCategoryServices->getCategory($customerExcludedCategoryIdArray);
                    $customerExcludedCategoryIdArray = $excludeCategoryResponse;
                }
            }


        if(empty($customerIncludeProductCategory)){
            $paramCategory = "";
        }
        if(!empty($customerIncludeCategoryIdArray)){
            $paramCategory = implode(",",array_intersect($customerIncludeCategoryIdArray,explode(',',$paramCategory)));
        }
        if(empty($customerIncludeCategoryIdArray) && !empty($customerExcludedCategoryIdArray)){
            $paramCategory = implode(",",array_diff(explode(',',$paramCategory),$customerExcludedCategoryIdArray));
        }

        if($paramCategory != ""){

            $productListing = $indexService->getProductListForCurrentTenant();
            $productListing->setVariantMode(ProductListInterface::PRODUCT_TYPE_OBJECT );
            $productListing->addCondition("productType = 'virtual-product'", 'productType');
            $productListing->addRelationCondition('category', "dest IN (".$paramCategory.")");

            if(!empty($customerIncludeProductIdArray)){
                $productListing->addCondition("o_virtualProductId IN (".implode(",",$customerIncludeProductIdArray).")",'o_virtualProductId');
            }
            if(!empty($customerExcludedProductIdArray)){
                $productListing->addCondition("o_virtualProductId NOT IN (".implode(",",$customerExcludedProductIdArray).")",'o_virtualProductId');
            }
            if(!empty($sort)){
                $productListing->setOrderKey($sort);
            }
            $productListing->setOrder("DESC");
        }
        
        return $productListing;
   }

   public function getProductDetailsListing($customer,$factoryId,$indexService){

        $customerIncludeProduct = $customer->getCompany()->getProductCategory();
        $customerIncludeProductIdArray = array();
        if(!empty($customerIncludeProduct)){
            foreach($customerIncludeProduct as $custIncProCat){
                if($custIncProCat->geto_className() == "Product"){
                    $customerIncludeProductIdArray[] = $custIncProCat->getId();
                }
            }
        }

        $customerExcludedProduct = $customer->getCompany()->getExcludedProductCategory();
        $customerExcludedProductIdArray = array();
        if(!empty($customerExcludedProduct)){
            foreach($customerExcludedProduct as $custExcProCat){
                if($custExcProCat->geto_className() == "Product"){
                    $customerExcludedProductIdArray[] = $custExcProCat->getId();
                }
            }

        }

        $productListing = $indexService->getProductListForCurrentTenant();
        $productListing->setVariantMode(ProductListInterface::PRODUCT_TYPE_OBJECT );
        $productListing->addCondition("productType = 'virtual-product'", 'productType');
        $productListing->addCondition("factoryItemNo = '".$factoryId."'");
        if(!empty($customerIncludeProductIdArray)){
            $productListing->addCondition("o_virtualProductId IN (".implode(",",$customerIncludeProductIdArray).")",'o_virtualProductId');
        }
        if(!empty($customerExcludedProductIdArray)){
            $productListing->addCondition("o_virtualProductId NOT IN (".implode(",",$customerExcludedProductIdArray).")",'o_virtualProductId');
        }

        return $productListing;
   }

   public function getMostViewedProduct($user, $indexService, $pageType, $virtualProducts = null, $activityManager = null){
       $type = 'Visit Product';
       $list = \Pimcore::getContainer()->get('cmf.activity_store')->getActivityList();
       if($user){
        $list->setCondition('customerId = ' . $user->getId());
       }
       $list->setOrderKey('activityDate');
       $list->setOrder('desc');
       if ($type = 'Visit Product' ) {
           $select = $list->getQuery(false);
           $select->where('type = ?', $type);
           $condition = implode(' ', $list->getQuery()->getPart('where'));
           $list->setCondition($condition);
       }
       $activityData = $list->load();

       if($pageType == 'detail') {
           /*if(!empty($activityData)){
               if ($activityData[0]->getAttributes()['product'] != $virtualProducts->getProductCode()) {
                   $user->product = $virtualProducts->getProductCode();
                   $activityManager->trackActivity(new LoginActivity($user));
               }
           }else{
               $user->product = $virtualProducts->getProductCode();
               $activityManager->trackActivity(new LoginActivity($user));
           }*/
           $user->product = $virtualProducts->getProductCode();
           $activityManager->trackActivity(new LoginActivity($user));
       }

       $prodIDArray = [];
       foreach ($activityData as $actVal){
           $Prodids = $actVal->getAttributes()['product'];
           //if(!in_array($Prodids, $prodIDArray, true)){
               array_push($prodIDArray, $Prodids);
           //}
       }
       $uniqArr = array_unique($prodIDArray);

       $finalViewArray = array_slice($uniqArr, 0, 5);
       if($pageType == 'detail') {
           if (($key = array_search($virtualProducts->getProductCode(), $finalViewArray)) !== false) {
               unset($finalViewArray[$key]);
           }
       }
       //$values = array_count_values($prodIDArray);
       //arsort($values);
       //$popular = array_slice(array_keys($values), 0, 5, true);
       //$recentlyViewedProductIds = '';
       $recentlyViewedProductIds = [];
       foreach($finalViewArray as $rkey=>$rval){
           //$recentlyViewedProductIds .= '"'.$rval.'",';
           $lastviewedProductListing = $indexService->getProductListForCurrentTenant();
           $lastviewedProductListing->setVariantMode(ProductListInterface::PRODUCT_TYPE_OBJECT );
           $lastviewedProductListing->addCondition("productType = 'virtual-product'", 'productType');
           $lastviewedProductListing->addCondition("productCode = '".$rval."'", 'productCode');
           $lastviewedProductListing->getProducts()[0];
           $recentlyViewedProductIds[$rkey] = $lastviewedProductListing->getProducts()[0];
       }


       /*$recentlyViewedProductIds = substr($recentlyViewedProductIds, 0, -1);
       $lastviewedProductListing = array();*/
       /*if(trim($recentlyViewedProductIds) != ''){
           $lastviewedProductListing = $indexService->getProductListForCurrentTenant();
           $lastviewedProductListing->setVariantMode(ProductListInterface::PRODUCT_TYPE_OBJECT );
           $lastviewedProductListing->addCondition("productType = 'virtual-product'", 'productType');
           if($pageType == 'detail') {
               $lastviewedProductListing->addCondition("productCode != '" . $virtualProducts->getProductCode() . "'", 'productCode');
           }
           $lastviewedProductListing->addCondition("productCode IN (".$recentlyViewedProductIds.")");
           //$lastviewedProductListing->setOrderKey("trending");
           //$lastviewedProductListing->setOrder('desc');
       }*/
       /*p_r($recentlyViewedProductIds);
       die;*/
       return $recentlyViewedProductIds;
   }
}
