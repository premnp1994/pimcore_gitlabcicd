<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Services;

use Pimcore\Model\DataObject\SiteMenu;

class CategoryServices
{
   
    public function getSiteMenuCategory($siteMenuId){
        
        $siteMenuObject = SiteMenu::getById($siteMenuId);
        
        if($siteMenuObject instanceof SiteMenu){
            
            $category = $siteMenuObject->getLabel();
            $currentCatId = $siteMenuObject->getCategory()->getId();
            $categoryArray = [];
            $this->getSiteMenuChildrens($siteMenuObject->getChildren(),$categoryArray);

            return array("categoryName" => $category,"categoryIds" => implode(",",array_unique($categoryArray)),"currentCategoryId"=>$currentCatId);
        }
    }
    
    public function getSiteMenuChildrens($children,&$categoryArray){
       // p_r($children);die;
       
        foreach($children as $key => $value){
            
            if($value->getLevel() == 2){                
                 $this->getSiteMenuChildrens($value->getChildren(),$categoryArray);

            }elseif($value->getLevel() == 3 && !empty($value->getCategory())){
                
               array_push($categoryArray,$value->getCategory()->getId()); 
                
            }
        }
    }

}
