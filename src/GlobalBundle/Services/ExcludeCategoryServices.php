<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Services;

use Pimcore\Model\DataObject\Category;

class ExcludeCategoryServices
{
   
    public function getCategory($categoryId){
       
        $category = [];
        $categoryArray = [];
        foreach($categoryId as $value){
            $categoryObject = Category::getById($value);
            if($categoryObject instanceof Category){
               
               array_push($categoryArray,$value);
               
                if(!empty($categoryObject->getChildren())){
                    $this->getCategoryChildrens($categoryObject->getChildren(),$categoryArray);
                   // $category[] = $categoryArray;
                }
            }
        }
        
        return array_unique($categoryArray);
    }
    
    public function getCategoryChildrens($children,&$categoryArray){
       
        foreach($children as  $value){
            array_push($categoryArray,$value->getId());
            if(!empty($value->getChildren())){
                $this->getCategoryChildrens($value->getChildren(),$categoryArray);
            } 
        }
    }

}
