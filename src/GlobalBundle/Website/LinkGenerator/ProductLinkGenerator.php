<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace GlobalBundle\Website\LinkGenerator;

use AppBundle\Model\Product\Product;
use AppBundle\Website\Tool\ForceInheritance;
use AppBundle\Website\Tool\Text;
use Pimcore\Bundle\EcommerceFrameworkBundle\Model\ProductInterface;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Concrete;

class ProductLinkGenerator extends AbstractProductLinkGenerator implements LinkGeneratorInterface
{
    /**
     * @param Concrete $object
     * @param array $params
     *
     * @return string
     */
    public function generate(Concrete $object, array $params = []): string
    {
        if (!($object instanceof Product)) {
            throw new \InvalidArgumentException('Given object is no Product');
        }

        return $this->doGenerate($object, $params);
    }

    /**
     * @param ProductInterface $object
     * @param array $params
     * @return string
     */
    public function generateWithMockup(ProductInterface $object, array $params = []): string {
        return $this->doGenerate($object, $params);
    }

    /**
     * @param ProductInterface $object
     * @param array $params
     * @return string
     */
    public function generateWithMockupDetail(ProductInterface $object, array $params = []): string {
        return $this->doGenerateDetail($object, $params);
    }

    /**
     * @param $object
     * @param $params
     * @return string
     */
    protected function doGenerate($object, $params): string
    {
            $id = $object->getId();
            if(!empty($object->getChildren())){
                $level1 = $object->getChildren()[0];
                $id = $level1->getID();
                if(!empty($level1->getChildren())){
                    $level2 = $level1->getChildren()[0];
                    $id = $level2->getID();
                    if(!empty($level2->getChildren())){
                        $level3 = $level2->getChildren()[0];
                        $id = $level3->getID();
                    }
                }
            }
           
           return $this->pimcoreUrl->__invoke(
                [
                    'productname' => Text::toUrl($object->getOSName() ? $object->getOSName() : 'product'),
                    'product' => $id,
                    'path' => $this->getNavigationPath($object->getCategory(), $params['rootCategory'] ?? null),
                    'page' => null
                ],
                'shop-detail',
                true
            );


    }

    /**
     * @param $object
     * @param $params
     * @return string
     */
    protected function doGenerateDetail($object, $params): string
    {
        $id = $object->getId();
        return $this->pimcoreUrl->__invoke(
            [
                'productname' => Text::toUrl($object->getOSName() ? $object->getOSName() : 'product'),
                'product' => $id,
                'path' => $this->getNavigationPath($object->getCategory(), $params['rootCategory'] ?? null),
                'page' => null
            ],
            'shop-detail',
            true
        );
    }
}
