<?php

namespace GlobalBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class GlobalBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/global/js/pimcore/startup.js'
        ];
    }
}