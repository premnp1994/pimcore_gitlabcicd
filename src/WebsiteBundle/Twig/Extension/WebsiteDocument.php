<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace WebsiteBundle\Twig\Extension;

use Symfony\Component\Intl\Intl;
use Twig\Extension\AbstractExtension;
use GlobalBundle\AppHelpers\GeneralHelper;
use Twig\TwigFunction;
use Pimcore\Model\Document;

class WebsiteDocument extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('pimcore_document_by_path', [$this, 'getDocumentByPath']),
            new TwigFunction('pimcore_get_object_tags', [$this, 'getElementTags']),
            new TwigFunction('pimcore_generate_image_url', [$this, 'getImageUrl']),
            new TwigFunction('pimcore_get_current_route_name', [$this, 'getCurrentRouteName']),
            new TwigFunction('check_IE', [$this, 'isIe']),
        ];
    }
    /*
     * @param string $path
     *
     * @return boolean
     */

    public function getDocumentByPath($path){
         return Document::getByPath($path);
    }

    /*
     * @param string $type
     * @param string elementId
     *
     * @return string
     */
    public function getElementTags($type,$elementId){
        $tagArray=array();
        $tags = \Pimcore\Model\Element\Tag::getTagsForElement($type,$elementId);
        $tagArray=array();
        foreach($tags as $tag){
            $tagArray[]= ucfirst($tag->getName());
        }
        return implode(",",$tagArray);
    }

    /*
     * @param Object Image
     * @param string elementId
     *
     * @return string url
     */
    public function getImageUrl($imageObject){
        // $imgUrl="";
        // if($imageObject){
        //     $imgPath=$imageObject->getPath();
        //     $imgFileName=$imageObject->getFileName();
        //     $imgUrl=\Pimcore\Tool::getHostUrl().$imgPath.$imgFileName;
        // }
        return GeneralHelper::getAssetFullUrl($imageObject);
    }

    public function getCurrentRouteName(){

    }

    /**
     * Is IE
     * @return boolean
     */
    public function isIe()
    {
        $matches = array();
        preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

        if(count($matches)<2){
            preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
        }

        if (count($matches)>1){
            //Then we're using IE
            $version = $matches[1];

            switch(true){
                case ($version<=10):
                    return true;
                    break;
                default:
                    return false;
            }
        }
        return false;
    }
}
