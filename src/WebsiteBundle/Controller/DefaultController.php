<?php
namespace WebsiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

use WebsiteBundle\Controller\BaseController As BaseController;
use GlobalBundle\AppHelpers\GeneralHelper As DefaultHelper;
//Model Classess
use Pimcore\Model\DataObject\BookAMeeting;
use Pimcore\Model\DataObject\BecomeASupplier;
use Pimcore\Model\DataObject\CareerInquiry;
use Pimcore\Model\DataObject\NewsResourceCasestudy;
use Pimcore\Model\DataObject\LeaderShipTeam;
use GlobalBundle\AppHelpers\GeneralHelper;
use Pimcore\Model\Asset\Service;

class DefaultController extends BaseController
{

    /**
     * Description:- Load Default View
    */
    public function defaultAction(Request $request){
    }


    /**
     * Description:- Load view of landing page
     *
     * @Route("/", name="website_homepage")
     * @param Request $request
     *
     * @return mixed
    */
    public function indexAction(Request $request)
    {
        $caseStudiesListing = new NewsResourceCasestudy\Listing();
        $caseStudiesListing->setCondition("contentType LIKE ?", "%Case Study%");
        $caseStudiesListing->setOrderKey("o_modificationDate");
        $caseStudiesListing->setOrder("desc");
        $caseStudiesListing->setLimit(5);
        $caseStudiesListing->load();
        return [
                'caseStudies'=>$caseStudiesListing
            ];
    }

    /**
     * Description:- Load view for services page
     *
     * @Route("/services",name="website_services")
     * @param Request $request
     *
     *
    */
    public function servicesAction(Request $request)
    {

    }

    /**
     *  Description:- Load View For OurWork Page.
     *  Handle ajax request for load data in pop up.
     *
     * @Route("/our-work/{id}",name="website_ourwork");
     * @param Request $request
     * @param null $id
     * @return mixed
    */
    public function ourWorkAction(Request $request,$id=null)
    {
        $caseStudiesListing = new NewsResourceCasestudy\Listing();
        $caseStudiesListing->setCondition("contentType LIKE ?", "%Case Study%");
        if($request->isXmlHttpRequest() && $request->isMethod('GET') && null !== $id) {
            $caseStudiesListing->setCondition('o_id =?',$id);
            $caseStudiesListing->setLimit(1);
            $caseStudiesListing->load();
            $caseStudy=$caseStudiesListing->getObjects()[0];
            $tags = DefaultHelper::getElementTags('object',$caseStudy->getId());
            $caseStudyData=array();
            $caseStudyData['title']=$caseStudy->getTitle();
            $caseStudyData['tags']=$tags;
            $caseStudyData['summary']=$caseStudy->getSummary();
            $caseStudyData['desc']=$caseStudy->getDescription();
            $caseStudyData['image']= $caseStudy->getImage();
            return $this->render('@Website/popups/ourWork.html.twig', ['caseStudy'=>$caseStudyData]);
        }
        $caseStudiesListing->load();
        $paginator = new \Zend\Paginator\Paginator($caseStudiesListing);
        $paginator->setCurrentPageNumber($request->get('page') );
        $paginator->setItemCountPerPage(6);
        return [
                'caseStudies'=>$caseStudiesListing,
                'paginator'=>$paginator->getPages('Sliding')
            ];

    }

    /**
     *  Description:- Load View For OurWork Page.
     *  Handle ajax request for load data in pop up.
     *
     * @Route("/blog/{id}",name="website_blog");
     * @param Request $request
     * @param null $id
     * @return mixed
    */
    public function blogAction(Request $request,$id=null)
    {
        $caseStudiesListing = new NewsResourceCasestudy\Listing();
        $caseStudiesListing->setCondition("contentType LIKE ?", "%News%");
        if($request->isXmlHttpRequest() && $request->isMethod('GET') && null !== $id) {
            $caseStudiesListing->setCondition('o_id =?',$id);
            $caseStudiesListing->setLimit(1);
            $caseStudiesListing->load();
            $caseStudy=$caseStudiesListing->getObjects()[0];
            $tags = DefaultHelper::getElementTags('object',$caseStudy->getId());
            $caseStudyData=array();
            $caseStudyData['title']=$caseStudy->getTitle();
            $caseStudyData['tags']=$tags;
            $caseStudyData['summary']=$caseStudy->getSummary();
            $caseStudyData['desc']=$caseStudy->getDescription();
            $caseStudyData['image']= $caseStudy->getImage();
            return $this->render('@Website/popups/ourWork.html.twig', ['caseStudy'=>$caseStudyData]);
        }
        $caseStudiesListing->load();
        $paginator = new \Zend\Paginator\Paginator($caseStudiesListing);
        $paginator->setCurrentPageNumber($request->get('page') );
        $paginator->setItemCountPerPage(6);
        return [
                'caseStudies'=>$caseStudiesListing,
                'paginator'=>$paginator->getPages('Sliding')
            ];

    }


    /**
     * Description:- Load view for About Us page
     *
     * @Route("/about-us",name="website_aboutus");
     * @param Request $request
     *
     *
    */
    public function aboutUsAction(Request $request)
    {

        $leaderShipTeam = new LeaderShipTeam\Listing();
        $leaderShipTeam->load();
        $leadership = array();
        $team = array();
        foreach($leaderShipTeam as $value){
            if($value->getSectionType() == "Team"){
                $team[] = array('name'=>$value->getName(),
                                        'position'=>$value->getPosition(),
                                        'description'=>$value->getDescription(),
                                        'orderNumber'=>$value->getOrderNumber(),
                                        'type'=>$value->getSectionType(),
                                        'image'=>$value->getImage(),
                                        'bgColor'=>$value->getBgColor());
            }else{
                $leadership[] = array('name'=>$value->getName(),
                                        'position'=>$value->getPosition(),
                                        'description'=>$value->getDescription(),
                                        'orderNumber'=>$value->getOrderNumber(),
                                        'type'=>$value->getSectionType(),
                                        'image'=>$value->getImage(),
                                        'bgColor'=>$value->getBgColor());
            }
        }
        return [
            'leadershipArray'=>$leadership,
            'teamArray'=>$team
        ];

    }


    /**
     * Description:- Load view for News And Resouces Page
     *
     * @Route("/news-resources",name="website_news_and_resources");
     * @param Request $request
     *
     *
    */
    public function newsResourcesAction(Request $request){

    }



    /**
     * Description:- Handle Ajax Request For Book A Meeting Form
     *
     * @Route("/book-meeting",name="website_book_meeting");
     * @param Request $request
     *
     * @return Response Object
    */
    public function bookMeetingAction(Request $request){
        $error=array();
        $responseData=array();
        //if ($request->isMethod('POST') &&  $request->isXmlHttpRequest()) {
        if($request->getMethod() === "POST") {
            $meetingAgenda=$request->get('meetingAgenda');
            $firstName=$request->get('firstName');
            $email=$request->get('email');

            if(empty($meetingAgenda)){
                $error['meetingAgenda']="*Required";
            }
            if(empty($firstName)){
                $error['fname']="*Required";
            }
            if(empty($email)){
                $error['email']="*Required";
            }
            // else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //      $error['email']="*Not Valid";
            // }

            if(!empty($error)){
               $responseData=array("status"=>"failed",'code'=>"400","errorData"=>$error);
            }else{
                $bookMeeting= new BookAMeeting();
                $key="BM-".time();
                $bookMeeting->setKey($key, 'object');

                $bookMeeting->setParent(DefaultHelper::validateFolderPath(BOOK_MEETING_PATH));
                $bookMeeting->setName($firstName);
                $bookMeeting->setEmail($email);
                $bookMeeting->setMessage($meetingAgenda);
                $bookMeeting->setOmitMandatoryCheck(true);
                $bookMeeting->setPublished(true);
                $bookMeeting->save();

                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $mailReciver = $websiteConfig->get('MailNotification');
                $bookAMeeting = array('name'=>$firstName, 'email'=> $email, 'message'=>$meetingAgenda);
                GeneralHelper::sendMail($mailReciver,'/3SWebsite/Emails/BookAMeeting/BookAMeeting',$bookAMeeting);

                $added = $bookMeeting->getId();

                $responseData=array("status"=>"success",'code'=>"200","meetingId"=>$added);
            }
            return  new Response(json_encode($responseData));
        }else{
            $responseData=array("status"=>"failed",'code'=>"400","errorData"=>"Not Valid Request.");
            return  new Response(json_encode($responseData));
        }
    }

    /**
     * Description:- Handle Ajax Request For Become A Supplier Form
     *
     * @Route("/become-supplier", name="website_become_supplier");
     * @param Request $request
     *
     * @return Response Object
    */

    public function becomeSuppLierAction(Request $request){
        $error=array();
        $responseData=array();
        //if ($request->isMethod('POST') &&  $request->isXmlHttpRequest()) {
        if($request->getMethod() === "POST") {
            $supplierCompanyName=$request->get('supplierCompanyName');
            //$supplierBuildingNumber=$request->get('supplierBuildingNumber');
            //$supplierVillageName=$request->get('supplierVillageName');
            //$supplierPostName=$request->get('supplierPostName');
            //$supplierTalukaName=$request->get('supplierTalukaName');
            //$supplierLocalityNeighbourhood=$request->get('supplierLocalityNeighbourhood');
            //$supplierCpCode=$request->get('supplierCpCode');
            //$supplierDistrictName=$request->get('supplierDistrictName');
            //$supplierState=$request->get('supplierState');
            //$supplierTitle=$request->get('supplierTitle');
            $supplierFirstName=$request->get('supplierFirstName');
            //$supplierLastName=$request->get('supplierLastName');
            $suppllierJobTitle=$request->get('suppllierJobTitle');
            $supplierEmailAddress=$request->get('supplierEmailAddress');
            $supplierTelephoneMobile=$request->get('supplierTelephoneMobile');
            $supplierCompanyWebsite=$request->get('supplierCompanyWebsite');
            $supplierAddress=$request->get('supplierAddress');
            $supplierUpload=$request->files->get('supplierResume');

            if(empty($supplierCompanyName)){
                $error['supplierCompanyName']="*Required";
            }
            /*if(empty($supplierBuildingNumber)){
                $error['supplierBuildingNumber']="*Required";
            }*/
            /*if(empty($supplierCpCode)){
                $error['supplierCpCode']="*Required";
            }*/
            /*if(empty($supplierDistrictName)){
                $error['supplierDistrictName']="*Required";
            }*/
            /*if(empty($supplierState)){
                $error['supplierState']="*Required";
            }*/
            if(empty($supplierFirstName)){
                $error['supplierFirstName']="*Required";
            }
            /*if(empty($supplierLastName)){
                $error['supplierLastName']="*Required";
            }*/
            if(empty($suppllierJobTitle)){
                $error['suppllierJobTitle']="*Required";
            }
            if(empty($supplierEmailAddress)){
                $error['supplierEmailAddress']="*Required";
            }else if(!filter_var($supplierEmailAddress, FILTER_VALIDATE_EMAIL)) {
                 $error['supplierEmailAddress']="*Opps, you need fill a valid email.";
            }
            if(empty($supplierTelephoneMobile)){
                $error['supplierTelephoneMobile']="*Required";
            }

            if(empty($supplierUpload)){
                $error['supplierUpload']="*Required";
            }

            if(empty($supplierAddress)){
                $error['supplierAddress']="*Required";
            }

            /*if(isset($supplierCompanyWebsite)){
                $headers = @get_headers($supplierCompanyWebsite);
                if($headers || strpos( $headers[0], '404')) {
                    $error['$supplierCompanyWebsite']="*invalid url.";
                }
            }*/


            if(!empty($error)){
               $responseData=array("status"=>"failed",'code'=>"400","errorData"=>$error);
            }else{
                $supplier= new BecomeASupplier();
                $key="BS-".time();
                $supplier->setKey($key, 'object');
                $supplier->setParent(DefaultHelper::validateFolderPath(SUPPLIER_REQUEST_PATH));

                $supplier->setCompanyName($supplierCompanyName);
                /*$supplier->setBuildingName($supplierBuildingNumber);
                $supplier->setVIAName($supplierVillageName);
                $supplier->setPostName($supplierPostName);
                $supplier->setTalukName($supplierTalukaName);
                $supplier->setLocality($supplierLocalityNeighbourhood);
                $supplier->setCity($supplierCpCode);
                $supplier->setDistrictName($supplierDistrictName);
                $supplier->setState($supplierState);*/
                //$supplier->setTitle($suppllierJobTitle);
                $supplier->setFirstName($supplierFirstName);
                //$supplier->setLastName($supplierLastName);
                $supplier->setJobTitle($suppllierJobTitle);
                $supplier->setEmail($supplierEmailAddress);
                $supplier->setContactNumber($supplierTelephoneMobile);
                $supplier->setWebsite($supplierCompanyWebsite);
                $supplier->setAddress($supplierAddress);

                $supplier->setOmitMandatoryCheck(true);
                $supplier->setPublished(true);

                $path = '/3S Website Data/Supplier Request/';
                $folder = Service::createFolderByPath($path);
                $newAsset = new \Pimcore\Model\Asset();
                $newAsset->setFilename(time()." - ".$request->files->get('supplierResume')->getClientOriginalName());
                $newAsset->setData(file_get_contents($request->files->get('supplierResume')));
                $newAsset->setParentId($folder->getId());
                $newAsset->save();

                $supplier->setResume($newAsset);

                $supplier->save();

                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $mailReciver = $websiteConfig->get('MailNotification');
                $emailSubject = '';
                $becomeASupplier = array('companyName' => $supplierCompanyName, 'firstName' => $supplierFirstName, 'jobTitle' => $suppllierJobTitle, 'email' => $supplierEmailAddress, 'contactNumber' => $supplierTelephoneMobile, 'website' => $supplierCompanyWebsite, 'address' => $supplierAddress);
                GeneralHelper::sendMail($mailReciver,'/3SWebsite/Emails/BecomeASupplier/BecomeASupplier',$becomeASupplier,$emailSubject,$newAsset);

                $added = $supplier->getId();
                $responseData=array("status"=>"success",'code'=>"200","meetingId"=>$added);
            }
            return new Response(json_encode($responseData));
        }else{
            $responseData=array("status"=>"failed",'code'=>"400","errorData"=>"Not Valid Request.");
            return  new Response(json_encode($responseData));
        }

    }


    /**
     * Description:- Handle Ajax Request For Career With Us Form
     *
     * @Route("/career-with-us",name="website_become_career_with_us");
     * @param Request $request
     *
     * @return Response Object
    */
    public function careerWithUsAction(Request $request){
        $error=array();
        $responseData=array();
        //if ($request->isMethod('POST') &&  $request->isXmlHttpRequest()) {
        if($request->getMethod() === "POST") {
            $title=$request->get('title');
            $cfirstName=$request->get('cfirstName');
            $lastName=$request->get('lastName');
            $emailAddress=$request->get('emailAddress');
            $telphoneMobile=$request->get('telphoneMobile');
            $linkedInUrl=$request->get('linkedInUrl');
            $highestQualification=$request->get('highestQualification');
            $currentPosition=$request->get('currentPosition');
            $currentEmployer=$request->get('currentEmployer');

            $postAppliedFor=$request->get('postAppliedFor');
            $carrerUpload=$request->files->get('carrerUpload');

            if(empty($cfirstName)){
                $error['cfirstName']="*Required";
            }
            /*if(empty($lastName)){
               $error['lastName']="*Required";
            }*/
            if(empty($emailAddress)){
                $error['emailAddress']="*Required";
            }else if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
                 $error['emailAddress']="*Opps, you need fill a valid email.";
            }
            if(empty($telphoneMobile)){
                $error['telphoneMobile']="*Required";
            }
            /*if(empty($highestQualification)){
                $error['highestQualification']="*Required";
            }*/
            if(empty($postAppliedFor)){
                $error['postAppliedFor']="*Required";
            }

            /*if(isset($linkedInUrl)){
                $linkedBaseUrl="https://www.linkedin.com/";
                if(!str_contains($linkedBaseUrl, $linkedInUrl)){
                    $error['linkedInUrl']="*invalid url.";
                }
            }*/
            if(empty($carrerUpload)){
                $error['carrerUpload']="*Required";
            }

            if(!empty($error)){
               $responseData=array("status"=>"failed",'code'=>"400","errorData"=>$error);
            }else{
                $candidate= new CareerInquiry();
                $key="CWU-".time();
                $candidate->setKey($key, 'object');
                $candidate->setParent(DefaultHelper::validateFolderPath(CAREER_INQUIRY_PATH));
                $candidate->setTitle($title);
                $candidate->setFirstName($cfirstName);
                $candidate->setLastName($lastName);
                $candidate->setHighestQualification($highestQualification);
                $candidate->setLinkedinURL($linkedInUrl);
                $candidate->setEmail($emailAddress);
                $candidate->setContact($telphoneMobile);
                $candidate->setCurrentPosition($currentPosition);
                $candidate->setCurrentEmployer($currentEmployer);
                $candidate->setPostAppliedFor($postAppliedFor);

                $path = '/3S Website Data/Career Request/';
                $folder = Service::createFolderByPath($path);
                $newAssetCareer = new \Pimcore\Model\Asset();
                $newAssetCareer->setFilename(time().' - '.$request->files->get('carrerUpload')->getClientOriginalName());
                $newAssetCareer->setData(file_get_contents($request->files->get('carrerUpload')));
                $newAssetCareer->setParentId($folder->getId());
                $newAssetCareer->save();

                $candidate->setResume($newAssetCareer);
                $candidate->setOmitMandatoryCheck(true);
                $candidate->setPublished(true);
                $candidate->save();
                

                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $mailReciver = $websiteConfig->get('MailNotification');
                $careerWithUs = array(
                    'title' => $title,
                    'firstname' => $cfirstName,
                    'lastname' => $lastName,
                    'highestQualification' => $highestQualification,
                    'linkedInUrl' => $linkedInUrl,
                    'emailAddress' => $emailAddress,
                    'telphoneMobile' => $telphoneMobile,
                    'currentPosition' => $currentPosition,
                    'currentEmployer'=> $currentEmployer,
                    'postAppliedFor' => $postAppliedFor,
                    'address' => $supplierAddress
                );
                $emialSubject ='';
                GeneralHelper::sendMail($mailReciver,'/3SWebsite/Emails/CareerWithUs/CareerWithUs',$careerWithUs,$emialSubject,$newAssetCareer);

                $added = $candidate->getId();
                $responseData=array("status"=>"success",'code'=>"200","candidateId"=>$added);
            }
            return new Response(json_encode($responseData));
        }else{
            $responseData=array("status"=>"failed",'code'=>"400","errorData"=>"Not A Valid Request.");
            return  new Response(json_encode($responseData));
        }
    }

}
