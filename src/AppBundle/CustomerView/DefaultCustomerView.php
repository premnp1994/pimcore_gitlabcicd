<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\CustomerView;

use CustomerManagementFrameworkBundle\Model\CustomerInterface;
use CustomerManagementFrameworkBundle\CustomerView\DefaultCustomerView as PimcoreDefaultCustomerView;


class DefaultCustomerView extends PimcoreDefaultCustomerView
{
    
   
    /**
     * @param CustomerInterface $customer
     *
     * @return string|null
     */
    public function getOverviewTemplate(CustomerInterface $customer)
    {
        return 'AppBundle:Customers/partials:list-row.html.php';
    }

    /**
     * @return string
     */
    public function getOverviewWrapperTemplate()
    {
        return 'AppBundle:Customers/partials:list-table.html.php';
    }

    public function getFilterWrapperTemplate()
    {
        return 'AppBundle:Customers/partials/list-filter:filter-wrapper.html.php';
    }

    public function getFieldsFilterTemplate()
    {
        return 'AppBundle:Customers/partials/list-filter:fields.html.php';
    }

    public function getSegmentsFilterTemplate()
    {
        return 'AppBundle:Customers/partials/list-filter:segments.html.php';
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return string|null
     */
    public function getDetailviewTemplate(CustomerInterface $customer)
    {
        return 'AppBundle:Customers/partials:detail.html.php';
    }

    
}
