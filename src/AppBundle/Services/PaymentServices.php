<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Services;

use Carbon\Carbon;
use GlobalBundle\AppHelpers\GeneralHelper;
use Razorpay\Api\Api;
use Pimcore\Model\User;

class PaymentServices
{
    
    public function cancelRenewal($object){
        
        $payment_key = GeneralHelper::getWebsiteSetting('PAYMENT_KEY');
        $payment_secret = GeneralHelper::getWebsiteSetting('PAYMENT_SECRET');
        $api = new Api($payment_key, $payment_secret);
       
        if($object->getSubscriptionOrder()){
            $subscriptionOrder = $object->getSubscriptionOrder();
            $subscriptionOrder->setIsRecurring(false);
            $subscriptionOrder->save();
            $subscriptionId = $object->getSubscriptionOrder()->getTransactionId();
    
            if($subscriptionId){
                $subscriptionDetails = $api->subscription->fetch($subscriptionId);
                
                if($subscriptionDetails['status'] != "cancelled"){
                    $options = ['cancel_at_cycle_end' => 0];
                    $api->subscription->fetch($subscriptionId)->cancel($options);
                }

            }
        }
    }
   
    public function cancelMembership($object){
     
        $this->cancelRenewal($object);

        if(!empty($object->getChildren())){
            if($object->geto_className() == "Company" ){
                foreach($object->getChildren() as $value){
                    $value->setActive(false);
                    $value->setIsAdmin(false);
                    $value->setPublished(false);
                    $value->save();
                }
            }
            if($object->geto_className() == "SupplierCompany" ){
                foreach($object->getChildren() as $value){
                    $value->setPublished(false);
                    $value->save();
                    $user = User::getByName($value->getEmail());
                    $user->setActive(false);
                    $user->save();
                }
            }
            $companyID = $object->getId();
            $db = \Pimcore\Db::get();
            $db->query("UPDATE element_workflow_state SET place = 'lead_setup' WHERE cid = ?", [$companyID]);
                             
            
        }
      
    }

    public function subscriptionTimeFinished(){

        $subscriptionOrderData = new \Pimcore\Model\DataObject\SubscriptionOrder\Listing();
        $date = Carbon::now()->timestamp;
        $subscriptionOrderData->setCondition("endDate <= ?", [$date]);
        $subscriptionOrder = $subscriptionOrderData->load();
        
        if(!empty($subscriptionOrder)){
            foreach($subscriptionOrder as $subOrderValue){
                    if(!empty($subOrderValue->getCompany())){
                        
                            $company = $subOrderValue->getCompany();
                            
                            $this->cancelMembership($company);

                            $msg = "Transection successfull id: {$subOrderValue->getTransactionId()}" . PHP_EOL;
                            \Pimcore\Log\Simple::log('subscriptionOrderCancelCron_Log', $msg . PHP_EOL); 
                    }else{
                        
                        $msg = "Company not found. Transection fail id:: {$subOrderValue->getTransactionId()}" . PHP_EOL;
                        \Pimcore\Log\Simple::log('subscriptionOrderCancelCron_Log', $msg . PHP_EOL);                        
                    }
              
            }
        }else{
            
            echo  $msg = "Subscription not found " . PHP_EOL;
            \Pimcore\Log\Simple::log('subscriptionOrderCancelCron_Log', $msg . PHP_EOL);                        
        }
    }
    


}
