<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Model\Product;

class Product extends \Pimcore\Model\DataObject\Product
{
    const OBJECT_TYPE_ACTUAL_CAR = 'actual-product';
    const OBJECT_TYPE_VIRTUAL_CAR = 'virtual-product';

    /**
     * @return string
     */
    public function getOSName()
    {
        return ($this->getEanCode() ? $this->getEanCode() . ' ' : '') . $this->getProductName();
    }

    /**
     * @return string
     */
    public function getSubText(): string
    {
        $textParts = [];

        return "<span class='text-nowrap'>" . implode("</span>, <span class='text-nowrap'>", array_filter($textParts)) . '</span>';
    }

    /**
     * @return int|string
     */
    public function getOSProductNumber()
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getOSIndexType()
    {
        return $this->getProductType() === self::OBJECT_TYPE_ACTUAL_CAR ? self::OBJECT_TYPE_VARIANT : self::OBJECT_TYPE_OBJECT;
    }

    /**
     * @return int
     */
    public function getOSParentId()
    {
        if ($this->getProductType() == self::OBJECT_TYPE_ACTUAL_CAR) {
            $parent = $this->getParent();
            while ($parent->getParent() instanceof self) {
                $parent = $parent->getParent();
            }

            return $parent->getId();
        }

        return parent::getOSParentId();
    }

    /**
     * @return Hotspotimage[]
     */
    public function getAdditionalImages(): array
    {
        $items = $this->getEssentialGallery()->getItems();

        $curateGallery = $this->getCurateGallery()->getItems();
        if ($curateGallery) {
            $items = array_merge($items, $curateGallery);
        }

        $signatureGallery = $this->getSignatureGallery()->getItems();
        if ($signatureGallery) {
            $items = array_merge($items, $signatureGallery);
        }
        return $items;
    }

    /**
     * @return \Pimcore\Model\DataObject\Category|null
     */
    public function getMainCategory(): ?\Pimcore\Model\DataObject\Category
    {
        return $this->getCategory();
    }

    /**
     * @return \Pimcore\Model\DataObject\Product[]
     */
    public function getColorVariants(): array
    {
        if ($this->getProductType() == self::OBJECT_TYPE_ACTUAL_CAR) {
            $parent = $this->getParent();

            $productSiblings = [];
            foreach ($parent->getChildren() as $sibling) {
                if ($sibling instanceof self && $sibling->getProductType() == self::OBJECT_TYPE_ACTUAL_CAR) {
                    $productSiblings[] = $sibling;
                }
            }

            return $productSiblings;
        }

        return [];
    }

    public function getElementAdminStyle()
    {
        if (empty($this->o_elementAdminStyle)) {
            $this->o_elementAdminStyle = new AdminStyle\Product($this);
        }

        return $this->o_elementAdminStyle;
    }
}
