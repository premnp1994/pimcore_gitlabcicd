<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Model\Product\AdminStyle;

use AppBundle\Website\Tool\ForceInheritance;
use Pimcore\Model\Element\AdminStyle;
use Pimcore\Model\Element\ElementInterface;

class Product extends AdminStyle
{
    /** @var ElementInterface */
    protected $element;

    public function __construct($element)
    {
        parent::__construct($element);

        $this->element = $element;

        if ($element instanceof \AppBundle\Model\Product\Product) {
            ForceInheritance::run(function () use ($element) {
                if ($element->getProductType() == 'virtual-product') {
                    $this->elementIcon = '/bundles/pimcoreadmin/img/twemoji/1f4e6.svg';
                }
            });
        }
    }
}
