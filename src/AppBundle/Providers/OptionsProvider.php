<?php

namespace AppBundle\Providers;

use Pimcore\Db;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\User;

/**
 * Class OptionsProvider
 * @package Cadence\
 */
class OptionsProvider implements SelectOptionsProviderInterface {
	const LOGGER_MODULE = 'Dynamic Select';
	const SEPARATOR = '||';
	const CACHE_TAGS = ['dynamic_dropdown_output_lifetime'];
	const CACHE_PRIORITY = 1000;

	/**
	 * @param array $context
	 * @param \Pimcore\Model\DataObject\ClassDefinition\Data\Select|string $fieldDefinition
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {
		$list = [];

		$dataProvider = $fieldDefinition->getOptionsProviderData();

		try {
			$masterData = explode(self::SEPARATOR, $dataProvider);

			if (empty(array_search("", $masterData))) {
				/** @var Concrete $masterClass */
				$masterClass = $masterData[0];
				$masterValueGetter = $masterData[1];
				$masterKeyGetter = $masterData[2];
					
				if($masterClass == "Role"){
						$roleMerchantHead = User\Role::getByName("MerchantHead");
						if(!empty($roleMerchantHead)){
							$roleMerchantHeadId = $roleMerchantHead->getId();
							$roleMercantExecutive = User\Role::getByName("MerchantExecutive");
							$roleMercantExecutiveId = 0;
							if(!empty($roleMercantExecutive)){
								$roleMercantExecutiveId = $roleMercantExecutive->getId();	
							}

							$sql = "SELECT name  as `key`, id  as `value` FROM users where roles in (".$roleMerchantHeadId.",".$roleMercantExecutiveId.")";

							$sql .= " ORDER BY `key`";
							
							$stmt = Db::get()->prepare($sql);
							$stmt->execute();

							$list = $stmt->fetchAll();
						}
					if ($fieldDefinition->getMandatory() && !empty($list)) {
						array_unshift($list, array("key" => "(Empty)", "value" => ""));
					}
				}		
			} else {
				// AppLogger::log(self::LOGGER_MODULE)->error('Class or Key, Value must not be empty. ' . $dataProvider);
			}
		} catch (\Exception $e) {
			// AppLogger::log(self::LOGGER_MODULE)->error($e->getMessage() . $dataProvider);
		}

		return $list;
	}

	/**
     * Returns the value which is defined in the 'Default value' field  
     * @param array $context 
     * @param Data $fieldDefinition 
     * @return mixed
     */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}
}
