<?php

namespace AppBundle\Controller;

use AppBundle\Services\PaymentServices;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject;
use Carbon\Carbon as Carbon;
use GlobalBundle\AppHelpers\GeneralHelper;
use Razorpay\Api\Api;
use Pimcore\Model\DataObject\SupplierCompany;
use Pimcore\Model\User;

class CustomerController extends FrontendController
{

    /**
     * @Route("/customer")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from customer');
    }

	public function emailAction(Request $request)
    {
        $this->view->CDN_URL = PIMCORE_CDN_BASEURL;
    }

    /**
    * Description:- After finished subscription date workflow change
    *
    * @Route("/subscriptionTimeFinished" , name="subscriptionTimeFinished");
    */
    public function subscriptionTimeFinished(){

        $subscriptionOrderData = new \Pimcore\Model\DataObject\SubscriptionOrder\Listing();
        $date = Carbon::now()->timestamp;
        $subscriptionOrderData->setCondition("endDate <= ?", [$date]);
        $subscriptionOrder=$subscriptionOrderData->load();

        foreach($subscriptionOrder as $subOrderValue){
            $company = $subOrderValue->getCompany();
            $companyID = $subOrderValue->getCompany()->getId();

            $db = \Pimcore\Db::get();
            $db->query("UPDATE element_workflow_state SET 'place' = 'lead_setup' WHERE cid = ?", [$companyID]);

            $paymentServices = new PaymentServices();
            $paymentServices->cancelMembership($company);

        }
    }


   /**
    * Description:- Current Status of Subscription(webhook URL)
    *
    * @Route("/subscriptionStatus");
    */
    public function subscriptionStatus(Request $request){


        $payment_key = GeneralHelper::getWebsiteSetting('PAYMENT_KEY');
        $payment_secret = GeneralHelper::getWebsiteSetting('PAYMENT_SECRET');
        $api = new Api($payment_key, $payment_secret);

        $json=file_get_contents('php://input');
        json_decode($json);

        $webhookSignature = hash_hmac('sha256', $json, $payment_secret);
        $api->utility->verifyWebhookSignature($json, $webhookSignature, $payment_secret);

        file_put_contents('data.txt', $json);
        $result = file_get_contents('data.txt');
        $result = json_decode($result, true);

        if($result['event'] == "invoice.paid" ){
            $subscriptionData = $result['payload']['invoice']['entity'];
            $subscriptionId = $subscriptionData['subscription_id'];
        }else{
            $subscriptionData = $result['payload']['subscription']['entity'];
            $subscriptionId =$subscriptionData['id'];
        }

        if(!empty($subscriptionId)){
            $subscriptionOrderData = DataObject\SubscriptionOrder::getByTransactionId($subscriptionId);
            $subscriptionOrder = $subscriptionOrderData->load();
            if(!empty($subscriptionOrder)){
               if($subscriptionData['status'] == "paid" || $subscriptionData['status'] == "active" ){
                    $subscriptionOrder[0]->setOrderStatus("success");
                    $startDate = Carbon::now();
                    $endDate = Carbon::now()->addYear();
                    $subscriptionOrder[0]->setStartDate($startDate);
                    $subscriptionOrder[0]->setEndDate($endDate);
                    $subscriptionOrder[0]->setPaymentInfo(json_encode($result));
                    $subscriptionOrder[0]->save();
                    if($subscriptionOrder[0]->getCompany()->getO_className() !=  "Company"){
                        $merchantAliasEmail = \Pimcore\Model\WebsiteSetting::getByName('MerchantAlias');
                        $merchantAlias = $merchantAliasEmail->getData();
                        $list = new User\Listing();
                        $list->setCondition('email = ?', $merchantAlias);
                        $userData = $list->load();
                        $paramUploadDoc = array('name'=>(!empty($userData[0]))?$userData[0]->getFirstname():"",'company'=>$subscriptionOrder[0]->getCompany()->getName());
                        GeneralHelper::sendMail($merchantAlias,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/PaymentConfirmationEmailMerchant',$paramUploadDoc);
                    }
                }else{
                    $subscriptionOrder[0]->setOrderStatus("fail");
                    $subscriptionOrder[0]->setPaymentInfo(json_encode($result));
                    $subscriptionOrder[0]->save();
                }
            }
        }

        header("HTTP/1.1 200 OK");exit;
    }
}
