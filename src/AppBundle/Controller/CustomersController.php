<?php

/**
 * Pimcore
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Controller;

use Pimcore\Model\DataObject;
use CustomerManagementFrameworkBundle\CustomerList\Filter\Exception\SearchQueryException;
use CustomerManagementFrameworkBundle\Import\CustomerImportService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject\Company;
use Zend\Paginator\Adapter\ArrayAdapter;
use CustomerManagementFrameworkBundle\Controller\Admin\CustomersController as PimcoreCustomerController;
/**
 * @Route("/customers")
 */
class CustomersController extends PimcoreCustomerController
{

    /**
     * @param Request $request
     * @Route("/list")
     * @return Response
     */
    public function listAction(Request $request, CustomerImportService $customerImportService)
    {
      
        $filters = $this->fetchListFilters($request);
        
        $orders = $this->fetchListOrder($request);
        $errors = $request->get('errors', []);
        $paginator = null;
        $customerView = \Pimcore::getContainer()->get('cmf.customer_view');

        try {
            $listing = $this->buildListing($filters, $orders);
            $paginator = $this->buildPaginator($request, $listing);
        } catch(SearchQueryException $e) {
            $errors[] = $customerView->translate('There was an error in you search query: %s', $e->getMessage());
        } catch(\Exception $e) {
            $errors[] = $customerView->translate('Error while building customer list: %s', $e->getMessage());
        }
        
        //empty paginator as the view expects a valid paginator
        if(null === $paginator) {
            $paginator = $this->buildPaginator($request, new ArrayAdapter([]));
        }
        
        $companyData = new DataObject\Company\Listing();
        $companyData->setUnpublished(true);
        $companyList = $companyData->load();
        
        $companyArray = array();
        foreach($companyList as $value){
            $companyArray[] = $value->getName();
        }

        $companyCustomerData = new DataObject\Customer\Listing();
        $companyCustomerList = $companyCustomerData->load();
        $companyCustomerArray = array();
        foreach($companyCustomerList as $values){
            $companyDetails = $values->getCompany();
            $companyCustomerArray[$values->geto_id()] = (!empty($companyDetails))?$companyDetails->getName():'';
        }
       //p_r($paginator);die;
        return $this->render(
            'AppBundle:Customers:list.html.php',
            [
                'segmentGroups' => $this->loadSegmentGroups(),
                'filters' => $filters,
                'errors' => $errors,
                'paginator' => $paginator,
                'customerView' => $customerView,
                'searchBarFields' => $this->getSearchHelper()->getConfiguredSearchBarFields(),
                'request' => $request,
                'clearUrlParams' => $this->clearUrlParams,
                'filterDefinitions' => $this->getFilterDefinitions(),
                'filterDefinition' => $this->getFilterDefinition($request),
                'accessToTempCustomerFolder' => boolval($this->hasUserAccessToTempCustomerFolder()),
                'hideAdvancedFilterSettings' => boolval($request->get('segmentId')),
                'customerImportService' => $customerImportService,
                'companyList'=>$companyArray,
                'companyCustomerList'=>$companyCustomerArray
            ]
        );
    }

    /**
     * @param array $filters
     * @param array $orders
     * @return Listing\Concrete
     */
    protected function buildListing(array $filters = [], array $orders = [])
    {
       // p_r($filters); die;
        /** @var Listing|Listing\Concrete $listing */
        $listing = $this->getSearchHelper()->getCustomerProvider()->getList();
         //p_r($listing); die;

        if (count($orders) > 0) {
            $listing
                ->setOrderKey(array_keys($orders), false)
                ->setOrder(array_values($orders), false);
        } else {
            $listing
                ->setOrderKey('o_id')
                ->setOrder('ASC');
        }

        $this->getSearchHelper()->addListingFilters($listing, $filters, $this->getAdminUser());
       
        return $listing;
    }
}
