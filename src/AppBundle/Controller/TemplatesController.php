<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Controller;

use Pimcore\Model\Document\PageSnippet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Controller\Controller;
use AppBundle\Newsletter\ProviderHandler\TemplateExporter;
use Pimcore\Model\Document;
use CustomerManagementFrameworkBundle\Event\Newsletter\Mailchimp\TemplateExportResolveProviderHandlerEvent;
use CustomerManagementFrameworkBundle\Newsletter\ProviderHandler\Mailchimp;
use CustomerManagementFrameworkBundle\Traits\ApplicationLoggerAware;
use Pimcore\Helper\Mail;

/**
 * @Route("/templates")
 */
class TemplatesController extends Controller
{
    use ApplicationLoggerAware;

    const LIST_ID_PLACEHOLDER = 'global';

    /**
     * @var NewsletterManagerInterface
     */
    private $newsletterManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var MailChimpExportService
     */
    private $exportService;
    
    /**
     * @param Request $request
     * @Route("/export")
     */
    public function exportAction(Request $request)
    {
       
        $document = PageSnippet::getById($request->get('document_id'));
        if ($document) {
           
           $data =  $this->exportTemplate($document);

            return $this->adminJson(['success' => true,'data' => $data]);
        } else {
            throw new \Exception(sprintf('Document {%s} not found!', $request->get('document_id')));
        }
    }

    /**
     * Returns a JsonResponse that uses the admin serializer
     *
     * @param mixed $data    The response data
     * @param int $status    The status code to use for the Response
     * @param array $headers Array of extra headers to add
     * @param array $context Context to pass to serializer when using serializer component
     * @param bool $useAdminSerializer
     *
     * @return JsonResponse
     */
    protected function adminJson($data, $status = 200, $headers = [], $context = [], bool $useAdminSerializer = true)
    {
        $json = $this->encodeJson($data, $context, JsonResponse::DEFAULT_ENCODING_OPTIONS, $useAdminSerializer);

        return new JsonResponse($json, $status, $headers, true);
    }

     /**
     * Encodes data into JSON string
     *
     * @param mixed $data    The data to be encoded
     * @param array $context Context to pass to serializer when using serializer component
     * @param int $options   Options passed to json_encode
     * @param bool $useAdminSerializer
     *
     * @return string
     */
    protected function encodeJson($data, array $context = [], $options = JsonResponse::DEFAULT_ENCODING_OPTIONS, bool $useAdminSerializer = true)
    {
        /** @var SerializerInterface $serializer */
        $serializer = null;

        if ($useAdminSerializer) {
            $serializer = $this->container->get('pimcore_admin.serializer');
        } else {
            $serializer = $this->container->get('serializer');
        }

        return $serializer->serialize($data, 'json', array_merge([
            'json_encode_options' => $options,
        ], $context));
    }

    public function exportTemplate(Document\PageSnippet $document)
    {
        //  $exportService = $this->resolveProviderHandler($document)->getExportService();
        //  $apiClient = $exportService->getApiClient();

        // $remoteId = $exportService->getRemoteId($document, self::LIST_ID_PLACEHOLDER);
       
        $html = \Pimcore\Model\Document\Service::render($document);
      
        //dirty hack to prevent absolutize unsubscribe url placeholder of mailchimp
        $html = str_replace(["*|UNSUB|*", "*|FORWARD|*", "*|UPDATE_PROFILE|*", "*|ARCHIVE|*"], ["data:*|UNSUB|*", "data:*|FORWARD|*", "data:*|UPDATE_PROFILE|*", "data:*|ARCHIVE|*"], $html);
       
        // modifying the content e.g set absolute urls...
        $html = Mail::embedAndModifyCss($html, $document);
        $html = Mail::setAbsolutePaths($html, $document);

        //dirty hack to make sure mailchimp merge tags are not url-encoded
        $html = str_replace("*%7C", "*|", $html);
        $html = str_replace("%7C*", "|*", $html);

        //dirty hack to prevent absolutize unsubscribe url placeholder of mailchimp
        $html = str_replace(["data:*|UNSUB|*", "data:*|FORWARD|*", "data:*|UPDATE_PROFILE|*", "data:*|ARCHIVE|*"], ["*|UNSUB|*", "*|FORWARD|*", "*|UPDATE_PROFILE|*", "*|ARCHIVE|*"], $html);


        $fileName =  $document->getKey().'.html';
       
        // $myfile = fopen($fileName, "w") or die("Unable to open file!");
        // fwrite($myfile, $html);
        // fclose($myfile);
        
        $data = array("filename" => $document->getKey().'.html',"html" => $html);

        return $data;
        //return \Pimcore\Tool::getHostUrl().'/'.$fileName;

        



























        // //check if template really exists in MailChimp
        // if ($remoteId) {
        //     $result = $apiClient->get("templates/$remoteId");
        //     if ($apiClient->success() && $result['id'] && $result['active']) {
        //         $templateExists = true;
        //     }
        // }

        // $templateName = substr($document->getKey(), 0, 35) . ' [ID ' . $document->getID() . ']';

        // if ($remoteId && $templateExists) {
        //     $this->getLogger()->info(
        //         sprintf(
        //             '[MailChimp] Updating new Template with name %s based on document id %s',
        //             $templateName,
        //             $document->getId()
        //         )
        //     );

        //     $result = $apiClient->patch("templates/$remoteId", [
        //         'name' => $templateName,
        //         'html' => $html
        //     ]);
        // } else {
        //     $this->getLogger()->info(
        //         sprintf(
        //             '[MailChimp][Template] Creating new template with name %s based on document id %s',
        //             $templateName,
        //             $document->getId()
        //         )
        //     );

        //     $result = $apiClient->post('templates', [
        //         'name' => $templateName,
        //         'html' => $html
        //     ]);
        // }

        // if ($apiClient->success()) {
        //     $remoteId = $result['id'];
        //     $exportNote = $exportService->createExportNote($document, self::LIST_ID_PLACEHOLDER, $remoteId);
        //     $exportNote->save();
        // } else {
        //     $this->getLogger()->error(
        //         sprintf(
        //             '[MailChimp][Template] Failed to export template %s: %s %s',
        //             $templateName,
        //             json_encode($apiClient->getLastError()),
        //             $apiClient->getLastResponse()['body']
        //         ),
        //         [
        //             'relatedObject' => $document
        //         ]
        //     );

        //     throw new \Exception('[MailChimp] Creating new Template failed: ' . json_encode($apiClient->getLastError()));
        // }
    }
      /**
     * @param Document\PageSnippet $document
     * @return Mailchimp
     * @throws \Exception
     */
    protected function resolveProviderHandler(Document\PageSnippet $document): Mailchimp
    {
        $event = new TemplateExportResolveProviderHandlerEvent($document);
        $this->eventDispatcher->dispatch($event->getName(), $event);
        if(!empty($event->getProviderHandler())) {
            return $event->getProviderHandler();
        }

        foreach($this->newsletterManager->getNewsletterProviderHandlers() as $newsletterProviderHandler) {
            if($newsletterProviderHandler instanceof Mailchimp) {
                return $newsletterProviderHandler;
            }
        }

        throw new \Exception('No mailchimp provider handlers are registered');
    }
}
