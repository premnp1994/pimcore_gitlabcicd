<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Db;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use \Pimcore\Model\DataObject;

class DefaultController extends FrontendController
{

    public function defaultAction(Request $request)
    {
    }

    public function SubmitForReviewMailAction(Request $request)
    {
    }

    public function BookADemoAction(Request $request)
    {
    }

    public function ContactUsAction(Request $request)
    {
    }

    public function BecomeASupplierAction(Request $request)
    {
    }

    public function CareerWithUsAction(Request $request)
    {
    }

    /**
     * return description from description preset object on preset selection
     * @Route("/getDescription")
     */
    public function getDescriptionAction(Request $request)
    {
        $myObject = DataObject\Product::getById($request->get('id'));

        if (!empty($request->get('presetId'))) {
            $productData = DataObject\DescriptionPreset::getById($request->get('presetId'));

            $presetDescription = $productData->getDescription('en');
        }

        return $this->json($presetDescription);
    }

    /**
     * return state master data list on country selection
     * @Route("/getStates")
     */
    public function getStatesAction(Request $request)
    {
        $contry = $request->get('country');
        if (!empty($contry)) {
            $sql = "SELECT name  as `key`, o_id  as `value` FROM object_STT where country = " . "'" . $contry . "'";
            $sql .= " ORDER BY `key`";
            $stmt = Db::get()->prepare($sql);
            $stmt->execute();
            $list = $stmt->fetchAll();
            if (!empty($list)) {
                array_unshift($list, array("key" => "(Empty)", "value" => ""));
                return $this->json(["success" => true, "list" => $list]);
            } else {
                $list = array("key" => "(Empty)", "value" => "");
                return $this->json(["success" => true, "list" => $list]);
            }
        } else {
            $list = array("key" => "(Empty)", "value" => "");
            return $this->json(["success" => true, "list" => $list]);
        }
    }

    /**
     * * return city master data list on state selection
     *   @Route("/getCities")
     */
    public function getCitiesAction(Request $request)
    {
        $state = $request->get('state');
        if (!empty($state)) {
            $sql = "SELECT name  as `key`, o_id  as `value` FROM object_CT where state = " . "'" . $state . "'";
            $sql .= " ORDER BY `key`";
            $stmt = Db::get()->prepare($sql);
            $stmt->execute();
            $list = $stmt->fetchAll();
            if (!empty($list)) {
                array_unshift($list, array("key" => "(Empty)", "value" => ""));
                return $this->json(["success" => true, "list" => $list]);
            } else {
                $list = array("key" => "(Empty)", "value" => "");
                return $this->json(["success" => true, "list" => $list]);
            }
        } else {
            $list = array("key" => "(Empty)", "value" => "");
            return $this->json(["success" => true, "list" => $list]);
        }
    }
}
