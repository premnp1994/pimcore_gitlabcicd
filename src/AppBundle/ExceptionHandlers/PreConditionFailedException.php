<?php

namespace AppBundle\ExceptionHandlers;

/**
 * Class PreConditionFailedException
 * @package PdpBundle\Api\ExceptionHandlers
 */
class PreConditionFailedException extends \Exception
{

}
