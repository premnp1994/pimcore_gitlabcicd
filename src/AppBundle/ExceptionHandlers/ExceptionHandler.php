<?php

namespace AppBundle\ExceptionHandlers;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ExceptionHandler
 * @package PdpBundle\Api\ExceptionHandlers
 */
class ExceptionHandler
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * ExceptionHandler constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $message
     * @throws  HttpException
     */
    public function preConditionFailedException($message)
    {
        $this->logger->error($message);
        throw new HttpException("200", $message);//412
    }

    /**
     * @param $message
     * @throws  HttpException
     */
    public function badRequestException($message)
    {
        $this->preConditionFailedException($message);//400
    }

    /**
     * @param $message
     * @throws  HttpException
     */
    public function retryException($message)
    {
        $this->preConditionFailedException($message);//449
    }

    /**
     * @param $message
     * @throws  HttpException
     */
    public function conflictException($message)
    {
        $this->preConditionFailedException($message);//409
    }
}
