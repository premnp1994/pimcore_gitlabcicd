<?php

namespace AppBundle\ExceptionHandlers;

/**
 * Class BadRequestException
 * @package PdpBundle\Api\ExceptionHandlers
 */
class BadRequestException extends \Exception
{

}
