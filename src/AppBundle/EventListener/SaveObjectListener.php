<?php
namespace AppBundle\EventListener;

use AppBundle\Model\Product\AdminStyle\Product;
use Pimcore\Event\Model\DataObjectEvent;
use GlobalBundle\AppHelpers\GeneralHelper;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Razorpay\Api\Api;

class SaveObjectListener {
    public function __construct() {
        $this->sessionData = \Pimcore\Tool\Authentication::authenticateSession();
    }

    /**
     * Functional Description: This function is used to dump aca database after save aca object.
     * @param DataObjectEvent $e
     * @throws \Exception
     */
    public function onPreUpdate(DataObjectEvent $e) {

        $object = $e->getObject();
        /*if($object instanceof DataObject\Product){

            $assetsParent = Asset::getByPath($object->getPath());
            if(!empty($assetsParent)){
                $assetsDep = $object->getDependencies()->requires;

                foreach ($assetsDep as $assetsKey => $assetsValue){
                    if ($assetsValue['type'] === 'asset'){
                        $asset = Asset::getById($assetsValue['id']);
                        if($assetsParent->getId() != $asset->getParentId()){
                            $asset->setParent($assetsParent);
                            $asset->save();
                        }
                    }
                }
            }

        }*/
        if($object->getO_type() != 'folder'){
            if ($object->getClassName() == 'Subscription') {

                if($object->isPublished()){

                    //$api = new Api(PAYMENT_KEY, PAYMENT_SECRET);
                    $payment_key = GeneralHelper::getWebsiteSetting('PAYMENT_KEY');
                    $payment_secret = GeneralHelper::getWebsiteSetting('PAYMENT_SECRET');
                    $api = new Api($payment_key, $payment_secret);
                    $PaymentCurrency = GeneralHelper::getWebsiteSetting('PaymentCurrency');

                    $plan = $api->plan->create(array(
                        'period' => 'yearly',
                        'interval' => 1,
                        'item' => array(
                        'name' => $object->getName(),
                        'description' => $object->getDescription(),
                        'amount' => $object->getAnnualCost()*100,
                        'currency' => $PaymentCurrency
                        )
                        ));

                    $object->setPlanId($plan['id']);
                    $planInfo = json_encode($plan->toArray());
                    $object->setPlanInfo($planInfo);

                }
            }
        }
    }

    /**
     * Description: This function is used to dump aca database after save aca object.
     * @param DataObjectEvent $e
     * @throws \Exception
     */
    public function onPostUpdate(DataObjectEvent $e) {

        $object = $e->getObject();
        if($object instanceof DataObject\Product){

            $assetsParent = Asset::getByPath($object->getPath());
            if(!empty($assetsParent)){
                $assetsDep = $object->getDependencies()->requires;

                foreach ($assetsDep as $assetsKey => $assetsValue){
                    if ($assetsValue['type'] === 'asset'){
                        $asset = Asset::getById($assetsValue['id']);
                        if($assetsParent->getId() != $asset->getParentId()){
                            $asset->setParent($assetsParent);
                            $asset->save();
                        }
                    }
                }
            }

        }
    }

    public function onPreAdd(DataObjectEvent $e) {
        $object = $e->getObject();
        if($object->getO_type() != 'folder'){
            $className = $object->geto_className();
            switch ($className) {
                case "Agenda":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_AGENDA));
                    break;
                case "Being":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_BEING));
                    break;
                case "City":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_CITY));
                  break;
                case "Color":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_COLOR));
                    break;
                case "Compliance":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_COMPLIANCE));
                  break;
                case "Construction":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_CONSTRUCTION));
                    break;
                case "CrossCategory":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_CROSS_CATEGORY));
                  break;
                case "Currency":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_CURRENCY));
                    break;
                case "DescriptionPreset":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_DESCRIPTION_PRESET));
                  break;
                case "Designation":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_DESIGNATION));
                    break;
                case "DesignType":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_DESIGN_TYPE));
                  break;
                case "Event":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_EVENT));
                    break;
                case "Finish":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_FINISH));
                  break;
                case "FunctionData":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_FUNCTION));
                    break;
                case "Holiday":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_HOLIDAY));
                  break;
                case "Ids":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_IDS));
                    break;
                case "LeadTime":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_LEAD_TIME));
                  break;
                case "LifeStyleCollection":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_LIFESTYLE_COLLECTION));
                    break;
                case "Material":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_MATERIAL));
                  break;
                // case "MiSourceContact":
                //     $object->setParent(GeneralHelper::validateFolderPath(MASTER_MISOURCECONTACT));
                //     break;
                case "OrderStatus":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_ORDER_STATUS));
                  break;
                case "Port":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_PORT));
                    break;
                case "PriceLevel":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_PRICE_LEVEL));
                  break;
                case "Season":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_SEASON));
                    break;
                case "SellingUnit":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_SELLING_UNIT));
                  break;
                case "SiteMenu":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_SITEMENU));
                    break;
                case "Space":
                  $object->setParent(GeneralHelper::validateFolderPath(MASTER_SPACE));
                  break;
                case "State":
                    $object->setParent(GeneralHelper::validateFolderPath(MASTER_STATE));
                    break;
                case "NewsResourceCasestudy":
                    if($object->getContentType() == "News"){
                        $object->setParent(GeneralHelper::validateFolderPath(WEBSITE_NEWS));
                    }elseif($object->getContentType() == "Resources"){
                        $object->setParent(GeneralHelper::validateFolderPath(WEBSITE_RESOURCES));
                    }else{
                        $object->setParent(GeneralHelper::validateFolderPath(WEBSITE_CASE_STUDIES));
                    }
                    break;
                case "LeaderShipTeam":
                    if($object->getSectionType() == "Team"){
                        $object->setParent(GeneralHelper::validateFolderPath(WEBSITE_OUR_TEAM));
                    }else{
                        $object->setParent(GeneralHelper::validateFolderPath(WEBSITE_LEADERSHIP_TEAM));
                    }
                    break;
                case "Subscription":
                    if($object->getUserType() == "Customer"){
                        $object->setParent(GeneralHelper::validateFolderPath(SUBSCRIPTION_CUSTOMER));
                    }else{
                        $object->setParent(GeneralHelper::validateFolderPath(SUBSCRIPTION_SUPPLIER));
                    }
                    break;
                default:
                  //$object->setParent(GeneralHelper::validateFolderPath('/home'));
              }

        }
    }
}
