<?php
/**
 * @package  BayerBundle
 *
 * @author Bhavika Matariya
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Model\DataObject\DataObjectImportEvent;
use Symfony\Component\EventDispatcher\Event;
use Pimcore\Model\DataObject\Finish;
use Pimcore\Model\DataObject\Material;
use Pimcore\Model\DataObject\Ids;
use Pimcore\Model\DataObject\Product;
use Pimcore\Tool\Admin;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;


class ImportObjectListener
{
    public function onPreImport(Event $e)
    {
        try {

            $obj = $e->getObject();
            if ($obj instanceof Finish || $obj instanceof Material || $obj instanceof Ids || $obj instanceof Product) {
                $obj = $e->getObject();

                if($obj instanceof Product){
                    $templateName = $e->getConfig()->shareSettings->configName;
                    $proRowData = $e->getRowData();

                    $user = Admin::getCurrentUser();
                    if ($user == null) {
                     $user = \Pimcore\Tool\Session::getReadonly()->get("user");
                    }

                    // Supplier Import event START
                    //$roleId = $user->getRoles()[0];SupplierCsvTemplate
                    $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                    $supplier = $websiteConfig->get('SupplierCsvTemplate');
                    if ($templateName == $supplier) {

                        //For asign supplier
                        $supplireName = $user->getName();
                        $supplierObject = new DataObject\Supplier\Listing();
                        $supplierObject->setCondition("o_key = ?", [$supplireName]);
                        $supplierObject->setLimit(1);
                        $loadSupplier = $supplierObject->load();
                        if(!empty($loadSupplier)){
                            $supplierCompany = $loadSupplier[0]->getCompany();
                            //p_r($supplierCompany);die;
                            $obj->setSupplier($supplierCompany);
                        }

                        // for product type
                        $productType = $proRowData[1];
                        if($productType == 'actual-product' || $productType == 'virtual-product'){
                            //$obj->setColor($items);
                        }else{
                            throw new \Exception('invalid product type value', 1);
                        }

                        // for category
                        $productCategory = $proRowData[3];
                        $categoryObject = DataObject\Category::getByPath($productCategory,1);
                        if(empty($categoryObject)){
                            throw new \Exception('invalid category value', 1);
                        }

                        //For main images
                        $mainImage = $proRowData[6];
                        if(!empty($mainImage)){
                            $newAsset = Asset::getByPath($mainImage);
                            if(empty($newAsset)){
                                throw new \Exception('invalid main image added in CSV', 1);
                            }
                        }


                        //For EssentialGallery images
                        $galleryImage = $proRowData[7];
                        if(!empty($galleryImage)){
                            $imgArray = explode(',', $galleryImage);
                            $galleryData = $imgArray;

                            $items = [];
                            foreach($galleryData as $img){
                                $newAsset = Asset::getByPath($img);
                                if(!empty($newAsset)){
                                    $advancedImage = new \Pimcore\Model\DataObject\Data\Hotspotimage();
                                    $advancedImage->setImage($newAsset);
                                    $items[] = $advancedImage;
                                }else{
                                    throw new \Exception('invalid essentialGallery image added in CSV', 1);
                                }
                            }
                            $obj->setEssentialGallery(new \Pimcore\Model\DataObject\Data\ImageGallery($items));
                        }


                        // for color
                        $productColor = $proRowData[8];
                        //if($productType == 'actual-product' || $productType == 'virtual-product'){
                        if(!empty($productColor)){
                            $colorArray = explode(',', $productColor);
                            $items = [];
                            foreach($colorArray as $color){
                                $colorObject = DataObject\Color::getByName($color,1);
                                $loadColor = $colorObject->load();
                                if(!empty($loadColor)){
                                    $items[] = $loadColor[0]->getId();
                                }else{
                                    throw new \Exception('invalid color value', 1);
                                }
                            }
                            $obj->setColor($items);
                        }


                        // for finish
                        $productFinish = $proRowData[9];
                        if(!empty($productFinish)) {
                            $finishArray = explode(',', $productFinish);
                            $items = [];
                            foreach ($finishArray as $finish) {
                                $finishObject = DataObject\Finish::getByName($finish, 1);
                                $loadFinish = $finishObject->load();
                                if (!empty($loadFinish)) {
                                    $items[] = $loadFinish[0]->getId();
                                } else {
                                    throw new \Exception('invalid finish value', 1);
                                }
                            }
                            $obj->setFinish($items);
                        }

                        // for material
                        $productMaterial = $proRowData[10];
                        if(!empty($productMaterial)) {
                            $materialArray = explode(',', $productMaterial);
                            $items = [];
                            foreach ($materialArray as $material) {
                                $materialObject = DataObject\Material::getByName($material, 1);
                                $loadMaterial = $materialObject->load();
                                if (!empty($loadMaterial)) {
                                    $items[] = $loadMaterial[0]->getId();
                                } else {
                                    throw new \Exception('invalid material value', 1);
                                }
                            }
                            $obj->setMaterial($items);
                        }

                        // for sampleLeadTime //
                        $productSampleLeadTime = $proRowData[11];
                        if(!empty($productSampleLeadTime)) {
                            $leadTimeObject = DataObject\LeadTime::getByName($productSampleLeadTime, 1);
                            $loadLeadTime = $leadTimeObject->load();
                            if (!empty($loadLeadTime)) {
                                $leadTimeID = $loadLeadTime[0]->getId();
                                $obj->setSampleLeadTime($leadTimeID);
                            } else {
                                throw new \Exception('invalid sample lead time value', 1);
                            }
                        }

                        $productSampleLeadTime = $proRowData[12];
                        if(!empty($productSampleLeadTime)){
                            $leadTimeObject = DataObject\LeadTime::getByName($productSampleLeadTime,1);
                            $loadLeadTime = $leadTimeObject->load();
                            if(!empty($loadLeadTime)){
                                $leadTimeID = $loadLeadTime[0]->getId();
                                $obj->setProductionLeadTime($leadTimeID);
                            }else{
                                throw new \Exception('invalid production lead time value', 1);
                            }
                        }


                        // for selling unit
                        $productSellUnit = $proRowData[13];
                        if(!empty($productSellUnit)){
                            $sellingUniObject = DataObject\SellingUnit::getByName($productSellUnit,1);
                            $loadSellingUnit = $sellingUniObject->load();
                            if(!empty($loadSellingUnit)){
                                $sellingUnitID = $loadSellingUnit[0]->getId();
                                $obj->setSellingUnit($sellingUnitID);
                            }else{
                                throw new \Exception('invalid selling unit value', 1);
                            }
                        }


                        // for isFCL
                        $isFCL = $proRowData[18];
                        if(!empty($isFCL)){
                            if($isFCL == 'Both' || $isFCL == 'LCL' || $isFCL == 'FCL'){
                                //$obj->setisFcl($isFCL);
                            }else{
                                throw new \Exception('invalid isFCL value', 1);
                            }
                        }

                    }
                    // Supplier Import Event END


                    // Marchant Import event START
                    $marchantRoleID = $websiteConfig->get('MerchantCsvTemplate');
                    if ($templateName == $marchantRoleID) {

                        // for category
                        $productCategory = $proRowData[0];
                        if(!empty($productCategory)){
                            $categoryObject = DataObject\Category::getByPath($productCategory,1);
                            if(empty($categoryObject)){
                                throw new \Exception('invalid category value', 1);
                            }
                        }

                        // for Supplier
                        $productSupplier = $proRowData[1];
                        if(!empty($productSupplier)){
                            $supplierObject = DataObject\SupplierCompany::getByPath($productSupplier,1);
                            if(empty($supplierObject)){
                                throw new \Exception('invalid supplier value', 1);
                            }
                        }


                        // for Construction
                        $productConstruction = $proRowData[6];
                        if(!empty($productConstruction)){
                            $constArray = explode(',', $productConstruction);
                            $items = [];
                            foreach($constArray as $const){
                                $constructionObject = DataObject\Construction::getByName($const,1);
                                $loadConstruction = $constructionObject->load();
                                if(!empty($loadConstruction)) {
                                    $items[] = $loadConstruction[0]->getId();
                                }else{
                                    throw new \Exception('invalid construction value', 1);
                                }
                            }
                            $obj->setConstruction($items);
                        }


                        //For main images
                        $mainImage = $proRowData[11];
                        if(!empty($mainImage)){
                            $newAsset = Asset::getByPath($mainImage);
                            if(empty($newAsset)){
                                throw new \Exception('invalid main image added in CSV', 1);
                            }
                        }


                        //For EssentialGallery images
                        $galleryImage = $proRowData[12];
                        if(!empty($galleryImage)){
                            $imgArray = explode(',', $galleryImage);
                            $galleryData = $imgArray;

                            $items = [];
                            foreach($galleryData as $img){
                                $newAsset = Asset::getByPath($img);
                                if(!empty($newAsset)){
                                    $advancedImage = new \Pimcore\Model\DataObject\Data\Hotspotimage();
                                    $advancedImage->setImage($newAsset);
                                    $items[] = $advancedImage;
                                }else{
                                    throw new \Exception('invalid EssentialGallery images added in CSV', 1);
                                }

                            }
                            $obj->setEssentialGallery(new \Pimcore\Model\DataObject\Data\ImageGallery($items));
                        }



                        // for material
                        $productMaterial = $proRowData[13];
                        if(!empty($productMaterial)){
                            $materialArray = explode(',', $productMaterial);
                            $items = [];
                            foreach($materialArray as $material){
                                $materialObject = DataObject\Material::getByName($material,1);
                                $loadMaterial = $materialObject->load();
                                if(!empty($loadMaterial)){
                                    $items[] = $loadMaterial[0]->getId();
                                }else{
                                    throw new \Exception('invalid material value', 1);
                                }
                            }
                            $obj->setMaterial($items);
                        }



                        // for color
                        $productColor = $proRowData[14];
                        if(!empty($productColor)){
                            $colorArray = explode(',', $productColor);
                            $items = [];
                            foreach($colorArray as $color){
                                $colorObject = DataObject\Color::getByName($color,1);
                                $loadColor = $colorObject->load();
                                if(!empty($loadColor)){
                                    $items[] = $loadColor[0]->getId();
                                }else{
                                    throw new \Exception('invalid color value', 1);
                                }
                            }
                            $obj->setColor($items);
                        }


                        // for finish
                        $productFinish = $proRowData[15];
                        if(!empty($productFinish)){
                            $finishArray = explode(',', $productFinish);
                            $items = [];
                            foreach($finishArray as $finish){
                                $finishObject = DataObject\Finish::getByName($finish,1);
                                $loadFinish = $finishObject->load();
                                if(!empty($loadFinish)){
                                    $items[] = $loadFinish[0]->getId();
                                }else{
                                    throw new \Exception('invalid finish value', 1);
                                }
                            }
                            $obj->setFinish($items);
                        }


                        // for sampleLeadTime //
                        $productSampleLeadTime = $proRowData[16];
                        if(!empty($productSampleLeadTime)){
                            $leadTimeObject = DataObject\LeadTime::getByName($productSampleLeadTime,1);
                            $loadLeadTime = $leadTimeObject->load();
                            if(!empty($loadLeadTime)){
                                $leadTimeID = $loadLeadTime[0]->getId();
                                $obj->setSampleLeadTime($leadTimeID);
                            }else{
                                throw new \Exception('invalid sample lead time value', 1);
                            }
                        }


                        $productSampleLeadTime = $proRowData[17];
                        if(!empty($productSampleLeadTime)){
                            $leadTimeObject = DataObject\LeadTime::getByName($productSampleLeadTime,1);
                            $loadLeadTime = $leadTimeObject->load();
                            if(!empty($loadLeadTime)){
                                $leadTimeID = $loadLeadTime[0]->getId();
                                $obj->setProductionLeadTime($leadTimeID);
                            }else{
                                throw new \Exception('invalid production lead time value', 1);
                            }
                        }


                        // for isFCL
                        $isFCL = $proRowData[18];
                        if(!empty($isFCL)){
                            if($isFCL == 'Both' || $isFCL == 'LCL' || $isFCL == 'FCL'){
                                //$obj->setisFcl($isFCL);
                            }else{
                                throw new \Exception('invalid isFCL value', 1);
                            }
                        }



                        // for selling unit
                        $productSellUnit = $proRowData[19];
                        if(!empty($productSellUnit)){
                            $sellingUniObject = DataObject\SellingUnit::getByName($productSellUnit,1);
                            $loadSellingUnit = $sellingUniObject->load();
                            if(!empty($loadSellingUnit)){
                                $sellingUnitID = $loadSellingUnit[0]->getId();
                                $obj->setSellingUnit($sellingUnitID);
                            }else{
                                throw new \Exception('invalid selling unit value', 1);
                            }
                        }


                        // for Port
                        $productPort = $proRowData[21];
                        if(!empty($productPort)){
                            $portObject = DataObject\Port::getByPath($productPort,1);
                            if(empty($portObject)){
                                throw new \Exception('invalid port value', 1);
                            }
                        }


                        // for Price Lavel
                        $productPriceLavel = $proRowData[26];
                        if(!empty($productPriceLavel)){
                            $priceLavelArray = explode(',', $productPriceLavel);
                            $items = [];
                            foreach($priceLavelArray as $priceLavel){
                                $priceLavelObject = DataObject\PriceLevel::getByName($priceLavel,1);
                                $loadPriceLavel = $priceLavelObject->load();
                                if(!empty($loadPriceLavel)) {
                                    $items[] = $loadPriceLavel[0]->getId();
                                }else{
                                    throw new \Exception('invalid price lavel value', 1);
                                }
                            }
                            $obj->setPriceLevel($items);
                        }


                        // for Agenda
                        $productAgenda = $proRowData[30];
                        if(!empty($productAgenda)){
                            $agendaArray = explode(',', $productAgenda);
                            $items = [];
                            foreach($agendaArray as $agenda){
                                $agendaObject = DataObject\Agenda::getByName($agenda,1);
                                $loadAgenda = $agendaObject->load();
                                if(!empty($loadAgenda)) {
                                    $items[] = $loadAgenda[0]->getId();
                                }else{
                                    throw new \Exception('invalid agenda value', 1);
                                }
                            }
                            $obj->setAgenda($items);
                        }

                    }
                    // Supplier Import Event END 12 13 14
                }
                $obj->setOmitMandatoryCheck(true);
                $obj->setPublished(false);
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("There is some issue.", 1);
        }
    }


    public function onPostImport(Event $e)
    {
        try {

            $db = \Pimcore\Db::get();
            $obj = $e->getObject();
            $className =  $obj->getClassName();

            if($className == 'Product'){
                $user = Admin::getCurrentUser();
                if ($user == null) {
                 $user = \Pimcore\Tool\Session::getReadonly()->get("user");
                }

                $templateName = $e->getConfig()->shareSettings->configName;

                //$roleId = $user->getRoles()[0];
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $supplierTamplate = $websiteConfig->get('SupplierCsvTemplate');
                //$roleArray = explode(",",$SupplierCsvTemplate);

                $merchantTamplate = $websiteConfig->get('MerchantCsvTemplate');
                //$merchantRoleArray = explode(",",$MerchantCsvTemplate);

                if ($templateName == $supplierTamplate || $templateName == $merchantTamplate) {
                    $obj = $e->getObject();
                    $objId = $obj->getId();

                    $sth = $db->prepare("SELECT * FROM element_workflow_state WHERE cid = '".$objId."'");
                    $sth->execute();

                    if(!empty($row = $sth->fetch())){

                        if($row['place'] == 'advance_published'){
                            $updtnewreq = $db->query('UPDATE element_workflow_state SET place = "quotation_attributes_review,curation_attributes_review,generate_EAN_code" WHERE cid = "'.$objId.'"');
                            $updtnewreq->execute();
                            $obj->setPublished(true);
                            $obj->save();
                        }else{
                            $updtnewreq = $db->query('UPDATE element_workflow_state SET place = "supplier_submit,select_curator,select_merchant,reject_merchant,onHold_merchant,reject_curator,onHold_curator" WHERE cid = "'.$objId.'"');
                            $updtnewreq->execute();
                        }

                    }else{
                        $newreq = $db->query('INSERT INTO element_workflow_state (cid, ctype, place, workflow) VALUES ('.$objId.', "object", "supplier_submit,select_curator,select_merchant,reject_merchant,onHold_merchant,reject_curator,onHold_curator", "product_data_enrichment")');
                        //$newreq->execute();
                    }
                }


            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("There is some issue.", 1);
        }
    }

    public function onImportDone(Event $e)
    {
        try {
            $obj = $e->getObject();
            $className =  $obj->getClassName();

            if($className == 'Product'){
                $user = Admin::getCurrentUser();
                if ($user == null) {
                 $user = \Pimcore\Tool\Session::getReadonly()->get("user");
                }
                $templateName = $e->getConfig()->shareSettings->configName;

                //$roleId = $user->getRoles()[0];
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();

                $supplierTamplate = $websiteConfig->get('SupplierCsvTemplate');
                //$roleArray = explode(",",$SupplierCsvTemplate);

                $merchantTamplate = $websiteConfig->get('MerchantCsvTemplate');
                //$merchantRoleArray = explode(",",$MerchantCsvTemplate);

                if ($templateName == $supplierTamplate || $templateName == $merchantTamplate) {
                    $headMerchant = $websiteConfig->get('Head-Merchant');
                    $curatorMail = $websiteConfig->get('Curator');
                    //$submitForReviewMailTemplateID = $websiteConfig->get('SubmitForReviewMailTemplateID');
                    $mail = new \Pimcore\Mail();
                    $mail->addTo($headMerchant,$curatorMail);
                    //$mail->setDocument(\Pimcore\Model\Document::getById($submitForReviewMailTemplateID));
                    $mail->setDocument(\Pimcore\Model\Document::getByPath('/'.EMAILDOCUMENT_PATH.'/Emails/Products/ProductSubmitForReview'));
                    $mail->setSubject('Product update by Supplier');
                    $mail->send();
                }
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("There is some issue.", 1);
        }
    }

}
