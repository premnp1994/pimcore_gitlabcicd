<?php
namespace AppBundle\EventListener;

use AppBundle\Services\PaymentServices;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Pimcore\Model\User;
use Pimcore\Model\DataObject;
use Carbon\Carbon as Carbon;
use GlobalBundle\AppHelpers\GeneralHelper;
use Razorpay\Api\Api;

class SupplierWorkflowListener {

    public function leadSetup(TransitionEvent $event){
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

		// $merchantEmail = GeneralHelper::getWebsiteSetting('MerchantAlias');
        // $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
        //                                 'preState'=>$getFrom[0],
        //                                 'currentState'=>$getTo[0]);
        // GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);

    }

    public function leadValidation(TransitionEvent $event){
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

        $this->checkValidation($event);

        $merchantEmail = GeneralHelper::getWebsiteSetting('MerchantAlias');
        $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);

    }

    public function onbordingPaymentWaived(TransitionEvent $event)
    {
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

		$merchantEmail =  GeneralHelper::getWebsiteSetting('MerchantAlias');
        $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);

        $subscription = $this->getSubscription($company);
        if(!empty($subscription)){
            $subscriptionOrder = new DataObject\SubscriptionOrder();
            $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('w-'.Carbon::now()->timestamp, 'object'));
            $subscriptionOrder->setParentId($subscription[0]->getId());
            $subscriptionOrder->setIsRecurring(false);
            $supplierWaivedOffPeriodData = $merchantEmail = GeneralHelper::getWebsiteSetting('SubscriptionWaiveOffPeriod');
            $startDate = Carbon::now();
            $endDate = Carbon::now()->addDays($supplierWaivedOffPeriodData);
            $subscriptionOrder->setStartDate($startDate);
            $subscriptionOrder->setEndDate($endDate);
            $subscriptionOrder->setOrderType("Waiveoff");
            $subscriptionOrder->setOrderStatus("free");
            $subscriptionOrder->setSubscription($subscription[0]);
            $subscriptionOrder->setCompany($company);
            $subscriptionOrder->setTransactionId("0");
            $subscriptionOrder->setPublished(true);
            $subscriptionOrder->save();

            $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
            $company->setIsWaivedOff(true);
            $company->save();
        }else{
            throw new \Exception(implode("<br>",['Please check Supplier Subscription and Subscription']));
        }

    }

    public function onbordingTrail(TransitionEvent $event)
    {
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

		$merchantEmail = GeneralHelper::getWebsiteSetting('MerchantAlias');
        $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);

        $subscription = $this->getSubscription($company);
        if(!empty($subscription)){
            $subscriptionOrder = new DataObject\SubscriptionOrder();
            $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('t-'.Carbon::now()->timestamp, 'object'));
            $subscriptionOrder->setParentId($subscription[0]->getId());
            $subscriptionOrder->setIsRecurring(false);
            $supplierTrialPeriodData = GeneralHelper::getWebsiteSetting('SubsciptionTrialPeriod');
            $startDate = Carbon::now();
            $endDate = Carbon::now()->addDays($supplierTrialPeriodData);
            $subscriptionOrder->setStartDate($startDate);
            $subscriptionOrder->setEndDate($endDate);
            $subscriptionOrder->setOrderType("Trial");
            $subscriptionOrder->setOrderStatus("free");
            $subscriptionOrder->setSubscription($subscription[0]);
            $subscriptionOrder->setCompany($company);
            $subscriptionOrder->setTransactionId("0");
            $subscriptionOrder->setPublished(true);
            if($subscriptionOrder->save()){
                $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
                $company->setIsWaivedOff(true);
                $company->save();
            }
        }else{
            throw new \Exception(implode("<br>",['Please check Supplier Subscription and Subscription']));
        }
    }

    public function onbordingTakePayment(TransitionEvent $event)
    {

        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

		$merchantEmail = GeneralHelper::getWebsiteSetting('MerchantAlias');
        $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);


        $supplierList = new \Pimcore\Model\DataObject\Supplier\Listing();
        $supplierList->setCondition("company__id = ".$companyID);
        $supplier=$supplierList->load();

        $subscription = $this->getSubscription($company);
        if(!empty($subscription)){
            $subscriptionOrder = new DataObject\SubscriptionOrder();
            $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('p-'.Carbon::now()->timestamp, 'object'));
            $subscriptionOrder->setParentId($subscription[0]->getId());
            $subscriptionOrder->setIsRecurring(true);
            $subscriptionOrder->setOrderType("Paid");
            $subscriptionOrder->setOrderStatus("pending");
            $subscriptionOrder->setSubscription($subscription[0]);
            $subscriptionOrder->setCompany($company);
            $subscriptionOrder->setTransactionId("0");
            $subscriptionOrder->setPublished(true);

            $payment_key = GeneralHelper::getWebsiteSetting('PAYMENT_KEY');
            $payment_secret = GeneralHelper::getWebsiteSetting('PAYMENT_SECRET');
            $api = new Api($payment_key, $payment_secret);
            $subscriptionPayment  = $api->subscription->create(array(
                'plan_id' => $subscription[0]->getPlanId(),
                'customer_notify' => 0,
                'total_count' => 10,
                'expire_by' => Carbon::now()->addYear()->timestamp));

            $subscriptionOrder->setTransactionId($subscriptionPayment['id']);
            $subscriptionOrder->setPaymentInfo(json_encode($subscriptionPayment->toArray()));
            $paymentUrl = $subscriptionPayment['short_url'];
            if($subscriptionOrder->save()){
                $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
                //$paymentUrl = \Pimcore\Tool::getHostUrl() .'/supplierPayment/'.base64_encode($companyID);
                $paramsSupplierPaymentLink = array('paymentLink'=>$paymentUrl,'name'=>$supplier[0]->getFirstname(),'subscriptionName'=>$subscription[0]->getName(),'amountMonth'=>$subscription[0]->getMonthlyCost());
                GeneralHelper::sendMail($supplier[0]->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/PaymentLinkEmailToSupplier',$paramsSupplierPaymentLink);
            }
        }else{
            throw new \Exception(implode("<br>",['Please check Supplier Subscription and Subscription']));
        }
    }

    public function onbordingFinished(TransitionEvent $event)
    {
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

		$merchantEmail =  GeneralHelper::getWebsiteSetting('MerchantAlias');
        $paramsChangeWorkflow = array('supplierCompanyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);

        GeneralHelper::sendMail($merchantEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/SupplierWorkflowChange',$paramsChangeWorkflow);
        $layouts="";

        if($company->getSubscription() == "Essential" ){
            $role = User\Role::getByName("SupplierEssentials");
            $layouts = "PROD_EssentialSupplier";
        }elseif($company->getSubscription() == "Signature"){
            $role = User\Role::getByName("SupplierSignature");
            $layouts = "PROD_SignatureSupplier";
        }elseif($company->getSubscription() == "Curate"){
            $role = User\Role::getByName("SupplierCuration");
            $layouts = "PROD_CurateSupplier";
        }

        $supplierList = new \Pimcore\Model\DataObject\Supplier\Listing();
        $supplierList->setCondition("company__id = ".$companyID);
        $supplierList->setUnpublished(true);
        $supplier=$supplierList->load();

        $db = \Pimcore\Db::get();
        $supplierFolderId =$db->fetchCol("select id from users where type='userfolder' and name='Suppliers'");

        $username = $supplier[0]->getEmail();
        $userList = new User\Listing();
        $userList->setCondition('parentId = ? and name = ?',[$supplierFolderId[0], $username]);
        $user = $userList->load();

        if(count($user) == 0){
            $user = User::create(["name" => $username,
                                  "hasCredentials" => true,
                                  "active" => true,
                                  "parentId" => $supplierFolderId[0],
                                  "firstname"=> trim($supplier[0]->getFirstname()),
                                  "lastname" => trim($supplier[0]->getLastname()),
                                  "email" => $supplier[0]->getEmail(),
                                  "roles" => [$role->getId()],
                                  "admin"=> 0,
                                ]);
            $userId = $user->getId();
        }else{
            $userId = $user[0]->getId();
        }

        $supplier[0]->setUser($userId);
        $supplier[0]->setPublished(true);
        $supplier[0]->save();

        $productFolderName = '/Product';
        $productObject = GeneralHelper::validateFolderPath($productFolderName);
        $productObjectId = $productObject->getId();
        $productAsset = GeneralHelper::validateAssetFolderPath($productFolderName);
        $productAssetId = $productAsset->getId();

        $folderName = $productFolderName.'/'.$company->getName();
        $folderObject = GeneralHelper::validateFolderPath($folderName);
        $folderObjectId = $folderObject->getId();
        $folderAsset = GeneralHelper::validateAssetFolderPath($folderName);
        $folderAssetId = $folderAsset->getId();

        $db = \Pimcore\Db::get();

            $checkProductObj = $db->fetchCol("select * from users_workspaces_object where cid=".$productObjectId." and cpath='".$productFolderName."' and userId = ".$userId);
            if(count($checkProductObj) == 0){
                $db->query("INSERT INTO `users_workspaces_object` (`cid`, `cpath`, `userId`, `list`, `view`, `save`, `publish`, `unpublish`, `delete`, `rename`, `create`, `settings`, `versions`, `properties`, `lEdit`, `lView`, `layouts`)
                VALUES ($productObjectId, '".$productFolderName."', ".$userId.", '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', NULL, NULL, '".$layouts."')");
            }

            $checkProductObj = $db->fetchCol("select * from users_workspaces_asset where cid=".$productAssetId." and cpath='".$productFolderName."' and userId = ".$userId);
            if(count($checkProductObj) == 0){
                $db->query("INSERT INTO `users_workspaces_asset` (`cid`, `cpath`, `userId`, `list`, `view`, `publish`, `delete`, `rename`, `create`, `settings`, `versions`, `properties`)
                VALUES ($productAssetId, '".$productFolderName."', ".$userId.", '0', '0', '0', '0', '0', '1', '0', '0', '0')");
            }

            $checkProductObj = $db->fetchCol("select * from users_workspaces_object where cid=".$folderObjectId." and cpath='".$folderName."' and userId = ".$userId);
            if(count($checkProductObj) == 0){
                $db->query("INSERT INTO `users_workspaces_object` (`cid`, `cpath`, `userId`, `list`, `view`, `save`, `publish`, `unpublish`, `delete`, `rename`, `create`, `settings`, `versions`, `properties`, `lEdit`, `lView`, `layouts`)
                VALUES ($folderObjectId, '".$folderName."', ".$userId.", '1', '1', '1', '0', '0', '0', '0', '1', '0', '0', '0', NULL, NULL,  '".$layouts."')");
            }

            $checkProductObj = $db->fetchCol("select * from users_workspaces_asset where cid=".$folderAssetId." and cpath='".$folderName."' and userId = ".$userId);
            if(count($checkProductObj) == 0){
                $db->query("INSERT INTO `users_workspaces_asset` (`cid`, `cpath`, `userId`, `list`, `view`, `publish`, `delete`, `rename`, `create`, `settings`, `versions`, `properties`)
                VALUES ($folderAssetId, '".$folderName."', ".$userId.", '1', '1', '1', '1', '1', '1', '1', '1', '1')");
            }

        $link = \Pimcore\Tool::getHostUrl() .'/resetpage/'.base64_encode($username);
        $loginlink = \Pimcore\Tool::getHostUrl() .'/admin';

        $paramsFinishSupplier = array('link'=>$link,'name'=>$supplier[0]->getFirstname(),'username'=>$username,'loginlink'=>$loginlink);
        GeneralHelper::sendMail($supplier[0]->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Supplier/PostOnboardingEmailToSupplier',$paramsFinishSupplier);

    }

    public function cancelMembership(TransitionEvent $event)
    {
        $company = $event->getSubject();
        $paymentServices = new PaymentServices();
        $paymentServices->cancelMembership($company);
        
        $params = array('name' => $company->getContactPerson(),'company' => $company->getName());
        GeneralHelper::sendMail($company->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CancelMemebership',$params);

        $company->setPublished(false);
    }

    public function cancelRenewal(TransitionEvent $event){

        $company = $event->getSubject();

        $paymentServices = new PaymentServices();
        $paymentServices->cancelRenewal($company);

        $params = array('name' => $company->getContactPerson(),'company' => $company->getName());
        GeneralHelper::sendMail($company->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CancelRenewal',$params);
    }

    private function checkValidation($event){
        $supplierCompany = $event->getSubject();

        $errorMessages = [];
        try{
            if (!$supplierCompany->getEmail()) {
                $errorMessages[] = 'Email is a required field.';
            }
            if (!$supplierCompany->getName()) {
                $errorMessages[] = 'Name is a required field.';
            }
            if (!$supplierCompany->getContactPerson()) {
                $errorMessages[] = 'Contact Person is a required field.';
            }
            if (count($errorMessages) > 0) {
                throw new \Exception(implode("<br>", $errorMessages));
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    private function getSubscription($company){

        $subData = $company->getSubscription();

        $subscriptionList = new \Pimcore\Model\DataObject\Subscription\Listing();
        $subscriptionList->setCondition("name = ? AND userType = ?",[$subData,'Supplier']);
        $subscription = $subscriptionList->load();
        if($subscription[0]->getPlanId()){
            return $subscription;
        }else{
            throw new \Exception(implode("<br>", ["Please Genrate Subscription Plan"]));
        }

   }

}
