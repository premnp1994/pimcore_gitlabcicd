<?php
/**
 * @package  BayerBundle
 *
 * @author Bhavika Matariya
 */

namespace AppBundle\EventListener;

use PHPUnit\Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\CompletedEvent;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Symfony\Component\Workflow\Event\leaveEvent;
use Pimcore\Db;
use Pimcore\Model\DataObject;

class WorkflowListener implements EventSubscriberInterface {

    public static function getSubscribedEvents()
    {
        return [
            'workflow.workflow.completed.submit_for_review_action' => ['submitForReview'],
            'workflow.workflow.completed.selected_merchant_done' => ['generateEANcode'],
            'workflow.workflow.completed.generate_EAN_code_done' => ['submitForReviewSTG2'],
            'workflow.workflow.completed.quotation_curation_attributs_done' => ['advancePublish'],
            'workflow.workflow.completed.update_by_supplier_stg2' => ['submitForReviewSTG3'],
            'workflow.workflow.completed.start_todo' => ['submitForReview'],
            'workflow.workflow.completed.reject_merchant_action' => ['rejectMerchantAction'],
            'workflow.workflow.completed.reject_curator_action' => ['rejectCuratorAction'],
            'workflow.workflow.completed.onHold_merchant_action' => ['onHoldMerchantAction'],
            'workflow.workflow.completed.onHold_curator_action' => ['onHoldCuratorAction']
            
            /*'workflow.workflow.completed.select_merchant_action' => ['selectMerchantAction'],
            'workflow.workflow.completed.select_curator_action' => ['selectCuratorAction']            */
        ];
        
    }

    public function submitForReview(CompletedEvent $event)
    {
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            //Array ( [0] => new ) Array ( [0] => supplier_submit [1] => select_curator [2] => select_merchant )

            if($getFrom[0] == 'supplier_submit' && $getTo[0] == 'new'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,select_curator,select_merchant,reject_merchant,reject_curator,onHold_merchant,onHold_curator" WHERE cid = '.$obID);
                $updtnewreq->execute();
                
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $headMerchant = $websiteConfig->get('Head-Merchant');
                $curatorMail = $websiteConfig->get('Curator');
                //$submitForReviewMailTemplateID = $websiteConfig->get('P');
                $mail = new \Pimcore\Mail();
                $mail->addTo($headMerchant,$curatorMail);
                //$mail->setDocument(\Pimcore\Model\Document::getById($submitForReviewMailTemplateID));
                $mail->setDocument(\Pimcore\Model\Document::getByPath('/'.EMAILDOCUMENT_PATH.'/Emails/Products/ProductSubmitForReview'));
                $mail->setSubject('Product update by Supplier');
                //$mail->setBodyHtml("Product update by Supplier please review");
                $mail->send();
            }

            if($getFrom[0] == 'new' && $getTo[0] == 'supplier_submit' && $getTo[1] == 'select_curator' && $getTo[2] == 'select_merchant'){
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $headMerchant = $websiteConfig->get('Head-Merchant');
                $curatorMail = $websiteConfig->get('Curator');
                //$submitForReviewMailTemplateID = $websiteConfig->get('SubmitForReviewMailTemplateID');
                $mail = new \Pimcore\Mail();
                $mail->addTo($headMerchant,$curatorMail);
                //$mail->setDocument(\Pimcore\Model\Document::getById($submitForReviewMailTemplateID));
                $mail->setDocument(\Pimcore\Model\Document::getByPath('/'.EMAILDOCUMENT_PATH.'/Emails/Products/ProductSubmitForReview'));
                $mail->setSubject('Product update by Supplier');
                //$mail->setBodyHtml("Product update by Supplier please review");
                $mail->send();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function submitForReviewSTG2(CompletedEvent $event)
    {
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            if($getFrom[0] == 'generate_EAN_code' && $getTo[0] == 'reviewd'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="quotation_attributes_review,curation_attributes_review,generate_EAN_code" WHERE cid = '.$obID);
                $updtnewreq->execute();
                
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $headMerchant = $websiteConfig->get('Head-Merchant');
                $curatorMail = $websiteConfig->get('Curator');
                //$submitForReviewMailTemplateID = $websiteConfig->get('SubmitForReviewMailTemplateID');
                $mail = new \Pimcore\Mail();
                $mail->addTo($headMerchant,$curatorMail);
                //$mail->setDocument(\Pimcore\Model\Document::getById($submitForReviewMailTemplateID));
                $mail->setDocument(\Pimcore\Model\Document::getByPath('/'.EMAILDOCUMENT_PATH.'/Emails/Products/ProductSubmitForReview'));
                $mail->setSubject('Product update by Supplier');
                //$mail->setBodyHtml("Product update by Supplier please review");
                $mail->send();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function submitForReviewSTG3(CompletedEvent $event)
    {
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            if($getFrom[0] == 'advance_published' && $getTo[0] == 'generate_EAN_code'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="quotation_attributes_review,curation_attributes_review,generate_EAN_code" WHERE cid = '.$obID);
                $updtnewreq->execute();
                
                $websiteConfig = \Pimcore\Config::getWebsiteConfig();
                $headMerchant = $websiteConfig->get('Head-Merchant');
                $curatorMail = $websiteConfig->get('Curator');
                //$submitForReviewMailTemplateID = $websiteConfig->get('SubmitForReviewMailTemplateID');
                $mail = new \Pimcore\Mail();
                $mail->addTo($headMerchant,$curatorMail);
                //$mail->setDocument(\Pimcore\Model\Document::getById($submitForReviewMailTemplateID));
                $mail->setDocument(\Pimcore\Model\Document::getByPath('/'.EMAILDOCUMENT_PATH.'/Emails/Products/ProductSubmitForReview'));
                $mail->setSubject('Product update by Supplier');
                //$mail->setBodyHtml("Product update by Supplier please review");
                $mail->send();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function generateEANcode(CompletedEvent $event)
    {
        try {
            //EAN Code = country code + supplier company code + product code (13 digits code)
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            
            $result = $this->checkValidation($event);
            
            if($result['success'] == "false"){

                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,selected_merchant,selected_curator" WHERE cid = '.$obID);
                            $updtnewreq->execute();
                throw new \Exception($result['data']);

            }else{
                //UPDATE element_workflow_state SET place="supplier_submit,selected_merchant,selected_curator" WHERE cid = 472

                if($getFrom[0] == 'selected_merchant' && $getFrom[1] == 'selected_curator' && $getTo[0] == 'quotation_attributes_review' && $getTo[1] == 'curation_attributes_review' && $getTo[2] == 'generate_EAN_code'){
                    $rootPath = PIMCORE_PROJECT_ROOT.'/var/config/country.json';
                    $str = file_get_contents($rootPath);
                    $json = json_decode($str, true); 
                    $product = $event->getSubject();
                    $productCode = $product->getProductCode(); 
                    $productSupplierCompany = $product->getSupplier(); 
                    $suppCompCode = $productSupplierCompany->getCompanyCode();
                    $CountryCodeAL2 = $productSupplierCompany->getCountry();

                    if(empty($productSupplierCompany) || empty($suppCompCode) || empty($CountryCodeAL2) || empty($productCode)){
                        
                        $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,selected_merchant,selected_curator" WHERE cid = '.$obID);
                        $updtnewreq->execute();
                        throw new \Exception("There is some issue on generate EANcode");
                    }else{
                        $key = array_search($CountryCodeAL2, array_column($json, 'alpha-2'));
                        $countryCode = $json[$key]['country-code'];
                        $countEANCode =  strlen($countryCode.$suppCompCode.$productCode);
                        if($countEANCode < 13){
                            $extraAdd =  13 - $countEANCode;  // 5
                            $proCodeLen = strlen($productCode);
                            $proCodeCounting = str_pad($productCode, $proCodeLen + $extraAdd, "0", STR_PAD_LEFT);
                        }else{
                            $proCodeCounting = $productCode;
                        }
                        $eanCode = $countryCode.$suppCompCode.$proCodeCounting;
                        $product->setEanCode($eanCode);
                        $product->setPublished(true);
                        
                        $updtnewreq = $db->query('UPDATE element_workflow_state SET place="quotation_attributes_review,curation_attributes_review,generate_EAN_code" WHERE cid = '.$obID);
                        $updtnewreq->execute();
                    }
                }
            }
        } catch (Model\Element\ValidationException $e) {
            $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,selected_merchant,selected_curator" WHERE cid = '.$obID);
                    $updtnewreq->execute();
            Logger::warn('EANcode Generate: ' . $e->getMessage());
        }
    }

    public function advancePublish(CompletedEvent $event)
    {
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            if($getFrom[0] == 'quotation_reviewed' && $getFrom[1] == 'curation_reviewed' && $getTo[0] == 'advance_published'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="advance_published" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function rejectMerchantAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            
            if($getFrom[0] == 'reject_merchant' && $getTo[0] == 'rejected_merchant'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="rejected_merchant" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function rejectCuratorAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            /*p_r($getFrom);
            p_r($getTo); 
            die;*/
            if($getFrom[0] == 'reject_curator' && $getTo[0] == 'rejected_curator'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="rejected_curator" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function onHoldMerchantAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            if($getFrom[0] == 'onHold_merchant' && $getTo[0] == 'onHolded_merchant'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="onHolded_merchant" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }
    public function onHoldCuratorAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            /*p_r($getFrom);
            p_r($getTo); die;*/
            if($getFrom[0] == 'onHold_curator' && $getTo[0] == 'onHolded_curator'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="onHolded_curator" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }
    

    /*public function selectMerchantAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            p_r($getFrom);
            p_r($getTo); 
            die;
            if($getFrom[0] == 'select_merchant' && $getTo[0] == 'selected_merchant'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,select_curator,reject_curator,onHold_curator,selected_merchant" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }*/

   /* public function selectCuratorAction(CompletedEvent $event){
        try {
            $db = \Pimcore\Db::get();
            $obID = $event->getSubject()->getId();
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();
            p_r($getFrom);
            p_r($getTo); 
            die;
            if($getFrom[0] == 'select_merchant' && $getTo[0] == 'selected_merchant'){
                $updtnewreq = $db->query('UPDATE element_workflow_state SET place="supplier_submit,select_curator,reject_curator,onHold_curator,selected_merchant" WHERE cid = '.$obID);
                $updtnewreq->execute();
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }*/
    
    
    

    function checkValidation($event){
        $product = $event->getSubject();
        $errorMessages = [];
        try{
            if (!$product->getProductCode()) {
                $errorMessages[] = 'Product Code is a required field.';
            }
            if (!$product->getSupplier()) {
                $errorMessages[] = 'Supplier is a required field.';
            }
            if (!$product->getCategory()) {
                $errorMessages[] = 'Category is a required field.';
            }
            if (!$product->getProductName()) {
                $errorMessages[] = 'Product Name is a required field.';
            }
            if (!$product->getMaterial()) {
                $errorMessages[] = 'Material is a required field.';
            }
            if (!$product->getSellingUnit()) {
                $errorMessages[] = 'Selling Unit is a required field.';
            }
            if (!$product->getPriceLevel()) {
                $errorMessages[] = 'Price Level is a required field.';
            }
            if (!$product->getIsFCL()) {
                $errorMessages[] = 'IsFCL is a required field.';
            }
            if (!$product->getPortOfLoading()) {
                $errorMessages[] = 'Port Of Loading is a required field.';
            }
    
            if (count($errorMessages) > 0) {
                $error = ["success" => "false", "data" => implode("<br>", $errorMessages)];
                return $error;
            }

        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }
    
}
