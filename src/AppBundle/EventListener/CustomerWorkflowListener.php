<?php
namespace AppBundle\EventListener;

use AppBundle\Services\PaymentServices;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Pimcore\Model\DataObject;
use Carbon\Carbon as Carbon;
use GlobalBundle\AppHelpers\GeneralHelper;
use Razorpay\Api\Api;


class CustomerWorkflowListener implements EventSubscriberInterface {

    public static function getSubscribedEvents()
    {
        return [
        //    'workflow.company_workflow.transition.onboarding_and_take_payment' => ['onbordingTakePayment'],
        //    'workflow.company_workflow.transition.onboarding_and_payment_waived' => ['onbordingPaymentWaived'],
        //    'workflow.company_workflow.transition.onboarding_and_payment_waived_finished' => ['onbordingFinished'],
        //    'workflow.company_workflow.transition.onboarding_and_take_payment_finished' => ['onbordingFinished'],
        //    'workflow.company_workflow.transition.onboarding_and_trial_finished' => ['onbordingFinished'],
        //    'workflow.company_workflow.transition.onboarding_and_trial' => ['onbordingtrial'],
        //    'workflow.company_workflow.transition.lead_validation_review' => ['leadValidationReview'],
        //    'workflow.company_workflow.transition.lead_review' => ['leadReview'],
        //    'workflow.company_workflow.transition.lead_setup' => ['leadSetup']
        ];
    }


    public function leadReview(TransitionEvent $event){
        try {
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            $company = $event->getSubject();
            $companyID = $company->geto_id();

            $this->checkValidation($event);

            $salesEmail =GeneralHelper::getWebsiteSetting('SalesAlias');
            $paramsChangeWorkflow = array('companyName'=>$company->getName(),
                      'preState'=>$getFrom[0],
                      'currentState'=>$getTo[0]);
            GeneralHelper::sendMail($salesEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerWorkflowChange',$paramsChangeWorkflow);

        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function leadValidationReview(TransitionEvent $event){
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();

		$salesEmail = GeneralHelper::getWebsiteSetting('AccountAlias');
        $paramsChangeWorkflow = array('companyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($salesEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerWorkflowChange',$paramsChangeWorkflow);

    }

    public function leadSetup(TransitionEvent $event){
        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

        $salesEmail =GeneralHelper::getWebsiteSetting('SalesAlias');
        $paramsChangeWorkflow = array('companyName'=>$company->getName(),
                                        'preState'=>$getFrom[0],
                                        'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($salesEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerWorkflowChange',$paramsChangeWorkflow);

    }

    public function onbordingPaymentWaived(TransitionEvent $event)
    {

            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            $company = $event->getSubject();
            $companyID = $company->geto_id();

            $subscription = $this->getSubscription($company);
            if(!empty($subscription)){
                $subscriptionOrder = new DataObject\SubscriptionOrder();
                $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('w-'.Carbon::now()->timestamp, 'object'));
                $subscriptionOrder->setParentId($subscription[0]->getId());
                $subscriptionOrder->setIsRecurring(false);
                $customerWaibedOffPeriodData = GeneralHelper::getWebsiteSetting('SubscriptionWaiveOffPeriod');
                $startDate = Carbon::now();
                $endDate = Carbon::now()->addDays($customerWaibedOffPeriodData);
                $subscriptionOrder->setStartDate($startDate);
                $subscriptionOrder->setEndDate($endDate);
                $subscriptionOrder->setOrderType("Waiveoff");
                $subscriptionOrder->setOrderStatus("free");
                $subscriptionOrder->setSubscription($subscription[0]);
                $subscriptionOrder->setCompany(DataObject\Company::getById($companyID));
                $subscriptionOrder->setTransactionId("0");
                $subscriptionOrder->setPublished(true);
                if($subscriptionOrder->save()){
                    $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
                    $company->setIsWaivedOff(true);
                    $company->save();
                }
            }else{
                throw new \Exception(implode("<br>",['Please check Customer Subscription and Subscription']));
            }

    }
  public function onbordingTrial(TransitionEvent $event)
    {
        try{
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            $company = $event->getSubject();
            $companyID = $company->geto_id();

            $subscription = $this->getSubscription($company);
            if(!empty($subscription)){
                $subscriptionOrder = new DataObject\SubscriptionOrder();
                $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('t-'.Carbon::now()->timestamp.'-'.rand(111,999), 'object'));
                $subscriptionOrder->setParentId($subscription[0]->getId());
                $subscriptionOrder->setIsRecurring(false);
                $customerTrialPeriodData = GeneralHelper::getWebsiteSetting('SubsciptionTrialPeriod');
                $startDate = Carbon::now();
                $endDate = Carbon::now()->addDays($customerTrialPeriodData);
                $subscriptionOrder->setStartDate($startDate);
                $subscriptionOrder->setEndDate($endDate);
                $subscriptionOrder->setOrderType("Trial");
                $subscriptionOrder->setOrderStatus("free");
                $subscriptionOrder->setSubscription($subscription[0]);
                $subscriptionOrder->setCompany(DataObject\Company::getById($companyID));
                $subscriptionOrder->setTransactionId("0");
                $subscriptionOrder->setPublished(true);
                if($subscriptionOrder->save()){
                    $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
                    $company->setIsWaivedOff(true);
                }
            }else{
                throw new \Exception(implode("<br>",['Please check Customer Subscription and Subscription']));
            }
        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    public function onbordingTakePayment(TransitionEvent $event)
    {
            $transitionName = $event->getTransition();
            $getFrom = $transitionName->getFroms();
            $getTo = $transitionName->getTos();

            $company = $event->getSubject();
            $companyID = $company->geto_id();

            $customerList = new \Pimcore\Model\DataObject\Customer\Listing();
            $customerList->setUnpublished(true);
            $customerList->setCondition("company__id = ".$companyID);
            $customer=$customerList->load();

            $subscription = $this->getSubscription($company);

            if(!empty($subscription)){

                $subscriptionOrder = new DataObject\SubscriptionOrder();
                $subscriptionOrder->setKey(\Pimcore\Model\Element\Service::getValidKey('p-'.Carbon::now()->timestamp, 'object'));
                $subscriptionOrder->setParentId($subscription[0]->getId());
                $subscriptionOrder->setIsRecurring(true);
                $subscriptionOrder->setOrderType("Paid");
                $subscriptionOrder->setOrderStatus("pending");
                $subscriptionOrder->setSubscription($subscription[0]);
                $subscriptionOrder->setCompany($company);
                $subscriptionOrder->setTransactionId("0");
                $subscriptionOrder->setPublished(true);

                $payment_key = GeneralHelper::getWebsiteSetting('PAYMENT_KEY');
                $payment_secret = GeneralHelper::getWebsiteSetting('PAYMENT_SECRET');
                $api = new Api($payment_key, $payment_secret);


                    $subscriptionPayment  = $api->subscription->create(array(
                        'plan_id' => $subscription[0]->getPlanId(),
                        'customer_notify' => 0,
                        'total_count' => 10,
                        'expire_by' => Carbon::now()->addYear()->timestamp,
                        )
                );


                $subscriptionOrder->setTransactionId($subscriptionPayment['id']);
                $subscriptionOrder->setPaymentInfo(json_encode($subscriptionPayment->toArray()));
                $paymentUrl = $subscriptionPayment['short_url'];
                if($subscriptionOrder->save()){
                    $company->setSubscriptionOrder(DataObject\SubscriptionOrder::getById($subscriptionOrder->getId()));
                    $company->save();
                    //$paymentUrl = \Pimcore\Tool::getHostUrl() .'/payment/'.base64_encode($companyID);
                    $paramsPayment = array('paymentLink'=>$paymentUrl,'customerName'=>$customer[0]->getFirstname(),'subscriptionName'=>$subscription[0]->getName(),'amountMonth'=>$subscription[0]->getMonthlyCost());
                    GeneralHelper::sendMail($customer[0]->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerPaymentLink',$paramsPayment);
                }
            }else{
                throw new \Exception(implode("<br>",['Please check Customer Subscription and Subscription']));
            }


    }

    public function onbordingFinished(TransitionEvent $event)
    {

        $transitionName = $event->getTransition();
        $getFrom = $transitionName->getFroms();
        $getTo = $transitionName->getTos();

        $company = $event->getSubject();
        $companyID = $company->geto_id();

        $customerList = new \Pimcore\Model\DataObject\Customer\Listing();
        $customerList->setCondition("company__id = ".$companyID);
        $customerList->setUnpublished(true);
        $customer = $customerList->load();

        $salesEmail =GeneralHelper::getWebsiteSetting('SalesAlias');

        $paramsChangeWorkflow = array('companyName'=>$company->getName(),
                      'preState'=>$getFrom[0],
                      'currentState'=>$getTo[0]);
        GeneralHelper::sendMail($salesEmail,'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerWorkflowChange',$paramsChangeWorkflow);

        //generate token
        $token = md5($customer[0]->getId() . time() . uniqid());
        $customer[0]->setPasswordRecoveryToken($token);
        $customer[0]->setPasswordRecoveryTokenDate(Carbon::now());
        $customer[0]->setPublished(true);
        $customer[0]->setActive(true);
        $customer[0]->save();

        $link = \Pimcore\Tool::getHostUrl().'/account/reset-password?token=' . $token;
        $loginlink = \Pimcore\Tool::getHostUrl().'/account/login';

        $paramsChangePassword = array('customer' => $customer[0],'customerName' => $customer[0]->getFirstname(),'token' => $token,'tokenLink' => $link,'loginlink' => $loginlink);
        GeneralHelper::sendMail($customer[0]->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CustomerUserPassword',$paramsChangePassword);
    }

    public function cancelMembership(TransitionEvent $event)
    {
        $company = $event->getSubject();

        $paymentServices = new PaymentServices();
        $paymentServices->cancelMembership($company);

        $params = array('name' => $company->getContactPerson(),'company' => $company->getName());
        GeneralHelper::sendMail($company->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CancelMemebership',$params);
        
        $company->setPublished(false);
        
    }

    public function cancelRenewal(TransitionEvent $event){

        $company = $event->getSubject();

        $paymentServices = new PaymentServices();
        $paymentServices->cancelRenewal($company);

        $params = array('name' => $company->getContactPerson(),'company' => $company->getName());
        GeneralHelper::sendMail($company->getEmail(),'/'.EMAILDOCUMENT_PATH.'/Emails/Customer/CancelRenewal',$params);
        
    }


    private function checkValidation($event){
        $company = $event->getSubject();

        $errorMessages = [];
        try{
            if (!$company->getEmail()) {
                $errorMessages[] = 'Email is a required field.';
            }
            if (!$company->getName()) {
                $errorMessages[] = 'Name is a required field.';
            }
            if (!$company->getContactPerson()) {
                $errorMessages[] = 'Contact Person is a required field.';
            }
            if (!$company->getAddress1()) {
                $errorMessages[] = 'Address1 is a required field.';
            }
            if (!$company->getState()) {
                $errorMessages[] = 'State is a required field.';
            }
            if (!$company->getcountry()) {
                $errorMessages[] = 'Country is a required field.';
            }
            if (!$company->getCity()) {
                $errorMessages[] = 'City is a required field.';
            }
            if (!$company->getZipCode()) {
                $errorMessages[] = 'Zip Code is a required field.';
            }
            if (!$company->getForwarderName()) {
                $errorMessages[] = 'Forwarder Name is a required field.';
            }
            // if (!$company->getSubscription()) {
            //     $errorMessages[] = 'Subscription is a required field.';
            // }

            if (count($errorMessages) > 0) {
                throw new \Exception(implode("<br>", $errorMessages));
            }

        } catch (\Pimcore\Model\Element\ValidationException $e){
            throw new \Exception("Please fill required fields and save, then you can perform workflow action.", 1);
        }
    }

    private function getSubscription($company){

        $subData = $company->getSubscription();
        $subscriptionList = new \Pimcore\Model\DataObject\Subscription\Listing();
        $subscriptionList->setCondition("name = ? AND userType = ?",[$subData,'Customer']);
        $subscription = $subscriptionList->load();
        if($subscription[0]->getPlanId()){
            return $subscription;
        }else{
            throw new \Exception(implode("<br>", ["Please Genrate Subscription Plan"]));
        }
    }

}
