<?php
namespace AppBundle\EventListener;

use Aws\S3\S3Client;
use Pimcore\Model\Asset;
use Pimcore\Event\AssetEvents;
use Pimcore\Event\FrontendEvents;
use Pimcore\Event\Model\AssetEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class S3Listener implements EventSubscriberInterface
{

    /**
     * @var string
     */
    private $s3BaseUrl;

    /**
     * @var string
     */
    private $s3TmpUrlPrefix;

    /**
     * @var string
     */
    private $s3AssetUrlPrefix;

    /**
     * @var \Pimcore\Model\Asset
     */
    private $asset;

    /**
     * @var S3Client
     */
    private $s3Client;

    /**
     * @var bool
     */
    private $isUpdate;

    /**
     * Initialize neccessary variables
     */
    public function __construct()
    {
        if(!S3_ENABLED) {
            return;
        }

        if(CDN_ENABLED) {
            $this->s3BaseUrl = PIMCORE_CDN_BASEURL;
            $this->s3AssetUrlPrefix = $this->s3BaseUrl . "/assets";
            $this->s3TmpUrlPrefix = $this->s3BaseUrl . "/tmp";
        } else {
            $this->s3BaseUrl = PIMCORE_S3_BASEURL;
            $this->s3AssetUrlPrefix = $this->s3BaseUrl . str_replace("s3:/", "", PIMCORE_ASSET_DIRECTORY);
            $this->s3TmpUrlPrefix = $this->s3BaseUrl . str_replace("s3:/", "", PIMCORE_TEMPORARY_DIRECTORY);
        }

        $this->s3Client = new S3Client(S3_CLIENT_ACCESS);
    }

    public static function getSubscribedEvents()
    {
        return [
            FrontendEvents::ASSET_IMAGE_THUMBNAIL => 'onFrontendPathThumbnail',
            FrontendEvents::ASSET_DOCUMENT_IMAGE_THUMBNAIL => 'onFrontendPathThumbnail',
            FrontendEvents::ASSET_VIDEO_IMAGE_THUMBNAIL => 'onFrontendPathThumbnail',
            FrontendEvents::ASSET_VIDEO_THUMBNAIL => 'onFrontendPathThumbnail',
            FrontendEvents::ASSET_PATH => 'onFrontEndPathAsset',
            AssetEvents::IMAGE_THUMBNAIL => 'onAssetThumbnailCreated',
            AssetEvents::VIDEO_IMAGE_THUMBNAIL => 'onAssetThumbnailCreated',
            AssetEvents::DOCUMENT_IMAGE_THUMBNAIL => 'onAssetThumbnailCreated',
            AssetEvents::POST_ADD=>'onPostAdd',
            AssetEvents::POST_UPDATE=>'onPostUpdate',
        ];
    }

    /**
     * Make s3 bucket asset publicly available
     * @param $assetPath
     * @param bool $isTmp
     * @throws \Exception
     */
    protected function makeAssetPublicInS3($assetPath, $isTmp = false)
    {
//        p_r($assetPath);die;
        try{
            $this->s3Client->putObjectAcl(array(
                'ACL' => 'public-read',
                'Grants' => array(
                    array(
                        'Grantee' => array(
                            'DisplayName' => 'public-read-write',
                            'EmailAddress' => '',
                            'Type' => 'CanonicalUser'),
                        'Permission' => 'READ',
                    ),
                ),
                'Owner' => array(
                    'DisplayName' => S3_BUCKET,
                ),
                'Bucket' => S3_BUCKET,
                'Key' => $assetPath,
                'RequestPayer' => S3_BUCKET,
            ));
            if($isTmp === false) {
                //$this->asset->setProperty("isPublicInS3", "bool", true);
                $this->asset->update(true);
            }
        }catch (Aws\S3\Exception\S3Exception $e) {
            echo "There was an error uploading the file.\n";
        }

    }

    /**
     * Listen to following events
     * pimcore.asset.postAdd
     * @param ElementEventInterface $e
     * @return bool
     */
    public function onPostAdd(ElementEventInterface $e)
    {
        if(!S3_ENABLED) {
            return true;
        }

        if($e instanceof AssetEvent) {
            $this->asset = $e->getAsset();
            $key = $this->asset->getRealFullPath();

            try {
                $this->makeAssetPublicInS3(S3_FOLDER.$key);
            } catch(\Exception $e) {
                // do nothing
            }
        }
    }

    /**
     * Listen to following events
     * pimcore.asset.postUpdate
     * @param ElementEventInterface $e
     * @return bool
     */
    public function onPostUpdate(ElementEventInterface $e)
    {
        if(!S3_ENABLED) {
            return true;
        }

        if($e instanceof AssetEvent) {
            $this->asset = $e->getAsset();
            if($this->asset->getType() == "folder") {
                return false;
            }
            $key = $this->asset->getRealFullPath();
//            if($this->checkIfMakeAssetPublicinS3()) {
                try {
                    //if(!$this->asset->getProperty("isPublicInS3")) {
                    $this->makeAssetPublicInS3(S3_FOLDER.$key);
                    //}
                } catch(\Exception $e) {
                    // do nothing
                    p_r($e->getMessage());
                }
//            }

        }
    }


    /**
     * Listen to following events
     * pimcore.frontend.path.asset.image.thumbnail
     * pimcore.frontend.path.asset.document.image-thumbnail
     * pimcore.frontend.path.asset.video.image-thumbnail
     * pimcore.frontend.path.asset.video.thumbnail
     *
     * @param GenericEvent $event
     */
    public function onFrontendPathThumbnail(GenericEvent $event) {
        //$fileSystemPath = $event->getSubject()->getFileSystemPath();
        $assetId = $event->getSubject()->getAsset()->getId();
        $image = Asset\Image::getById($assetId);
        if(empty($event->getSubject()->getConfig())){
            $fileSystemPath = $image->getFileSystemPath();
        }else{
            $thumbnail = $image->getThumbnail($event->getSubject()->getConfig()->getName());
            $fileSystemPath = $thumbnail->getFileSystemPath();
        }

        $cacheKey = "thumb_s3_" . md5($fileSystemPath);
        $path = \Pimcore\Cache::load($cacheKey);

        if(!$path) {
            if(!file_exists($fileSystemPath)) {
                // the thumbnail doesn't exist yet, so we need to create it on request -> Thumbnail controller plugin
                $path = str_replace(PIMCORE_TEMPORARY_DIRECTORY, "", $fileSystemPath);
            } else {
                $key = str_replace(PIMCORE_TEMPORARY_DIRECTORY . "/", "/", $fileSystemPath);
                try {
                    $this->makeAssetPublicInS3(S3_TMP_FOLDER.$key, true);
                } catch(\Exception $e) {
                    // do nothing
                }
                $path = str_replace(PIMCORE_TEMPORARY_DIRECTORY . "/", $this->s3TmpUrlPrefix . "/", $fileSystemPath);
            }
        }
        $event->setArgument('frontendPath',$path);
    }

    /**
     * Listen to following events
     * pimcore.asset.image.thumbnail
     * pimcore.asset.video.image-thumbnail
     * pimcore.asset.document.image-thumbnail
     *
     * @param GenericEvent $event
     */
    public function onAssetThumbnailCreated(GenericEvent $event)
    {
        $thumbnail = $event->getSubject();

        $fsPath = $thumbnail->getFileSystemPath();

        if ($fsPath && $event->getArgument("generated")) {
            $cacheKey = "thumb_s3_" . md5($fsPath);

            \Pimcore\Cache::remove($cacheKey);
        }
    }

    /**
     * Listen to following events
     * pimcore.frontend.path.asset
     *
     * @param GenericEvent $event
     */
    public function onFrontEndPathAsset(GenericEvent $event) {

        if(!S3_ENABLED) {
            return true;
        }

        $asset = $event->getSubject();
        $path = str_replace(PIMCORE_ASSET_DIRECTORY . "/", $this->s3AssetUrlPrefix . "/", $asset->getFileSystemPath());
//        p_r($path);die;
        $event->setArgument('frontendPath',$path);
    }
}
