<?php

namespace AppBundle\Command;

/*
 * Subscription Time finished command
 */

use AppBundle\Services\PaymentServices;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CoreCompetencyCommand for Importing base CoreCompetency from Interim database
 *
 * @author Pimcore
 */
class SubscriptionFinishCommand extends Command {

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'nufind:subscription-finish';

    /**
     * @var array stores different languages
     */
    const languages = [
        "en_US",
    ];
    const tab = 'baseData';

    protected function configure() {
        $this->setName(SubscriptionFinishCommand::$defaultName)
                ->setDescription('Cancel Membership');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
       
        $paymentServices = new PaymentServices();
        $paymentServices->subscriptionTimeFinished();
      
    }

}
