/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */


 pimcore.registerNS("pimcore.plugin.AppBundle");
 pimcore.plugin.AppBundle = Class.create(pimcore.plugin.admin, {
 
     getClassName: function () {
         return "pimcore.plugin.AppBundle";
     },
 
     initialize: function () {
         pimcore.plugin.broker.registerPlugin(this);
         
     },
 
     postOpenDocument: function (document, type) {
       
        if (type === 'newsletter') {
            document.tab.items.items[0].add({
                text: t('plugin_cmf_newsletter_export_template'),
                iconCls: 'plugin_cmf_icon_export_action',
                scale: 'small',
                handler: function (obj) {

                    Ext.Ajax.request({
                        url: "/templates/export",
                        method: "post",
                        params: {document_id: document.id},
                        success: function (response) {
                            
                            var rdata = Ext.decode(response.responseText);
                            
                            if (rdata && rdata.success) {
                                //window.location.href = rdata.data;
                               // window.open(rdata.data,'_blank');
                                download(rdata.data.filename,rdata.data.html)
                                pimcore.helpers.showNotification(t("success"), t("plugin_cmf_newsletter_export_template_success"), "success");
                            } else {
                                pimcore.helpers.showNotification(t("error"), t("plugin_cmf_newsletter_export_template_error"), "error", response.responseText);
                            }

                        }.bind(this)
                    });

                }.bind(this, document)
            });
            pimcore.layout.refresh();
        }

        this.addSegmentAssignmentTab(document, 'document', type);
    },

    addSegmentAssignmentTab: function (element, type, subType) {
        var addTab = Boolean(pimcore.settings.cmf.segmentAssignment[type][subType]);
        
        if('object' === type && 'folder' !== subType) {
            addTab &= pimcore.settings.cmf.segmentAssignment[type][subType][element.data.general.o_className];
        }

        if (!addTab) {
            return;
        }

        this.segmentTab = new pimcore.plugin.customermanagementframework.segmentAssignmentTab(element, type);
        var tabPanel = element.tab.items.items[1];
        tabPanel.insert(tabPanel.items.length, this.segmentTab.getLayout());
        tabPanel.updateLayout();
    }
 });

 function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);
  }
 new pimcore.plugin.AppBundle();