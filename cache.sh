php bin/console cache:clear
php bin/console pimcore:deployment:classes-rebuild -d -c
php bin/console assets:install web/ --symlink --relative
php bin/console pimcore:cache:clear