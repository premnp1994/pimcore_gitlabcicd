<?php
if (S3_ENABLED) {
    $s3Client = new \Aws\S3\S3Client(S3_CLIENT_ACCESS);

    $s3Client->registerStreamWrapper();

    // set default file context
    \Pimcore\File::setContext(stream_context_create([
        's3' => ['seekable' => true]
    ]));
}
