<?php

return [
    "default" => [
        "iconCls" => "pimcore_nav_icon_perspective",
        "elementTree" => [
            [
                "type" => "documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -3
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -2
            ],
            [
                "type" => "objects",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -1
            ]
        ],
        "dashboards" => [
            "predefined" => [
                "welcome" => [
                    "positions" => [
                        [
                            [
                                "id" => 1,
                                "type" => "pimcore.layout.portlets.modificationStatistic",
                                "config" => null
                            ],
                            [
                                "id" => 2,
                                "type" => "pimcore.layout.portlets.modifiedAssets",
                                "config" => null
                            ],
                        ],
                        [
                            [
                                "id" => 3,
                                "type" => "pimcore.layout.portlets.modifiedObjects",
                                "config" => null
                            ],
                            [
                                "id" => 4,
                                "type" => "pimcore.layout.portlets.modifiedDocuments",
                                "config" => null
                            ]
                        ]
                    ],
                    ]
                ]
            ]
    ],
    "Admin" => [
        "iconCls" => "pimcore_nav_icon_perspective",
        "elementTree" => [
            [
                "type" => "documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -3
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -2
            ],
            [
                "type" => "objects",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -1
            ]
        ],
        "dashboards" => [
            "predefined" => [
                "welcome" => [
                    "positions" => [
                        [
                            [
                                "id" => 1,
                                "type" => "pimcore.layout.portlets.modificationStatistic",
                                "config" => null
                            ],
                            [
                                "id" => 2,
                                "type" => "pimcore.layout.portlets.modifiedAssets",
                                "config" => null
                            ],
                        ],
                        [
                            [
                                "id" => 3,
                                "type" => "pimcore.layout.portlets.modifiedObjects",
                                "config" => null
                            ],
                            [
                                "id" => 4,
                                "type" => "pimcore.layout.portlets.modifiedDocuments",
                                "config" => null
                            ]
                        ]
                    ],
                    ]
                ]
            ]
    ],
    "Curator" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
           
            [
                "type" => "customview",
                "id" => 2,
                "name"=>"Products",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],            
            [
                "type" => "customview",
                "id" => 23,
                "name"=>"Campaigns",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 1,
                "name"=>"Masters",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
        ],
    ],
    "Accounts" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
           
            [
                "type" => "customview",
                "id" => 10,
                "name"=>"Customers",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 11,
                "name"=>"Suppliers",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    "3S CMS" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
           
            [
                "type" => "customview",
                "id" => 24,
                "name"=>"3S Website Data",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 19,
                "name"=>"3S Website Assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 14,
                "name"=>"3S Website Pages",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    "Mi Source CMS" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
           
            [
                "type" => "customview",
                "id" => 25,
                "name"=>"Mi Source Website Data",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 20,
                "name"=>"Mi Source Website Assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 15,
                "name"=>"Mi Source Website Pages",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    "Supplier" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [            
            [
                "type" => "customview",
                "id" => 2,
                "name"=>"Products",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 18,
                "name"=>"Product Assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    "Merchant" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [    
            [
                "type" => "customview",
                "id" => 1,
                "name"=>"Masters",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 18,
                "name"=>"Product Assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],        
            [
                "type" => "customview",
                "id" => 3,
                "name"=>"Products",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 23,
                "name"=>"Campaigns",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 4
            ],
            [
                "type" => "customview",
                "id" => 21,
                "name"=>"Supplier Documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 5
            ],
            [
                "type" => "customview",
                "id" => 11,
                "name"=>"Suppliers",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 6
            ],
            [
                "type" => "customview",
                "id" => 17,
                "name"=>"Supplier Subscriptions",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 7
            ],
            [
                "type" => "customview",
                "id" => 12,
                "name"=>"RFQ/RFC",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 8
            ],
            [
                "type" => "customview",
                "id" => 13,
                "name"=>"RFN",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 9
            ],
        ],
    ],
    "Sales" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [  
            [
                "type" => "customview",
                "id" => 22,
                "name"=>"Customer Documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 10,
                "name"=>"Customers",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 16,
                "name"=>"Customer Subscriptions",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 1,
                "name"=>"Masters",
                "position" => "left",
                "expanded" => true,
                "hidden" => false,
                "sort" => 4
            ]
            
        ],
    ],
    /*
    "Merchant" => [
        "iconCls" => "pimcore_nav_icon_object",
        "toolbar" => [
            "file" =>0,
            "extras" => [
                "hidden" => true
            ],
            "marketing" => [
                "hidden" => true
            ],
            "settings"=>false,
//            "settings" => [
//                "items" => [
//                    "documentTypes" => false,
//                    "predefinedProperties" => false,
//                    "predefinedMetadata" => false,
//                    "system" => false,
//                    "web2print" => false,
//                    "routes" => false,
//                    "thumbnails" => false,
//                    "adminTranslations" => false,
//                    "website" => false,
//                    "users" => false,
//                    "cache" => false,
//                    "objects"=>false,
//                    "tagConfiguration"=>false
//                ]
//            ],
//            "search" => [
//                "items" => [
//                    "documents" => false,
//                    "esBackendSearch" => false
//                ]
//            ],
            "search"=>false,
            "ecommerce" => false,
            "notification"=>true
        ],
        "elementTree" => [
            [
                "type" => "customview",
                "id" => 1,
                "hidden" => false,
                "position" => "left",
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 2,
                "hidden" => false,
                "position" => "left",
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 3,
                "hidden" => false,
                "position" => "left",
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 4,
                "hidden" => false,
                "position" => "left",
                "sort" => 4
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
        ],
    ],
    "Curator" => [
        "iconCls" => "pimcore_nav_icon_asset",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 2,
                "name"=>"Product",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ],
            [
                "type" => "customview",
                "id" => 7,
                "name"=>"Customer",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ]
        ],
    ],
    "Marketing" => [
        "iconCls" => "pimcore_nav_icon_document",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "id" => 2,
                "name"=>"Product",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    "Sales" => [
        "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/shopping-cart.svg",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "customview",
                "id" => 2,
                "name"=>"Product",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 7,
                "name"=>"Customer",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ]
        ],
    ],
    "Account" => [
        "icon" => "pimcore_nav_icon_object",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "customview",
                "id" => 7,
                "name"=>"Customer",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ]
        ],
    ],
    "Supplier" => [
        "iconCls" => "pimcore_nav_icon_object",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "customview",
                "position" => "left",
                'id' => 2,
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "position" => "left",
                'id' => 3,
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ]
        ],
    ],
    "3SWebsite" => [
        "iconCls" => "pimcore_nav_icon_object",
        "toolbar" => [
            "file" => false,
            "extras" => false,
            "marketing" => false,
            "settings" => false,
            "search" => false,
            "ecommerce" => false
        ],
        "elementTree" => [
            [
                "type" => "customview",
                "position" => "left",
                'id' => 9,
                "expanded" => false,
                "hidden" => false,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "position" => "left",
                'id' => 8,
                "expanded" => false,
                "hidden" => false,
                "sort" => 2
            ],
            [
                "type" => "customview",
                "position" => "left",
                'id' => 7,
                "expanded" => false,
                "hidden" => false,
                "sort" => 3
            ]
        ],
    ],
    */
];
