<?php

return [
    "views" => [
        [
            "treetype" => "object",
            "name" => "Masters",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/automotive.svg",
            "id" => 1,
            "rootfolder" => "/Master",
            "showroot" => TRUE,
            "classes" => "AGND,BNG,CA,CT,CLR,CMPL,CONS,CRSC,CUR,DESCP,DESG,DT,EVNT,FNS,FUNC,HLD,IDS,LT,LSC,MAT,OS,PORT,PRCL,SEAS,SU,SMENU,SPC,STT,SUBS",
            "position" => "left",
            "sort" => "3",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 1,                                             // hide "Add Object" , "Add Variant"                                    // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "asset",
            "name" => "Product Assets",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/camera.svg",
            "id" => 18,
            "rootfolder" => "/Product",
            "showroot" => TRUE,
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "asset" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                     'addFolder' => 1,                                          // hide "Rename"
                     'searchAndMove' => 0,                                   // hide "Search And Move"
                     'lock' => 0,                                            // hide "Lock"
                     'unlock' => 0,                                          // hide "Unlock"
                     'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                     'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                     'reload' => 1                                         // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Products", //products for supplier
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-product.svg",
            "id" => 2,
            "rootfolder" => "/Product",
            "showroot" => TRUE,
            "classes" => "PROD",
            "position" => "left",
            "sort" => "4",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 1,                                   // hide "Import From CSV"
                        
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Products", //products for Merchant
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-product.svg",
            "id" => 3,
            "rootfolder" => "/Product",
            "showroot" => TRUE,
            "classes" => "PROD",
            "position" => "left",
            "sort" => "4",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 1,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 1,                                   // hide "Import From CSV"
                        
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Orders",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-cart.svg",
            "id" => 4,
            "showroot" => FALSE,
            "rootfolder" => "/Shop/Orders",
            "classes" => "",
            "position" => "left",
            "sort" => "10",
            "expanded" => TRUE
        ],
        /*[
            "treetype" => "object",
            "name" => "Customers",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 7,
            "showroot" => FALSE,
            "rootfolder" => "/Customer",
            "classes" => "CU,COMP,SUP",
            "position" => "left",
            "sort" => "6",
            "expanded" => TRUE
        ],*/
        [
            "treetype" => "object",
            "name" => "Customer Segments",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pie_chart.svg",
            "id" => 7,
            "showroot" => FALSE,
            "rootfolder" => "/Customer Management/segments",
            "classes" => "1,2,3",
            "position" => "left",
            "sort" => "6",
            "expanded" => TRUE
        ],
        [
            "treetype" => "document",
            "name" => "Catalog",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-print.svg",
            "id" => 6,
            "showroot" => FALSE,
            "rootfolder" => "/print",
            "position" => "left",
            "sort" => "5",
            "expanded" => FALSE
        ],
        [
            "treetype" => "document",                                           // document view
            "name" => "3S Website Pages",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/text.svg",
            "id" => 24,                                                          // again, unique ID
            "rootfolder" => "/3SWebsite",
            "showroot" => TRUE,
            "position" => "left",                                              // show it in the right accordion
            "sort" => "1",
            "expanded" => true,                                                 // expand the tree panel
            "treeContextMenu" => [
                "document" => [
                    "items" => [
                        "add" => 0,                                             // hide all the "Add *" stuff
                        "addFolder" => 0,                                       // hide "Add Folder"
                        "searchAndMove" => 0,      
                        "lock" => 0,                                            // hide "Lock"
                        "unlock" => 0,                                          // hide "Unlock"
                        "lockAndPropagate" => 0,                                // hide "UnlockAndPropagate"
                        "unlockAndPropagate" => 0,                              // hide "Lock And Propagate"
                        "reload" => 1,                                           // show "Reload" (redundant, visible by default anyway)
                        "addSnippet" => 0,
                        "addLink" => 0,
                        "addEmail" => 0,
                        "addNewsletter" => 0,
                        "paste" => 0,
                        "pasteCut" => 0,
                        "copy" => 0,
                        "cut" => 0,
                        "rename" => 0,
                        "unpublish" => 0,
                        "publish" => 0,
                        "delete" => 0,
                        "convert" => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "asset",
            "name" => "3S Website Assets",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/camera.svg",
            "id" => 19,
            "rootfolder" => "/3S Website Data",
            "showroot" => TRUE,
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "asset" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                     'addFolder' => 0,                                          // hide "Rename"
                     'searchAndMove' => 0,                                   // hide "Search And Move"
                     'lock' => 0,                                            // hide "Lock"
                     'unlock' => 0,                                          // hide "Unlock"
                     'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                     'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                     'reload' => 1                                         // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "3S Website Data", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 14,
            "rootfolder" => "/3S Website",
            "showroot" => TRUE,
            "classes" => "BAS,BAM,CI,LST,NRC",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 1,                                             // hide "Add Object" , "Add Variant"
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Customers", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/person.svg",
            "id" => 10,
            "rootfolder" => "/Customer",
            "showroot" => TRUE,
            "classes" => "COMP,CU",
            "position" => "left",
            "sort" => "6",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,
                        'importCsv' => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Suppliers", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 11,
            "rootfolder" => "/Supplier",
            "showroot" => TRUE,
            "classes" => "SUP,SUPC",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "RFQ/RFC", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 12,
            "rootfolder" => "/RFQ",
            "showroot" => TRUE,
            "classes" => "EF_OSO,EF_OSOI",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 1,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "RFN", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 13,
            "rootfolder" => "/RFN",
            "showroot" => TRUE,
            "classes" => "RFN",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                    ]
                ]
            ]
        ],
        
        [
            "treetype" => "object",
            "name" => "Customer Subscriptions", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 16,
            "rootfolder" => "/Subscription/Customer",
            "showroot" => TRUE,
            "classes" => "SUBS,SUBORD",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "Supplier Subscriptions", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 17,
            "rootfolder" => "/Subscription/Supplier",
            "showroot" => TRUE,
            "classes" => "SUBS,SUBORD",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 0
                    ]
                ]
            ]
        ],        
        [
            "treetype" => "asset",
            "name" => "Supplier Documents",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/camera.svg",
            "id" => 21,
            "rootfolder" => "/SupplierDocuments",
            "showroot" => TRUE,
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "asset" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                     'addFolder' => 0,                                          // hide "Rename"
                     'searchAndMove' => 0,                                   // hide "Search And Move"
                     'lock' => 0,                                            // hide "Lock"
                     'unlock' => 0,                                          // hide "Unlock"
                     'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                     'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                     'reload' => 1                                         // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "asset",
            "name" => "Customer Documents",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/camera.svg",
            "id" => 22,
            "rootfolder" => "/CustomerDocuments",
            "showroot" => TRUE,
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "asset" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                     'addFolder' => 1,                                          // hide "Rename"
                     'searchAndMove' => 0,                                   // hide "Search And Move"
                     'lock' => 0,                                            // hide "Lock"
                     'unlock' => 0,                                          // hide "Unlock"
                     'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                     'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                     'reload' => 1                                         // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "document",                                           // document view
            "name" => "Campaigns",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/text.svg",
            "id" => 23,                                                          // again, unique ID
            "rootfolder" => "/MiSource/Campaigns",
            "showroot" => TRUE,
            "position" => "left",                                              // show it in the right accordion
            "sort" => "1",
            "expanded" => true,                                                 // expand the tree panel
            "treeContextMenu" => [
                "document" => [
                    "items" => [
                        "add" => 0,                                             // hide all the "Add *" stuff
                        "addFolder" => 1,                                       // hide "Add Folder"
                        "searchAndMove" => 0,      
                        "lock" => 0,                                            // hide "Lock"
                        "unlock" => 0,                                          // hide "Unlock"
                        "lockAndPropagate" => 0,                                // hide "UnlockAndPropagate"
                        "unlockAndPropagate" => 0,                              // hide "Lock And Propagate"
                        "reload" => 1,                                           // show "Reload" (redundant, visible by default anyway)
                        "addSnippet" => 0,
                        "addLink" => 0,
                        "addEmail" => 1,
                        "addNewsletter" => 1,
                        "paste" => 1,
                        "pasteCut" => 1,
                        "copy" => 1,
                        "cut" => 1,
                        "rename" => 1,
                        "unpublish" => 1,
                        "publish" => 1,
                        "delete" => 1,
                        "convert" => 1                        
                    ]
                ]
            ]
        ],
        [
            "treetype" => "document",                                           // document view
            "name" => "Mi Source Website Pages",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/text.svg",
            "id" => 25,                                                          // again, unique ID
            "rootfolder" => "/MiSource",
            "showroot" => TRUE,
            "position" => "left",                                              // show it in the right accordion
            "sort" => "1",
            "expanded" => true,                                                 // expand the tree panel
            "treeContextMenu" => [
                "document" => [
                    "items" => [
                        "add" => 0,                                             // hide all the "Add *" stuff
                        "addFolder" => 0,                                       // hide "Add Folder"
                        "searchAndMove" => 0,      
                        "lock" => 0,                                            // hide "Lock"
                        "unlock" => 0,                                          // hide "Unlock"
                        "lockAndPropagate" => 0,                                // hide "UnlockAndPropagate"
                        "unlockAndPropagate" => 0,                              // hide "Lock And Propagate"
                        "reload" => 1,                                           // show "Reload" (redundant, visible by default anyway)
                        "addSnippet" => 0,
                        "addLink" => 0,
                        "addEmail" => 0,
                        "addNewsletter" => 0,
                        "paste" => 0,
                        "pasteCut" => 0,
                        "copy" => 0,
                        "cut" => 0,
                        "rename" => 0,
                        "unpublish" => 0,
                        "publish" => 0,
                        "delete" => 0,
                        "convert" => 0
                    ]
                ]
            ]
        ],
        [
            "treetype" => "asset",
            "name" => "NuFind Website Assets",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/camera.svg",
            "id" => 20,
            "rootfolder" => "/NuFind Website",
            "showroot" => TRUE,
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "asset" => [
                    "items" => [                                             // hide "Add Object" , "Add Variant"
                     'addFolder' => 0,                                          // hide "Rename"
                     'searchAndMove' => 0,                                   // hide "Search And Move"
                     'lock' => 0,                                            // hide "Lock"
                     'unlock' => 0,                                          // hide "Unlock"
                     'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                     'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                     'reload' => 1                                         // hide reload
                    ]
                ]
            ]
        ],
        [
            "treetype" => "object",
            "name" => "NuFind Website Data", 
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/businessman.svg",
            "id" => 15,
            "rootfolder" => "/NuFind Website",
            "showroot" => TRUE,
            "classes" => "BAD,MC",
            "position" => "left",
            "sort" => "7",
            "expanded" => TRUE,
            "treeContextMenu" => [
                "object" => [
                    "items" => [
                        'addFolder' => 0,                                       // hide "Add Folder"
                        'paste' => 0,                                           // hide "Paste"
                        'copy' => 0,                                            // hide "Copy"
                        'cut' => 0,                                             // hide "Cut"
                        'publish' => 0,                                         // hide "Publish"
                        'unpublish' => 0,                                       // hide "Unpublish"
                        'delete' => 0,                                          // show "Delete" (redundant as this is the default)
                        'rename' => 0,                                          // hide "Rename"
                        'searchAndMove' => 0,                                   // hide "Search And Move"
                        'lock' => 0,                                            // hide "Lock"
                        'unlock' => 0,                                          // hide "Unlock"
                        'lockAndPropagate' => 0,                                // hide "Lock and Propagate"
                        'unlockAndPropagete' => 0,                              // hide "Unlock and Propagate"
                        'reload' => 1,                                          // hide reload                        
                        'add' => 0,                                             // hide "Add Object" , "Add Variant"
                        'importCsv' => 0
                    ]
                ]
            ]
        ],
    ]
];
