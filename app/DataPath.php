<?php

//Objct Path 3S Website

use Pimcore\Model\DataObject\Fieldcollection\Definition;

define("BOOK_MEETING_PATH","/3S Website/Booked Meeting");
define("CAREER_INQUIRY_PATH","/3S Website/Career Inquiry");
define("SUPPLIER_REQUEST_PATH","/3S Website/Supplier Request");


//Objct Path NuFIND Website
define("BOOKADEMO_PATH", '/NuFind Website/Booked Demo');
define("CONTACTUS_PATH", '/NuFind Website/Contact Us');
define("EMAILDOCUMENT_PATH", 'NuFind');

define('CUSTOMER_PATH', '/Customer');
define('SUPPLIER_PATH', '/Supplier');
define('SUBSCRIPTION', '/Subscription');
define('SUBSCRIPTION_ORDER', '/Subscription Order');


define('MASTER_AGENDA','/Master/Agenda');
define('MASTER_BEING','/Master/Being');
define('MASTER_CATEGORY','/Master/Category');
define('MASTER_CITY','/Master/City');
define('MASTER_COLOR','/Master/Color');
define('MASTER_COMPLIANCE','/Master/Compliance');
define('MASTER_CONSTRUCTION','/Master/Construction');
define('MASTER_CROSS_CATEGORY','/Master/Cross Category');
define('MASTER_CURRENCY','/Master/Currency');
define('MASTER_DESCRIPTION_PRESET','/Master/Description Preset');
define('MASTER_DESIGN_TYPE','/Master/Design Type');
define('MASTER_DESIGNATION','/Master/Designation');
define('MASTER_EVENT','/Master/Event');
define('MASTER_FILTER','/Master/Filter');
define('MASTER_FINISH','/Master/Finish');
define('MASTER_FUNCTION','/Master/Function');
define('MASTER_HOLIDAY','/Master/Holiday');
define('MASTER_IDS','/Master/IDS');
define('MASTER_LEAD_TIME','/Master/Lead Time');
define('MASTER_LIFESTYLE_COLLECTION','/Master/Lifestyle Collection');
define('MASTER_MATERIAL','/Master/Material');
define('MASTER_MISOURCECONTACT','/Master/Misource Contact');
define('MASTER_ORDER_STATUS','/Master/Order Status');
define('MASTER_PORT','/Master/Port');
define('MASTER_PRICE_LEVEL','/Master/Price Level');
define('MASTER_SEASON','/Master/Season');
define('MASTER_SELLING_UNIT','/Master/Selling Unit');
define('MASTER_SITEMENU','/Master/Sitemenu');
define('MASTER_SPACE','/Master/Space');
define('MASTER_STATE','/Master/State');
define('WEBSITE_CASE_STUDIES','/3S Website/Case Studies');
define('WEBSITE_NEWS','/3S Website/News');
define('WEBSITE_RESOURCES','/3S Website/Resources');
define('WEBSITE_LEADERSHIP_TEAM','/3S Website/Leadership Team');
define('WEBSITE_OUR_TEAM','/3S Website/Our Team');
define('SUBSCRIPTION_CUSTOMER','/Subscription/Customer');
define('SUBSCRIPTION_SUPPLIER','/Subscription/Supplier');
